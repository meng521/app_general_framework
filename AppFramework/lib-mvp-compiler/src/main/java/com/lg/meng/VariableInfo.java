package com.lg.meng;

import javax.lang.model.element.VariableElement;

public class VariableInfo {
    VariableElement variableElement;

    public VariableElement getVariableElement() {
        return variableElement;
    }

    public void setVariableElement(VariableElement variableElement) {
        this.variableElement = variableElement;
    }
}
