package com.lg.meng;

import com.google.auto.service.AutoService;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterSpec;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.util.Elements;
import javax.tools.Diagnostic;


@AutoService(Processor.class)
public class LifecyclerInjectProcessor extends AbstractProcessor {
    private Messager messager;
    Map<String, List<VariableInfo>> classMap = new HashMap<>();
    Map<String, TypeElement> classTypeElementMap = new HashMap<>();
    private Filer filer;
    private Elements elementUtils;

    @Override
    public synchronized void init(ProcessingEnvironment processingEnvironment) {
        super.init(processingEnvironment);
        messager = processingEnv.getMessager();
        filer = processingEnvironment.getFiler();
        elementUtils = processingEnvironment.getElementUtils();
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        HashSet<String> supportTypes = new LinkedHashSet<>();
        supportTypes.add(BindLifecycler.class.getCanonicalName());
        return supportTypes;
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }

    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {
        messager.printMessage(Diagnostic.Kind.NOTE, "LifecyclerInjectProcessor process...");
        collectInfo(roundEnvironment);
        writeToFile();
        return true;
    }

    private void collectInfo(RoundEnvironment roundEnvironment) {
        System.err.println("------------LifecyclerInjectProcessor collectInfo");
        classMap.clear();
        classTypeElementMap.clear();
        Set<? extends Element> elements = roundEnvironment.getElementsAnnotatedWith(BindLifecycler.class);
        for (Element element : elements) {
            VariableElement variableElement = (VariableElement) element;
            TypeElement typeElement = (TypeElement) variableElement.getEnclosingElement();
            String classFullName = typeElement.getQualifiedName().toString();
            System.err.println("------------classFullName:" + classFullName);
            List<VariableInfo> infoList = classMap.get(classFullName);
            if (infoList == null) {
                infoList = new ArrayList<>();
                classMap.put(classFullName, infoList);
                classTypeElementMap.put(classFullName, typeElement);
            }

            VariableInfo variableInfo = new VariableInfo();
            variableInfo.setVariableElement(variableElement);
            infoList.add(variableInfo);
        }
    }

    void writeToFile() {
        try {
            System.err.println("------------LifecyclerInjectProcessor writeToFile");
            for (String classFullName : classMap.keySet()) {
                TypeElement typeElement = classTypeElementMap.get(classFullName);
                // 使用构造函数绑定数据
                MethodSpec.Builder constructor = MethodSpec.constructorBuilder()
                        .addModifiers(Modifier.PUBLIC)
                        .addParameter(ParameterSpec.builder(TypeName.get(typeElement.asType()), "host").build());

                MethodSpec.Builder method = MethodSpec.methodBuilder("onDestory")
                        .addModifiers(Modifier.PUBLIC);

                List<VariableInfo> variableList = classMap.get(classFullName);

                List<FieldSpec> fieldSpecList = new ArrayList<>();
                for (VariableInfo variableInfo : variableList) {
                    VariableElement variableElement = variableInfo.getVariableElement();
                    // 变量名称(比如：TextView tv 的 tv)
                    String variableName = variableElement.getSimpleName().toString();
                    FieldSpec fieldSpec = FieldSpec.builder(Lifecycer.class, variableName, Modifier.PRIVATE).build();
                    fieldSpecList.add(fieldSpec);

                    constructor.addStatement("this.$L = host.$L", variableName, variableName);

                    method.addStatement("this.$L.onDestory()", variableName);
                }

                // 构建Class
                TypeSpec typeSpec = TypeSpec.classBuilder(typeElement.getSimpleName() + "_LifecyclerInjector")
                        .addModifiers(Modifier.PUBLIC)
                        .addMethod(constructor.build())
                        .addMethod(method.build())
                        .addFields(fieldSpecList)
                        .addSuperinterface(Lifecycer.class)
                        .build();

                // 与目标Class放在同一个包下，解决Class属性的可访问性
                String packageFullName = elementUtils.getPackageOf(typeElement).getQualifiedName().toString();
                JavaFile javaFile = JavaFile.builder(packageFullName, typeSpec)
                        .build();
                // 生成class文件
                javaFile.writeTo(filer);
            }
        } catch (Exception e) {

        }


    }
}
