package com.android.payutil.activity;

import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.payutil.Constants;
import com.android.payutil.R;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Author: Ligang
 * Date: 2020/01/13
 * Description:
 */
public class WxH5PayActivity extends BaseTitleActivity {
    @BindView(R.id.et)
    EditText et;
    @BindView(R.id.tv_wx_h5_pay_demo)
    TextView tv_wx_h5_pay_demo;

    @Override
    protected String provideTitle() {
        return "H5支付";
    }

    @Override
    protected int provideContentViewId() {
        return R.layout.activity_wx_h5_pay;
    }

    @Override
    protected void beforeSetContentView() {

    }

    @Override
    protected void afterSetContentView() {
        tv_wx_h5_pay_demo.setText(Constants.wx_h5_pay);
    }

    @OnClick({R.id.btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn:
                if (!TextUtils.isEmpty(et.getText().toString())) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(et.getText().toString()));
                    startActivity(intent);
                }
                break;
        }
    }
}
