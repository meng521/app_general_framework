package com.android.payutil;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.android.payutil.activity.AlipayActivity;
import com.android.payutil.activity.BaseTitleActivity;
import com.android.payutil.activity.WxH5PayActivity;
import com.android.payutil.adapter.ClassListAdapter;
import com.android.payutil.model.ClassItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class MainActivity extends BaseTitleActivity {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private ClassListAdapter listAdapter;
    private List<ClassItem> classItemList = new ArrayList<>();

    @Override
    protected int provideContentViewId() {
        return R.layout.activity_main;
    }

    @Override
    protected void beforeSetContentView() {

    }

    private void add(String title, Class clz) {
        classItemList.add(ClassItem.newInstance(title, clz));
    }

    private void initClassList() {
        add("阿里支付", AlipayActivity.class);
        add("H5支付", WxH5PayActivity.class);
    }

    @Override
    protected void afterSetContentView() {
        initClassList();
        listAdapter = new ClassListAdapter(classItemList);
        listAdapter.setOnItemClickListener((adapter, view, position) -> {
            jumpTo(listAdapter.getItem(position).getClz());
        });
        recyclerView.setAdapter(listAdapter);
        findViewById(R.id.btn_back).setVisibility(View.GONE);
        findViewById(R.id.tv_back).setVisibility(View.GONE);
    }

    @Override
    protected String provideTitle() {
        return "支付工具";
    }
}
