package com.android.payutil.model;

/**
 * Author: Ligang
 * Date: 2020/01/13
 * Description:
 */
public class ClassItem {
    private String title;
    private Class clz;

    private ClassItem(String title, Class clz) {
        this.title = title;
        this.clz = clz;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Class getClz() {
        return clz;
    }

    public void setClz(Class clz) {
        this.clz = clz;
    }

    public static ClassItem newInstance(String title, Class clz) {
        return new ClassItem(title, clz);
    }
}
