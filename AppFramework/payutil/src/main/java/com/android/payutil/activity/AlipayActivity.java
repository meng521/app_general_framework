package com.android.payutil.activity;

import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.alipay.sdk.app.PayTask;
import com.android.payutil.Constants;
import com.android.payutil.R;
import com.lg.meng.utils.ThreadUtils;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Author: Ligang
 * Date: 2020/01/13
 * Description:
 */
public class AlipayActivity extends BaseTitleActivity {
    @BindView(R.id.et)
    EditText et;
    @BindView(R.id.tv_pay_demo)
    TextView tv_pay_demo;
    @BindView(R.id.rg)
    RadioGroup rg;

    @Override
    protected String provideTitle() {
        return "阿里支付";
    }

    @Override
    protected int provideContentViewId() {
        return R.layout.activity_alipay;
    }

    @Override
    protected void beforeSetContentView() {

    }

    @Override
    protected void afterSetContentView() {
        tv_pay_demo.setText(Constants.ali_app_pay);
    }

    @OnClick({R.id.btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn:
                String payInfo = et.getText().toString();
                if (!TextUtils.isEmpty(payInfo)) {
                    ThreadUtils.runOnThread(() -> pay(payInfo));
                }
                break;
        }
    }

    private void pay(String payInfo) {
        switch (rg.getCheckedRadioButtonId()) {
            case R.id.rb_v1:
                payV1(payInfo);
                break;
            case R.id.rb_v2:
                payV2(payInfo);
                break;
        }
    }

    private void payV1(String payInfo) {
        PayTask payTask = new PayTask(activity);
        payTask.pay(payInfo, true);
    }


    private void payV2(String payInfo) {
        PayTask payTask = new PayTask(activity);
        payTask.payV2(payInfo, true);
    }

}
