package com.android.payutil;

/**
 * Author: Ligang
 * Date: 2020/01/13
 * Description:
 */
public interface Constants {
    String wx_h5_pay = "weixin://wap/pay?prepayid%3Dwx1220154614562318e9591e021469548400&package=352091575&noncestr=1578831346&sign=b385a1a82674f2ebfd4d06e9c9678230";

    String ali_app_pay = "alipay_sdk=alipay-sdk-java-3.7.73.ALL&app_id=2019050864401343&biz_content=%7B%22body%22%3A%22%7B%5C%22amount%5C%22%3A0.01%2C%5C%22payPurpose%5C%22%3A%5C%22RECHARGE_STAR_COIN%5C%22%2C%5C%22paymentCreateTime%5C%22%3A1578907372385%2C%5C%22paymentId%5C%22%3A148%2C%5C%22subject%5C%22%3A%5C%22smaradio%E8%BD%A6%E5%A8%B1%E6%98%9F+%E8%B4%AD%E4%B9%B0%E6%98%9F%E5%B8%81%5C%22%2C%5C%22tradeNumber%5C%22%3A%5C%22AL20200113172252387%5C%22%2C%5C%22userId%5C%22%3A580%7D%22%2C%22out_trade_no%22%3A%22AL20200113172252387%22%2C%22subject%22%3A%22smaradio%E8%BD%A6%E5%A8%B1%E6%98%9F+%E8%B4%AD%E4%B9%B0%E6%98%9F%E5%B8%81%22%2C%22timeout_express%22%3A%2210m%22%2C%22total_amount%22%3A%220.01%22%7D&charset=utf-8&format=json&method=alipay.trade.app.pay&notify_url=http%3A%2F%2Fapi.smaradio.com%2Fapi-v2-user%2Fexternal%2Falipaynotify&sign=qYCloZRX8lAsltdqYUF27DOiJnMCCf5Td4weKV4T2ezfdmQ5fGyJ2MQRcK9qjNP0ZAsvQHmcLJ%2FTVAzOo2i0iA%2FIaRC0z1xiQkqO8UQD2cpMkOZ46S3%2B3Thlhc%2F0fUc8j8y9O4lZkSlw7DjOKu2RUfJ2uqa7782FT8ars2UuJFQ%2BpkN%2B2QeLB5WCPlcKeskdzAxmStgyKJJTP%2F9kH4OdrlVel0fLmA7ZmVU2KGeN8u1%2FbvaHUhtK3YqUNJbqZOHiy7kygnu4G3Zokr2dz1%2B8G50btHnvVPkDzciy%2FGiJY%2BrGbQVEyzrpova%2FXdLPGOq7A9MwIxztsx5fw9cnYkan1w%3D%3D&sign_type=RSA2&timestamp=2020-01-13+17%3A22%3A52&version=1.0";
}
