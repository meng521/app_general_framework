package com.android.payutil.adapter;

import com.android.payutil.R;
import com.android.payutil.model.ClassItem;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * Author: Ligang
 * Date: 2020/01/13
 * Description:
 */
public class ClassListAdapter extends BaseQuickAdapter<ClassItem, BaseViewHolder> {
    public ClassListAdapter(List<ClassItem> data) {
        super(R.layout.item_class_list, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, ClassItem item) {
        helper.setText(R.id.tv_title, item.getTitle());
    }
}
