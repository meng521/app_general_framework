package com.lg.meng.dialog;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.blankj.utilcode.util.ScreenUtils;
import com.lg.meng.R;
import com.lg.meng.base.BaseDialog;

/**
 * Author: Ligang
 * Date: 2019/11/21
 * Description:
 */
public class CommonTipDialog extends BaseDialog implements View.OnClickListener {
    TextView tvDialogTip;
    TextView tvTitle;
    TextView btnDialogCancle;
    TextView btnDialogOk;
    public CommonTipDialog(@NonNull Activity activity) {
        super(activity);
        tvDialogTip = dialogView.findViewById(R.id.tv_dialog_tip);
        tvTitle = dialogView.findViewById(R.id.tv_title);
        btnDialogCancle = dialogView.findViewById(R.id.btn_dialog_cancle);
        btnDialogOk = dialogView.findViewById(R.id.btn_dialog_ok);
        btnDialogCancle.setOnClickListener(this);
        btnDialogOk.setOnClickListener(this);
    }

    @Override
    protected int provideViewId() {
        return R.layout.dialog_common_tip;
    }

    @Override
    protected void initView() {
        setWidth(ScreenUtils.getScreenWidth() * 24 / 30);
    }

    @Override
    protected void initData() {

    }

    public void onClick(View view) {
        if (view.getId() == R.id.btn_dialog_cancle) {
            dismiss();
        } else if (view.getId() == R.id.btn_dialog_ok) {
            dismiss();
        }
    }

    public static CommonTipDialog newInstance(Activity activity) {
        return new CommonTipDialog(activity);
    }


    public CommonTipDialog tip(CharSequence charSequence) {
        tvDialogTip.setText(charSequence);
        return this;
    }

    public CommonTipDialog title(CharSequence charSequence) {
        tvTitle.setText(charSequence);
        return this;
    }

    public CommonTipDialog setCancleButton(OnClickListener clickListener) {
        return setCancleButton(null, clickListener);
    }

    public CommonTipDialog setCancleButton(CharSequence text) {
        return setCancleButton(text, null);
    }

    public CommonTipDialog setCancleButton(CharSequence text, OnClickListener clickListener) {
        btnDialogCancle.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(text)) {
            btnDialogCancle.setText(text);
        }
        if (clickListener != null) {
            btnDialogCancle.setOnClickListener(v -> {
                clickListener.onClick(CommonTipDialog.this, 0);
            });
        } else if (TextUtils.isEmpty(text)) {
            btnDialogCancle.setVisibility(View.GONE);
        }
        return this;
    }

    public CommonTipDialog setOkButton(OnClickListener clickListener) {
        return setOkButton(null, clickListener);
    }

    public CommonTipDialog setOkButton(CharSequence text) {
        return setOkButton(text, null);
    }


    public CommonTipDialog setOkButton(CharSequence text, OnClickListener clickListener) {
        btnDialogOk.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(text)) {
            btnDialogOk.setText(text);
        }
        if (clickListener != null) {
            btnDialogOk.setOnClickListener(v -> {
                clickListener.onClick(CommonTipDialog.this, 0);
            });
        }
        return this;
    }
}
