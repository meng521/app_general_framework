package com.lg.meng.dialog;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.TextView;

import com.lg.meng.R;
import com.lg.meng.base.BaseDialog;

public class LoadingDialog extends BaseDialog {
    public LoadingDialog(@NonNull FragmentActivity activity) {
        super(activity, R.style.LoadingDialog);
        tvLoadingTips = dialogView.findViewById(R.id.tv_loading_tips);
    }

    @Override
    protected int provideViewId() {
        return R.layout.dialog_loading;
    }

    private TextView tvLoadingTips;

    @Override
    protected void initView() {
        setCanceledOnTouchOutside(false);
    }

    @Override
    protected void initData() {

    }

    public LoadingDialog loadingTips(String tips) {
        tvLoadingTips.setVisibility(View.VISIBLE);
        tvLoadingTips.setText(tips);
        return this;
    }
}
