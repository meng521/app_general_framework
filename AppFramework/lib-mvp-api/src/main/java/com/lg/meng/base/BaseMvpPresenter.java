package com.lg.meng.base;

import android.content.Context;
import android.util.Log;

public class BaseMvpPresenter<V> {
    public V view;
    public Context context;

    public V getView() {
        return view;
    }

    public void attachView(Context context, V view) {
        this.context = context;
        this.view = view;
//        Log.e("-=-=-=-=-=-=-=", "attachView() prsenter = " + this + ",context = " + context + ",view = " + view);
    }

    public void detachView() {
//        Log.e("-=-=-=-=-=-=-=", "detachView() prsenter = " + this + ",view = " + view);
        this.view = null;
        this.context = null;
    }

    public boolean isAttachView() {
//        Log.e("-=-=-=-=-=-=-=", "isAttachView() prsenter = " + this + ",view = " + view);
        return this.view != null;
    }


}
