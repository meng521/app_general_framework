package com.lg.meng.utils;

import com.blankj.utilcode.util.ToastUtils;
import com.lg.meng.base.BaseMvpActivity;
import com.tbruyelle.rxpermissions2.RxPermissions;

/**
 * Author: Ligang
 * Date: 2019/12/29
 * Description:
 */
public class PermissionsUtils {
    /**
     * @param activity
     * @param runnable
     * @param permissionTips 读写手机存储 多个用、隔开
     * @param permissions    Manifest.permission.WRITE_EXTERNAL_STORAGE
     */
    public static void permissionRun(BaseMvpActivity activity, Runnable runnable, String permissionTips, String... permissions) {
        new RxPermissions(activity).requestEachCombined(permissions)
                .subscribe(permission -> {
                    if (permission.granted) {
                        runnable.run();
                    } else if (permission.shouldShowRequestPermissionRationale) {
                        ToastUtils.showLong("没有【" + permissionTips.replace("、","或") + "】权限");
                    } else {
                        activity.showPermissionsSetDialog(permissionTips);
                    }
                });
    }
}
