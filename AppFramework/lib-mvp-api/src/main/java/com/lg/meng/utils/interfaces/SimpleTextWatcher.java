package com.lg.meng.utils.interfaces;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Author: Li&Meng
 * Date: 2019/7/31
 * Description:
 */

public class SimpleTextWatcher implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
