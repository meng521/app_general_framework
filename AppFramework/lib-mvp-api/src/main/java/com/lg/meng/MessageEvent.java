package com.lg.meng;

/**
 * CreateBy ligang ; Time : 2019/3/25
 * Description:
 */
public class MessageEvent {
    private int code;
    private Object obj;

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }

    public MessageEvent(int code, Object obj) {
        this.code = code;
        this.obj = obj;
    }

    public MessageEvent(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public static MessageEvent obtainEvent(int code) {
        MessageEvent messageEvent = new MessageEvent(code);
        return messageEvent;
    }
}
