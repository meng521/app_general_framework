package com.lg.meng.utils;

import java.util.List;

/**
 * Author: Ligang
 * Date: 2019/12/13
 * Description:
 */
public class AppTools {
    public static boolean isNull(List<?> list) {
        return list == null || list.size() == 0;
    }

    public static boolean isNotNull(String str) {
        return !isNull(str);
    }

    public static boolean isNull(String str) {
        return str == null || "".equals(str) || "null".equalsIgnoreCase(str);
    }

    public static boolean isAllNull(String... strs) {
        if (strs == null || strs.length == 0) {
            return true;
        }
        for (String str : strs) {
            if (!AppTools.isNull(str)) {
                return false;
            }
        }
        return true;
    }

    public static <T> boolean isNull(T[] array) {
        return array == null || array.length == 0;
    }

    public static <T> int len(T[] array) {
        return array == null ? 0 : array.length;
    }


    public static String toString(String[] stringArray) {
        return toString(stringArray, null);
    }

    public static String toString(String[] stringArray, String defaultValue) {
        if (!AppTools.isNull(stringArray)) {
            StringBuffer sb = new StringBuffer();
            for (String s : stringArray) {
                sb.append(s);
            }
            return sb.toString();
        } else {
            return defaultValue;
        }
    }
}
