package com.lg.meng.dialog;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.blankj.utilcode.util.ScreenUtils;
import com.lg.meng.R;
import com.lg.meng.application.StubApplication;
import com.lg.meng.base.BaseDialog;
import com.lg.meng.utils.MiuiUtils;

public class PermissionsSetDialog extends BaseDialog implements View.OnClickListener {
    TextView tvPermissions;
    private String permissions;
    public PermissionsSetDialog(@NonNull Activity activity) {
        super(activity);
    }

    @Override
    protected int provideViewId() {
        return R.layout.dialog_permissions_set;
    }

    @Override
    protected void initView() {
        setWidth(ScreenUtils.getScreenWidth()*3/4);
        tvPermissions = findViewById(R.id.tv_permissions);
        findViewById(R.id.tv_cancel).setOnClickListener(this);
        findViewById(R.id.tv_set).setOnClickListener(this);
        tvPermissions.setText(String.format(StubApplication.getApplication().getResources().getString(R.string.permissions_set), permissions));
    }

    @Override
    protected void initData() {

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.tv_cancel) {

        } else if (view.getId() == R.id.tv_set) {
            MiuiUtils.jumpToPermissionsEditorActivity(activity);
        }
        dismiss();
    }

    public void show(String permissions) {
        this.permissions = permissions;
        show();
    }
}
