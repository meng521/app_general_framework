package com.lg.meng.utils;

import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import com.lg.meng.receiver.NetBroadcastReceiver;

public class NetBroadcastHelper {
    NetBroadcastReceiver netBroadcastReceiver;
    IntentFilter intentFilter;

    public NetBroadcastHelper(NetBroadcastReceiver.OnNetworkChangeListener onNetworkChangeListener) {
        netBroadcastReceiver = new NetBroadcastReceiver(onNetworkChangeListener);
        intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        intentFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
    }

    public void registNetBroadcastReceiver(Context context) {
        context.registerReceiver(netBroadcastReceiver, intentFilter);
    }

    public void unregistNetBroadcastReceiver(Context context) {
        context.unregisterReceiver(netBroadcastReceiver);
    }
}
