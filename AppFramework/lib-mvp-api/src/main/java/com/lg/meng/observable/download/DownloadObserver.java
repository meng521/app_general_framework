package com.lg.meng.observable.download;



import com.lg.meng.observable.Observer;

import java.io.File;

/**
 * Author: Ligang
 * Date: 2019/5/2
 * Description: 下载回调接口
 */

public interface DownloadObserver extends Observer {
    void onDownloadSuccess(String flagType, FileDownloader downloader, File file);

    void onDownloading(String flagType, FileDownloader downloader, int progress, int total, int precent);

    void onDownloadStart(String flagType, FileDownloader downloader, int fileSize);

    void onDownloadFailed(String flagType, FileDownloader downloader, String msg);

    void onDownloadSpeed(String flagType, FileDownloader downloader, float speed, int remainedTime);

    void onDownloadCancel(String flagType, FileDownloader downloader);
}
