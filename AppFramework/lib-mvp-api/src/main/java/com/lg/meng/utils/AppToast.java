package com.lg.meng.utils;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.lg.meng.R;
import com.lg.meng.application.StubApplication;


/**
 * Author: Ligang
 * Date: 2019/12/11
 * Description:
 */
public class AppToast {
    public static void show(CharSequence charSequence) {
        showShort(charSequence);
    }

    public static void showLong(CharSequence charSequence) {
        Toast toast = Toast.makeText(StubApplication.getApplication(), charSequence, Toast.LENGTH_LONG);
        View view = LayoutInflater.from(StubApplication.getApplication()).inflate(R.layout.view_app_toast, null);
        toast.setView(view);
        TextView textView = (TextView) view;
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        textView.setText(charSequence);
        toast.show();
    }

    public static void showShort(CharSequence charSequence) {
        Toast toast = Toast.makeText(StubApplication.getApplication(), charSequence, Toast.LENGTH_SHORT);
        View view = LayoutInflater.from(StubApplication.getApplication()).inflate(R.layout.view_app_toast, null);
        toast.setView(view);
        TextView textView = (TextView) view;
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        textView.setText(charSequence);
        toast.show();
    }
}
