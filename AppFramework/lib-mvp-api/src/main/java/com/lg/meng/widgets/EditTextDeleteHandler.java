package com.lg.meng.widgets;

import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.lg.meng.utils.interfaces.SimpleTextWatcher;


/**
 * Author: Li&Meng
 * Date: 2019/8/1
 * Description:
 */

public class EditTextDeleteHandler {
    public static void setup(EditText editText, View deleteView) {
        deleteView.setVisibility(View.GONE);
        setup(editText, deleteView, false);
    }

    public static void setup(final EditText editText, View deleteView, final boolean bold) {
        editText.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                super.onTextChanged(s, start, before, count);
                if (TextUtils.isEmpty(s)) {
                    deleteView.setVisibility(View.INVISIBLE);
                    if (bold) {
                        editText.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));//常规
                    }
                } else {
                    deleteView.setVisibility(View.VISIBLE);
                    if (bold) {
                        editText.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));//加粗
                    }
                }
            }
        });
        editText.setOnFocusChangeListener((view, b) -> {
            if (!b) {
                deleteView.setVisibility(View.INVISIBLE);
            } else {
                if (TextUtils.isEmpty(editText.getText().toString())) {
                    deleteView.setVisibility(View.INVISIBLE);
                } else {
                    deleteView.setVisibility(View.VISIBLE);
                }
            }
        });
        deleteView.setOnClickListener(view -> {
            editText.getText().clear();
        });
    }
}
