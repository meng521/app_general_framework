package com.lg.meng.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.util.Log;
import com.blankj.utilcode.util.NetworkUtils;

public class NetBroadcastReceiver extends BroadcastReceiver {

    private OnNetworkChangeListener onNetworkChangeListener;

    public NetBroadcastReceiver(OnNetworkChangeListener onNetworkChangeListener) {
        this.onNetworkChangeListener = onNetworkChangeListener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // 网络状态发生了变化
        if (onNetworkChangeListener == null) {
            return;
        }
        String action = intent.getAction();
        if (ConnectivityManager.CONNECTIVITY_ACTION.equals(action)) {
            onNetworkChangeListener.onNetworkChange();
        } else if (WifiManager.WIFI_STATE_CHANGED_ACTION.equals(action)) {
            int wifiState = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, -1);
            switch (wifiState) {
                case WifiManager.WIFI_STATE_DISABLED:
                    log("logWIFI状态 wifiState:WIFI_STATE_DISABLED");
                    onNetworkChangeListener.onWifiStateDisabled();
                    break;
                case WifiManager.WIFI_STATE_DISABLING:
                    log("logWIFI状态 wifiState:WIFI_STATE_DISABLING");
                    onNetworkChangeListener.onWifiStateDisabling();
                    break;
                case WifiManager.WIFI_STATE_ENABLED:
                    log("logWIFI状态 wifiState:WIFI_STATE_ENABLED");
                    onNetworkChangeListener.onWifiStateEnabled();
                    break;
                case WifiManager.WIFI_STATE_ENABLING:
                    log("logWIFI状态 wifiState:WIFI_STATE_ENABLING");
                    onNetworkChangeListener.onWifiStateEnabling();
                    break;
                case WifiManager.WIFI_STATE_UNKNOWN:
                    log("logWIFI状态 wifiState:WIFI_STATE_UNKNOWN");
                    onNetworkChangeListener.onWifiStateUnknown();
                    break;
                //
            }
        }
    }

    private void log(String log) {
        Log.e("NetBroadcastReceiver===", log);
    }

    public interface OnNetworkChangeListener {
        void onNetworkChange();

        void onWifiStateDisabling();

        void onWifiStateDisabled();

        void onWifiStateEnabling();

        void onWifiStateEnabled();

        void onWifiStateUnknown();
    }
}
