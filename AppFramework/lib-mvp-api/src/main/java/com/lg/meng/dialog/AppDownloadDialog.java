package com.lg.meng.dialog;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.blankj.utilcode.util.ScreenUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.lg.meng.R;
import com.lg.meng.base.BaseDialog;
import com.lg.meng.model.ResponseVersionData;
import com.lg.meng.observable.download.DownloadObserver;
import com.lg.meng.observable.download.FileDownloader;
import com.lg.meng.observable.download.VersionDownloaderManager;

import java.io.File;

/**
 * Author: Li&Meng
 * Date: 2019/8/6
 * Description:
 */

public class AppDownloadDialog extends BaseDialog implements DownloadObserver {
    ProgressBar progressBar;
    TextView tvProgress1;
    TextView tvProgress2;
    private VersionDownloaderManager downloaderManager;

    private boolean showDismissTip = true;

    public AppDownloadDialog(@NonNull Activity activity) {
        super(activity);
    }

    @Override
    protected int provideViewId() {
        return R.layout.dialog_app_download;
    }

    @Override
    protected void initView() {
        progressBar = findViewById(R.id.progressBar);
        tvProgress1 = findViewById(R.id.tv_progress1);
        tvProgress2 = findViewById(R.id.tv_progress2);
        setWidth(ScreenUtils.getScreenWidth() * 8 / 10);
        setCancelable(!isForceUpdate);
        setCanceledOnTouchOutside(!isForceUpdate);
        downloaderManager = VersionDownloaderManager.getInstance();
        setOnDismissListener(dialogInterface -> {
            if (!isForceUpdate && showDismissTip) {
                ToastUtils.showLong("新版本正在后台下载");
            }
        });
    }

    @Override
    protected void initData() {

    }

    ResponseVersionData versionData;
    boolean isForceUpdate;

    public void show(ResponseVersionData versionData, boolean isForceUpdate) {
        this.versionData = versionData;
        this.isForceUpdate = isForceUpdate;
        show();
    }

    public void show(ResponseVersionData versionData) {
        show(versionData, true);
    }

    public static AppDownloadDialog newInstance(Activity activity) {
        return new AppDownloadDialog(activity);
    }

    @Override
    public void onDownloadSuccess(String flagType, FileDownloader downloader, File file) {
        VersionUpdateDialog.newInstance(activity).show(versionData);
        showDismissTip = false;
        dismiss();
    }

    @Override
    public void onDownloading(String flagType, FileDownloader downloader, int progress, int total, int precent) {
        progressBar.setProgress(precent);
        tvProgress1.setText(precent + "%");
        tvProgress2.setText(precent + "/100");
    }

    @Override
    public void onDownloadStart(String flagType, FileDownloader downloader, int fileSize) {

    }

    @Override
    public void onDownloadFailed(String flagType, FileDownloader downloader, String msg) {
        showDismissTip = false;
        dismiss();
    }

    @Override
    public void onDownloadSpeed(String flagType, FileDownloader downloader, float speed, int remainedTime) {

    }

    @Override
    public void onDownloadCancel(String flagType, FileDownloader downloader) {

    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        downloaderManager.deleteObserver(this);
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        downloaderManager.addObserver(this);
    }
}
