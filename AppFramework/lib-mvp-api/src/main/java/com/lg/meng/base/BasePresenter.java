package com.lg.meng.base;

import android.annotation.SuppressLint;
import android.text.TextUtils;

import com.blankj.utilcode.util.NetworkUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.lg.meng.api.BaseServerResponseError;
import com.lg.meng.model.BaseServerResponse;

import java.io.IOException;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Author: Ligang
 * Date: 2019/11/13
 * Description:
 */
public class BasePresenter<V extends BaseMvpView> extends BaseMvpPresenter<V> {
    protected void showLoadingDialog() {
        if (context instanceof BaseMvpActivity) {
            ((BaseMvpActivity) context).showLoadingDialog();
        }
    }

    protected void showLoadingDialog(boolean cancelable) {
        if (context instanceof BaseMvpActivity) {
            ((BaseMvpActivity) context).showLoadingDialog(cancelable);
        }
    }

    protected void showLoadingDialog(boolean cancelable, String tips) {
        if (context instanceof BaseMvpActivity) {
            ((BaseMvpActivity) context).showLoadingDialog(cancelable, tips);
        }
    }

    protected void dissmissLoadingDialog() {
        if (context instanceof BaseMvpActivity) {
            ((BaseMvpActivity) context).dissmissLoadingDialog();
        }
    }

    protected <T extends BaseServerResponse> Observer simpleObserver(Callback<T> callback) {
        return new Observer<T>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(T t) {
                if (isAttachView()) {
                    callback.resultSuccess(t);
                }
            }

            @SuppressLint("MissingPermission")
            @Override
            public void onError(Throwable e) {
                if (isAttachView()) {
                    if (e instanceof BaseServerResponseError) {
                        handleServerReturnError(((BaseServerResponseError) e).getHttpResponse());
                    } else if (e instanceof IOException) {
                        if (!NetworkUtils.isAvailableByPing()) {
                            ToastUtils.showLong("网络异常,请检查网络");
                        } else {
                            ToastUtils.showLong("服务器访问异常");
                        }
                    } else {
                        ToastUtils.showLong(e.getMessage());
                    }
                    callback.resultError(e);
                }
            }

            @Override
            public void onComplete() {

            }
        };
    }

    /**
     * 处理服务器返回的异常
     *
     * @param t
     * @param <T>
     */
    private <T extends BaseServerResponse> void handleServerReturnError(T t) {
        if (t.isShowErrorMsg() && !TextUtils.isEmpty(t.getErrorMsg())) {
            ToastUtils.showLong(t.getErrorMsg());
        }
    }


    public interface Callback<T extends BaseServerResponse> {
        void resultSuccess(T data);

        //        //指为符合返回条件的错误
        void resultError(Throwable e);
    }
}

