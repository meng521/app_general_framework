package com.lg.meng.base;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

import com.lg.meng.utils.MvpInjectHelper;

import java.util.ArrayList;
import java.util.List;

abstract class BaseMvpDialog extends Dialog {
    public List<BaseMvpPresenter> presenterList = new ArrayList<>();
    public Activity activity;
    public BaseMvpDialog dialog;

    public BaseMvpDialog(Activity activity) {
        super(activity);
        dialog = this;
        this.activity = activity;
        MvpInjectHelper.inject(this);
    }

    public BaseMvpDialog(Activity activity, int themeResId) {
        super(activity, themeResId);
        dialog = this;
        this.activity = activity;
        MvpInjectHelper.inject(this);
    }

    protected BaseMvpDialog(Activity activity, boolean cancelable, DialogInterface.OnCancelListener cancelListener) {
        super(activity, cancelable, cancelListener);
        dialog = this;
        this.activity = activity;
        MvpInjectHelper.inject(this);
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        for (BaseMvpPresenter presenter : presenterList) {
            if (presenter != null) {
                presenter.detachView();
            }
        }
    }
}
