package com.lg.meng.utils;


import android.app.Activity;

import com.lg.meng.base.BaseMvpFragment;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author：李刚
 * @Time：2018/4/30
 * @Description：
 */

public class FragmentBackHandler {
    public interface BackHandledInterface {
    }

    private static Map<Activity, FragmentBackHandler> backHandlerMap = new HashMap<>();

    private FragmentBackHandler() {
    }

    public static void regist(Activity activity) {
        if (!backHandlerMap.containsKey(activity)) {
            backHandlerMap.put(activity, new FragmentBackHandler());
        }
    }

    public static void unregist(Activity activity) {
        if (backHandlerMap.containsKey(activity)) {
            backHandlerMap.remove(activity);
        }
    }

    public static final FragmentBackHandler getInstance(Activity activity) {
        if (backHandlerMap.containsKey(activity)) {
            return backHandlerMap.get(activity);
        }
        return null;
    }

    private BaseMvpFragment currentFragment;

    public void setCurrentFragment(BaseMvpFragment baseFragment) {
        currentFragment = baseFragment;
    }

    public BaseMvpFragment getCurrentFragment() {
        return currentFragment;
    }
}
