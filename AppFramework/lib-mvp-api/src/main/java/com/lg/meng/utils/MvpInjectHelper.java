package com.lg.meng.utils;

import android.content.Context;

import java.lang.reflect.Constructor;

public class MvpInjectHelper {
    public static void inject(Context context,Object host) {
        String classFullName = host.getClass().getName() + "_PresenterInjector";
        try {
            Class proxy = Class.forName(classFullName);
            Constructor constructor = proxy.getConstructor(host.getClass());
            constructor.newInstance(host, context);
        } catch (Exception e) {

        }
    }

    public static void inject(Object host) {
        String classFullName = host.getClass().getName() + "_PresenterInjector";
        try {
            Class proxy = Class.forName(classFullName);
            Constructor constructor = proxy.getConstructor(host.getClass());
            constructor.newInstance(host);
        } catch (Exception e) {

        }
    }
}
