package com.lg.meng.observable;

/**
 * Author: Ligang
 * Date: 2019/4/6
 * Description:
 */

public interface Observer {
    /**
     * This method is called whenever the observed object is changed. An
     * application calls an <tt>Observable</tt> object's
     * <code>notifyObservers</code> method to have all the object's
     * observers notified of the change.
     *
     * @param   observable     the observable object.
     * @param   data   an argument passed to the <code>notifyObservers</code>
     *                 method.
     */
//    void update(Observable observable, T data);
}
