package com.lg.meng.dialog;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ScreenUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.lg.meng.R;
import com.lg.meng.base.BaseDialog;
import com.lg.meng.model.ResponseVersionData;
import com.lg.meng.observable.download.NewVersionDownloadService;
import com.lg.meng.observable.download.VersionDownloaderManager;

import java.io.File;


public class VersionUpdateDialog extends BaseDialog implements View.OnClickListener {
    TextView appUpdateContent;
    TextView appUpdateCancel;
    TextView appUpdateTitle;

    public VersionUpdateDialog(Activity activity) {
        super(activity);
    }

    @Override
    protected int provideViewId() {
        return R.layout.dialog_app_update;
    }

    @Override
    protected void initView() {
        setWidth(ScreenUtils.getScreenWidth() * 22 / 30);
        appUpdateContent = findViewById(R.id.app_update_content);
        appUpdateCancel = findViewById(R.id.app_update_id_cancel);
        appUpdateTitle = findViewById(R.id.app_update_title);
        findViewById(R.id.app_update_id_ok).setOnClickListener(this);
        findViewById(R.id.app_update_id_cancel).setOnClickListener(this);
        if (versionData.isIsForceUpdate()) {
            setCancelable(false);
            setCanceledOnTouchOutside(false);
            appUpdateCancel.setText("退出应用");
            appUpdateTitle.setText("强制更新");
        }
    }

    @Override
    protected void initData() {
        appUpdateContent.setText(versionData.getContent().replace("#", "\n"));
    }

    public void onClick(View view) {
        if (view.getId() == R.id.app_update_id_ok) {
            //判断是否已经下载
            String filePath = VersionDownloaderManager.getInstance().buildFilePath(versionData);
            Log.e("VersionUpdateDialog", "filePath:" + filePath);
            File file = new File(filePath);
//                isDownloadComplete = file.exists() && MD5Util.getFileMD5String(file).equalsIgnoreCase(versionData.getMd5Code());
            isDownloadComplete = file.exists();
            if (isDownloadComplete) { //去安装
                AppUtils.installApp(filePath);
            } else {
                //开始下载
                Intent serviceIntent = new Intent(activity, NewVersionDownloadService.class);
                serviceIntent.putExtra("versionData", versionData);
                activity.startService(serviceIntent);

                if (versionData.isIsForceUpdate()) {
//                        //强制更新
                    AppDownloadDialog.newInstance(activity).show(versionData);
                    dismiss();
                } else {
                    AppDownloadDialog.newInstance(activity).show(versionData,false);
                    dismiss();
                }
            }
        } else if (view.getId() == R.id.app_update_id_cancel) {
            if (versionData.isIsForceUpdate()) {
                activity.finish();
                android.os.Process.killProcess(android.os.Process.myPid());
            } else {
                dismiss();
            }
        }
    }

    private boolean isDownloadComplete; //是否已经下载完成
    private ResponseVersionData versionData;

    public void show(ResponseVersionData versionData) {
        this.versionData = versionData;
        show();
    }

    public static VersionUpdateDialog newInstance(Activity activity) {
        return new VersionUpdateDialog(activity);
    }

}
