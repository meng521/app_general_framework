package com.lg.meng.base;


import com.lg.meng.model.BaseServerResponse;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by ligang on 2017/11/22.
 * description：下拉刷新列表界面的Presenter基类
 */

public abstract class PaginationListPresenter<P extends BaseServerResponse, T, V extends IPaginationListView<P, T>> extends BasePresenter<V> {
    private boolean noMoreData;
    protected int page = 1;
    protected int pageZise = 10;
    private int pageTem;

    public boolean isNoMoreData() {
        return noMoreData;
    }

    private boolean isInitLoad = true;

    public abstract void loadData();

    public boolean isShowProgressDialog = true;

    private boolean initDataFailed;

    public boolean isInitDataFailed() {
        return initDataFailed;
    }

    protected abstract List<T> getList(P datas);

    protected void loadData(Observable<P> observable) {
        if (getView() == null) {
            return;
        }
        if (isInitLoad) { //如果之前没有成功加载过数据
//            getView().showInitLoadView();
//            if (isShowProgressDialog)
//                showProgressDialog();
        }
        observable.subscribe(simpleObserver(new Callback<P>() {
            @Override
            public void resultSuccess(P p) {
                if (isInitLoad) {
                    initDataFailed = false;
                }
                List<T> datas = getList(p);
                if (isRefresh()) { //如果是刷新数据或初始化数据
                    if (datas != null && datas.size() != 0) {
                        getView().refreshDataSuccess(p, datas);
                        recordLast(datas);
                        isInitLoad = false;
//                        getView().hideNoNetworkView();
//                        getView().hideDataEmptyView();
                        noMoreData = false;
                    } else {
                        getView().dataEmpty();
//                        getView().showDataEmptyView();
                        noMoreData = true;
                    }
//                    getView().hideInitLoadView();
                } else {
                    if (datas != null && datas.size() != 0) {
                        getView().loadMoreDataSuccess(p, datas);
                        recordLast(datas);
                        noMoreData = false;
                    } else {
                        getView().dataNoMore();
                        noMoreData = true;
                    }
                }
            }

            @Override
            public void resultError(Throwable e) {
                if (isInitLoad) {
                    initDataFailed = true;
                }
                noMoreData = true;
                if (isRefresh()) { //如果是刷新数据或初始化数据
                    getView().refreshDataFailed(e);
//                    getView().hideInitLoadView();
                    if (pageTem != 1) {
                        page = pageTem;
                        pageTem = 1;
                    }
                } else {
                    getView().loadMoreDataFailed(e);
                }
            }
        }));
    }

    protected void recordLast(List<T> datas) {
        page++;
        pageTem = page;
    }

    public void refreshData() {
        noMoreData = false;
        resetRecord();
        loadData();
    }

    protected void refreshData(Observable<P> observable) {
        loadData(observable);
    }

    protected void resetRecord() {
        page = 1;
    }

    protected boolean isRefresh() {
        return page == 1;
    }

}
