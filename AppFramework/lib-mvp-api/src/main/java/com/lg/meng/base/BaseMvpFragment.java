package com.lg.meng.base;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lg.meng.MessageEvent;
import com.lg.meng.utils.MvpInjectHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by Administrator on 2019/3/31.
 */

public abstract class BaseMvpFragment extends Fragment {
    protected Activity activity;
    public List<BaseMvpPresenter> presenterList = new ArrayList<>();

    protected abstract int provideContentViewId();

    protected View mView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int layoutId = provideContentViewId();
        if (layoutId == 0) {
            throw new RuntimeException("please provide a layout id !!");
        }
        mView = inflater.inflate(layoutId, null);
        mView.setClickable(true);
        ButterKnife.bind(this, mView);
        return mView;
    }

    protected abstract void initViews();

    protected abstract void initData();

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = getActivity();
        MvpInjectHelper.inject(this);
        EventBus.getDefault().register(this);
        initViews();
        initViews(savedInstanceState);
        initData();
    }

    protected void initViews(Bundle savedInstanceState) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        exeDestory();
    }


    protected void exeDestory() {
        for (BaseMvpPresenter presenter : presenterList) {
            if (presenter != null) {
                presenter.detachView();
            }
        }
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent messageEvent) {
        Log.e("=====", "----messageEvent");
    }

    protected <V extends View> V findViewById(int id) {
        return mView.findViewById(id);
    }

    /**
     * 所有继承BackHandledFragment的子类都将在这个方法中实现物理Back键按下后的逻辑
     * FragmentActivity捕捉到物理返回键点击事件后会首先询问Fragment是否消费该事件
     * 如果没有Fragment消息时FragmentActivity自己才会消费该事件
     */
    public boolean onBackPressed() {
        return false;
    }
}
