package com.lg.meng.api;

import com.lg.meng.model.BaseServerResponse;

/**
 * Author: Ligang
 * Date: 2019/11/25
 * Description:
 */
public class BaseServerResponseError extends Throwable {

    private BaseServerResponse t;

    public <T extends BaseServerResponse> BaseServerResponseError(T t) {
        this.t = t;
    }

    public BaseServerResponse getHttpResponse() {
        return t;
    }

    @Override
    public String getMessage() {
        if (t.isShowErrorMsg()) {
            return t.getErrorMsg();
        }
        return "";
    }
}
