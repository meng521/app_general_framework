package com.lg.meng.dialog;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.FileProvider;
import android.view.View;

import com.lg.meng.R;
import com.lg.meng.application.StubApplication;
import com.lg.meng.base.BaseDialog;
import com.lg.meng.base.BaseMvpActivity;
import com.lg.meng.utils.PermissionsUtils;

import java.io.File;

public class SelectPhotoDialog extends BaseDialog implements View.OnClickListener {
    public static final String TEM_PIC_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator
            + StubApplication.getApplication().getPackageName() + File.separator + "photo" + File.separator + "tem.png";

    public SelectPhotoDialog(@NonNull FragmentActivity activity) {
        super(activity);
    }

    @Override
    protected int provideViewId() {
        return R.layout.lib_dialog_select_photo;
    }

    @Override
    protected void initView() {
        fullWidth().bottom();
        getWindow().setWindowAnimations(R.style.UpDownDialogAnimation);
        setOnClickListener(R.id.tv_camera, this);
        setOnClickListener(R.id.tv_local, this);
        setOnClickListener(R.id.tv_cancel, this);
    }

    @Override
    protected void initData() {
        File file = new File(TEM_PIC_PATH);
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
    }

    private PermissionsSetDialog permissionsSetDialog;

    private void showPermissionsSetDialog(String permissions) {
        if (permissionsSetDialog == null) {
            permissionsSetDialog = new PermissionsSetDialog(activity);
        }
        permissionsSetDialog.show(permissions);
    }

    private void dismissPermissionsSetDialog() {
        if (permissionsSetDialog != null) {
            permissionsSetDialog.dismiss();
        }
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.tv_camera) {
            PermissionsUtils.permissionRun((BaseMvpActivity) activity, () -> {
                selectPicFromCamera();
            }, "摄像头、读写手机存储", Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA);
        } else if (id == R.id.tv_local) {
            PermissionsUtils.permissionRun((BaseMvpActivity) activity, () -> {
                selectPicFromLocal();
            }, "读写手机存储", Manifest.permission.WRITE_EXTERNAL_STORAGE);
        } else if (id == R.id.tv_cancel) {
            dismiss();
        }
    }

    private void selectPicFromCamera() {
        dismiss();
        File cameFile = new File(TEM_PIC_PATH);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT >= 24) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(activity, activity.getPackageName() + ".fileprovider", cameFile));
        } else {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(cameFile));
        }
        activity.startActivityForResult(intent, REQUEST_CODE_CAMERA);
    }

    private void selectPicFromLocal() {
        dismiss();
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        activity.startActivityForResult(Intent.createChooser(intent, "选择图片"), REQUEST_CODE_LOCAL);
    }

    public static final int REQUEST_CODE_CAMERA = 26;
    public static final int REQUEST_CODE_LOCAL = 27;
}
