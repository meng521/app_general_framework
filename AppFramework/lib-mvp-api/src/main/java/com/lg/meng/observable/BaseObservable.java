package com.lg.meng.observable;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Author: Ligang
 * Date: 2019/4/19
 * Description:
 */

public abstract class BaseObservable<observer extends Observer> extends Observable<observer> {

    protected <T> io.reactivex.Observable createMainThreadObservable(T t) {
        return io.reactivex.Observable.just(t).subscribeOn(AndroidSchedulers.mainThread());
    }

    protected Disposable createMainThreadDisposable(Consumer consumer) {
        return io.reactivex.Observable.just(1).subscribeOn(AndroidSchedulers.mainThread()).subscribe(consumer);
    }

    protected void release(){

    }

}
