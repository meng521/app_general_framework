package com.lg.meng.widgets;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.lg.meng.R;

/**
 * Author: Ligang
 * Date: 2019/11/15
 * Description:
 */

public class SimpleEmpty {
    public static final int STATE_LOADING = 1 << 1;
    public static final int STATE_FAIL = 1 << 2;
    public static final int STATE_NO_CONTENT = 1 << 3;
    public static final int STATE_GONE = 1 << 4;
    public View emptyView;
    ImageView imgRecyclerEmpty;
    TextView tvRecyclerEmpty;

    private String messageLoading = "玩命加载中...";
    private String messageFailed = "加载失败了,点击刷新";
    private String messageNoContent = "没有内容哦~~~";

    public void setMessageLoading(String messageLoading) {
        this.messageLoading = messageLoading;
    }

    public void setMessageFailed(String messageFailed) {
        this.messageFailed = messageFailed;
    }

    public void setMessageNoContent(String messageNoContent) {
        this.messageNoContent = messageNoContent;
    }

    public SimpleEmpty(Context context, View.OnClickListener onClickListener) {
        this(context, LayoutInflater.from(context).inflate(R.layout.lib_layout_recycler_empty, null), onClickListener);
    }

    public SimpleEmpty(Context context, View emptyView, View.OnClickListener onClickListener) {
        this.emptyView = emptyView;
        imgRecyclerEmpty = emptyView.findViewById(R.id.img_recycler_empty);
        tvRecyclerEmpty = emptyView.findViewById(R.id.tv_recycler_empty);
        setState(STATE_LOADING);
        emptyView.setOnClickListener(v -> {
            setState(STATE_LOADING);
            if (onClickListener != null) {
                onClickListener.onClick(v);
            }
        });
    }

    public void setState(int state) {
        emptyView.setVisibility(View.VISIBLE);
        switch (state) {
            case STATE_LOADING:
                imgRecyclerEmpty.setVisibility(View.GONE);
                tvRecyclerEmpty.setText(messageLoading);
                emptyView.setEnabled(false);
                emptyView.setVisibility(View.VISIBLE);
                break;
            case STATE_NO_CONTENT:
                imgRecyclerEmpty.setVisibility(View.VISIBLE);
                imgRecyclerEmpty.setImageResource(R.drawable.lib_icon_empty);
                tvRecyclerEmpty.setText(messageNoContent);
                emptyView.setEnabled(false);
                emptyView.setVisibility(View.VISIBLE);
                break;
            case STATE_FAIL:
                imgRecyclerEmpty.setVisibility(View.VISIBLE);
                imgRecyclerEmpty.setImageResource(R.drawable.lib_icon_load_failed);
                tvRecyclerEmpty.setText(messageFailed);
                emptyView.setEnabled(true);
                emptyView.setVisibility(View.VISIBLE);
                break;
            case STATE_GONE:
                emptyView.setVisibility(View.GONE);
                break;
        }
    }
}
