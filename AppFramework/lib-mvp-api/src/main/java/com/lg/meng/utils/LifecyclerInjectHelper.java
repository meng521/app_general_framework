package com.lg.meng.utils;

import com.lg.meng.Lifecycer;

import java.lang.reflect.Constructor;

/**
 * Author: Ligang
 * Date: 2019/5/6
 * Description:
 */

public class LifecyclerInjectHelper {
    public static Lifecycer inject(Object host) {
        String classFullName = host.getClass().getName() + "_LifecyclerInjector";
        try {
            Class proxy = Class.forName(classFullName);
            Constructor constructor = proxy.getConstructor(host.getClass());
            Object newInstance = constructor.newInstance(host);
            return (Lifecycer) newInstance;
        } catch (Exception e) {

        }
        return null;
    }
}
