package com.lg.meng.observable.message;

import android.util.SparseBooleanArray;
import android.view.View;

/**
 * Author: Ligang
 * Date: 2019/12/29
 * Description:
 */
public class BaseMessageWrapper implements MessageObserver {
    /*
        因为各个消息都会让这里处理，所以记录这些消息的状态，好做处理
     */
    private final SparseBooleanArray mArray = new SparseBooleanArray();
    @Override
    public void onMessageUpdate(AppMessage message) {
        /*
            先把该消息放进去
         */
        mArray.put(message.getId(), message.isRemind());
        /*
            计算可见性
         */
        int visibility = computeVisibility();

    }

    /*
        计算 view 的可见性
     */
    public int computeVisibility() {
        boolean res = false;
        for (int i = 0; i < mArray.size(); i++) {
            res |= mArray.valueAt(i);
        }
        return res ? View.VISIBLE : View.GONE;
    }


}
