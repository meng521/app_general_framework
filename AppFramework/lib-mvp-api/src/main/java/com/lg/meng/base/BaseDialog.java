package com.lg.meng.base;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import butterknife.ButterKnife;

import com.lg.meng.R;

/**
 * createBy: Ligang
 * date: 2018/8/26
 * description:
 */
public abstract class BaseDialog extends BaseMvpDialog {
    protected View dialogView;
    protected WindowManager.LayoutParams params;

    public BaseDialog(@NonNull Activity activity) {
        this(activity, R.style.CustomDialog);
    }

    public BaseDialog(Activity activity, int style) {
        super(activity, style);
        init(activity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        initData();
    }

    private void init(Context context) {
        dialogView = LayoutInflater.from(context).inflate(provideViewId(), null);
        ButterKnife.bind(this, dialogView);
        setContentView(dialogView);

    }

    protected abstract int provideViewId();

    protected abstract void initView();

    protected abstract void initData();

    protected void screenNotDark() {
        Window window = getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.dimAmount = 0f;
        window.setAttributes(params);
    }

    /**
     * 宽充满屏幕
     *
     * @return
     */
    public BaseDialog fullWidth() {
        if (params == null) {
            params = getWindow().getAttributes();
        }
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(params);
        return this;
    }

    /**
     * 高充满屏幕
     *
     * @return
     */
    public BaseDialog fullHeight() {
        if (params == null) {
            params = getWindow().getAttributes();
        }
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(params);
        return this;
    }

    /**
     * 高充满屏幕
     *
     * @return
     */
    public BaseDialog fullScreen() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        return this;
    }


    /**
     * 位置在屏幕最底部
     *
     * @return
     */
    public BaseDialog bottom() {
        if (params == null) {
            params = getWindow().getAttributes();
        }
        params.gravity = Gravity.BOTTOM;
        getWindow().setAttributes(params);
        return this;
    }

    /**
     * 位置在右上角
     *
     * @return
     */
    public BaseDialog rightTop() {
        if (params == null) {
            params = getWindow().getAttributes();
        }
        params.gravity = Gravity.RIGHT | Gravity.TOP;
        getWindow().setAttributes(params);
        return this;
    }

    /**
     * 位置在右上角
     *
     * @return
     */
    public BaseDialog leftTop() {
        if (params == null) {
            params = getWindow().getAttributes();
        }
        params.gravity = Gravity.LEFT | Gravity.TOP;
        getWindow().setAttributes(params);
        return this;
    }
    /**
     * 设置宽度
     *
     * @return
     */
    public BaseDialog setWidth(int width) {
        if (params == null) {
            params = getWindow().getAttributes();
        }
        params.width = width;
        getWindow().setAttributes(params);
        return this;
    }
    /**
     * 设置高度
     *
     * @return
     */
    public BaseDialog setHeight(int height) {
        if (params == null) {
            params = getWindow().getAttributes();
        }
        params.height = height;
        getWindow().setAttributes(params);
        return this;
    }

    @Override
    public void show() {
        if (!isShowing()) {
            super.show();
        }
    }

    public BaseDialog cancelable(boolean cancelable) {
        setCancelable(cancelable);
        return this;
    }

    public BaseDialog canceledOnTouchOutside(boolean canceledOnTouchOutside) {
        setCanceledOnTouchOutside(canceledOnTouchOutside);
        return this;
    }

    public BaseDialog dismissListener(OnDismissListener dismissListener) {
        setOnDismissListener(dismissListener);
        return this;
    }

    protected void setOnClickListener(int id, View.OnClickListener clickListener){
        findViewById(id).setOnClickListener(clickListener);
    }
}
