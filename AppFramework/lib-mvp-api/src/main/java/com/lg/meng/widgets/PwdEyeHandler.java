package com.lg.meng.widgets;

import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;

import com.lg.meng.utils.interfaces.SimpleTextWatcher;


/**
 * Author: Li&Meng
 * Date: 2019/8/1
 * Description:
 */

public class PwdEyeHandler {

    public static void setup(final EditText etPwd, View eyeView) {
        eyeView.setVisibility(View.GONE);
        eyeView.setOnClickListener(view -> {
            if (view.isSelected()) {
                etPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
            } else {
                etPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
            etPwd.setSelection(etPwd.getText().toString().length());
            view.setSelected(!view.isSelected());
        });

        etPwd.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                super.onTextChanged(s, start, before, count);
                if (TextUtils.isEmpty(s)) {
                    eyeView.setVisibility(View.GONE);
                } else {
                    eyeView.setVisibility(View.VISIBLE);
                }
            }
        });
    }
}
