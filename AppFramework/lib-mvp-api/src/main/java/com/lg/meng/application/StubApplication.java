package com.lg.meng.application;

import android.app.Application;

/**
 * Author: Ligang
 * Date: 2019/12/12
 * Description:
 */
public class StubApplication {
    private static Application application;

    public static void init(Application application) {
        StubApplication.application = application;
    }

    public static Application getApplication() {
        if (application == null) {
            throw new RuntimeException("请在Application中调用StubApplication.init()初始化");
        }
        return application;
    }
}
