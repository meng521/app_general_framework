package com.lg.meng.base;

import java.util.List;

/**
 * Created by ligang on 2017/11/22.
 * description：
 */

public interface IPaginationListView<P,T> extends BaseMvpView {
    void refreshDataSuccess(P p, List<T> datas);
    void loadMoreDataSuccess(P p, List<T> datas);
    void dataEmpty();
    void dataNoMore();
    void refreshDataFailed(Throwable e);
    void loadMoreDataFailed(Throwable e);
//    void showInitLoadView();
//    void hideInitLoadView();
//    void showNoNetworkView(boolean isInitLoad);
//    void hideNoNetworkView();
//    void showDataEmptyView();
//    void hideDataEmptyView();
}
