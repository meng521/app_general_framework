package com.lg.meng.utils;

import com.lg.meng.application.StubApplication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Author: Ligang
 * Date: 2020/01/05
 * Description:
 */
public class FileUtils {
    public static String readAssetFile(String fileName) {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(StubApplication.getApplication().getAssets().open(fileName)))) {
            String line;
            StringBuffer sb = new StringBuffer();
            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append("\n");
            }
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }
}
