package com.lg.meng.model;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;

public class NotificationModel {

    public NotificationModel() {
    }

    private Notification notification = null;

    private NotificationManager manager = null;

    private RemoteViews notificationView = null;

    private Context mContext = null;

    private NotificationCompat.Builder mBuilder = null;

    private PendingIntent mIntent = null;


    public Context getmContext() {
        return mContext;
    }

    public void cancelNotify() {
        if (manager != null) {
            manager.cancel(notifyId);
        }
    }

    /**
     * 首先设置context
     *
     * @param mContext
     * @return
     */
    public NotificationModel setmContext(Context mContext) {
        this.mContext = mContext;
        return this;
    }

    public RemoteViews getNotificationView() {
        return notificationView;
    }

    public Notification getNotification() {
        return notification;
    }

    public NotificationModel setNotificationView(int layoutId) {
        if (mContext == null) return this;
        notificationView = new RemoteViews(mContext.getPackageName(), layoutId);
        if (notification != null) {
            notification.contentView = notificationView;

        }
        return this;
    }

    public NotificationModel setIcon() {
        if (notification == null) return this;
        notification.icon = android.R.drawable.stat_sys_download;
        return this;
    }

    public NotificationModel setPendIntent(PendingIntent pendingIntent) {
        if (notification == null) {
            return this;
        }
        notification.contentIntent = pendingIntent;
        return this;
    }

    public NotificationModel setTextViewText(int viewId, String text) {
        if (notificationView != null)
            notificationView.setTextViewText(viewId, text);
        return this;
    }

    public NotificationModel setImageViewResource(int viewId, int resId) {
        if (notificationView != null)
            notificationView.setImageViewResource(viewId, resId);
        return this;
    }

    public NotificationModel setOnClick(int viewId, Intent broadcastIntent, int requestCode, int flags) {
        if (notificationView != null) {
            PendingIntent broadcast = PendingIntent.getBroadcast(mContext, requestCode, broadcastIntent, flags);
            notificationView.setOnClickPendingIntent(viewId, broadcast);
        }
        return this;
    }

    public NotificationModel setProgress(int viewId, int max, int progress, boolean indeterminate) {
        if (notificationView != null) {
            notificationView.setProgressBar(viewId, max, progress, indeterminate);
        }
        return this;
    }

    public NotificationManager getManager() {
        return manager;
    }

    public NotificationModel setManager() {
        if (mContext == null) return this;
        this.manager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        return this;
    }

    public NotificationModel setDefaults() {
        if (notification != null)
            notification.defaults = DEFAULT_LIGHTS;
        return this;
    }

    /**
     * 调用之前确保setManager 和 setNotification已调用
     */
    public void showNotification() {
        if (manager == null || notification == null) {
            return;
        }
        manager.notify(notifyId, notification);
    }

    /**
     * 调用之前确保setContext 已调用
     *
     * @return
     */
    public NotificationModel setBuilder() {
        if (mContext == null) return this;
        mBuilder = new NotificationCompat.Builder(mContext);
        return this;
    }

    /**
     * 调用之前确保setBuilder 已调用
     *
     * @return
     */
    public NotificationModel setNotification() {
        if (mBuilder == null) return this;
        notification = mBuilder.build();
        return this;

    }

    /**
     * notification 的属性
     */
    private int flags;


    public static final int FLAG_SHOW_LIGHTS = Notification.FLAG_SHOW_LIGHTS;             //三色灯提醒，在使用三色灯提醒时候必须加该标志符
    public static final int FLAG_ONGOING_EVENT = Notification.FLAG_ONGOING_EVENT;          //发起正在运行事件（活动中）
    public static final int FLAG_INSISTENT = Notification.FLAG_INSISTENT;   //让声音、振动无限循环，直到用户响应 （取消或者打开）
    public static final int FLAG_ONLY_ALERT_ONCE = Notification.FLAG_ONLY_ALERT_ONCE; //发起Notification后，铃声和震动均只执行一次
    public static final int FLAG_AUTO_CANCEL = Notification.FLAG_AUTO_CANCEL;     //用户单击通知后自动消失
    public static final int FLAG_NO_CLEAR = Notification.FLAG_NO_CLEAR;         //只有全部清除时，Notification才会清除 ，不清楚该通知(QQ的通知无法清除，就是用的这个)
    public static final int FLAG_FOREGROUND_SERVICE = Notification.FLAG_FOREGROUND_SERVICE;   //表示正在运行的服务


    /**
     * notification build 的属性
     */
    private String contentTitle = "";  // 通知栏标题
    private String contentText = "";   // 通知栏显示内容
    private String ticker = "";  // 首次弹出通知栏
    private long when = 0; // 通知显示时间
    private int smallIcon = -1; // 通知小图标
    private int notifyId = 827;
    private int priority; //权限
    /**
     * 设置为ture，表示它为一个正在进行的通知。他们通常是用来表示一个后台任务,
     * 用户积极参与(如播放音乐)或以某种方式正在等待,因此占用设备(如一个文件下载,同步操作,主动网络连接)
     */
    private boolean ongoing;
    /**
     * 向通知添加声音、闪灯和振动效果的最简单、最一致的方式是使用当前的用户默认设置，使用defaults属性，可以组合
     */
    private int defaults;

    /**
     * 自动关闭
     */
    private boolean autoCancel;
    /**
     * 设置通知集合的数量
     */
    private int number;

    public void setNumber(int number) {
        this.number = number;
    }

    public void setAutoCancel(boolean autoCancel) {
        this.autoCancel = autoCancel;
    }

    public NotificationModel setDefaults(int defaults) {
        this.defaults = defaults;
        if (mBuilder != null)
            mBuilder.setDefaults(defaults);
        return this;
    }

    public NotificationModel setOngoing(boolean ongoing) {
        if (mBuilder != null)
            mBuilder.setOngoing(ongoing);
        this.ongoing = ongoing;
        return this;
    }

    public NotificationModel setPriority(int priority) {
        this.priority = priority;
        if (mBuilder != null)
            mBuilder.setPriority(priority);
        return this;
    }

    public void setFlags(int flags) {
        this.flags = flags;
    }

    /**
     * Notification 的权限
     */
    public static final int PRIORITY_DEFAULT = Notification.PRIORITY_DEFAULT; //默认优先级用于没有特殊优先级分类的通知。
    public static final int PRIORITY_LOW = Notification.PRIORITY_LOW; //低优先级可以通知用户但又不是很紧急的事件。
    public static final int PRIORITY_MIN = Notification.PRIORITY_MIN; //用于后台消息 (例如天气或者位置信息)。最低优先级通知将只在状态栏显示图标，只有用户下拉通知抽屉才能看到内容。
    public static final int PRIORITY_HIGH = Notification.PRIORITY_HIGH; //高优先级用于重要的通信内容，例如短消息或者聊天，这些都是对用户来说比较有兴趣的。
    public static final int PRIORITY_MAX = Notification.PRIORITY_MAX; //重要而紧急的通知，通知用户这个事件是时间上紧迫的或者需要立即处理的。

    /**
     * Notification 的默认效果
     */
    public static final int DEFAULT_ALL = Notification.DEFAULT_ALL; // 所有默认
    public static final int DEFAULT_SOUND = Notification.DEFAULT_SOUND;  // 默认声音
    public static final int DEFAULT_VIBRATE = Notification.DEFAULT_VIBRATE; // 默认震动
    public static final int DEFAULT_LIGHTS = Notification.DEFAULT_LIGHTS; // 默认灯光

    public int getNotifyId() {
        return notifyId;
    }

    public NotificationModel setNotifyId(int notifyId) {
        this.notifyId = notifyId;
        return this;
    }

    public String getContentTitle() {
        return contentTitle;
    }

    public NotificationModel setContentTitle(String contentTitle) {
        this.contentTitle = contentTitle;
        if (mBuilder != null)
            mBuilder.setContentTitle(contentTitle);
        return this;
    }

    public String getContentText() {
        return contentText;
    }

    public NotificationModel setContentText(String contentText) {
        this.contentText = contentText;
        if (mBuilder != null)
            mBuilder.setContentText(contentText);
        return this;
    }

    public String getTicker() {
        return ticker;
    }

    public NotificationModel setTicker(String ticker) {
        this.ticker = ticker;
        if (mBuilder != null)
            mBuilder.setTicker(ticker);
        return this;
    }

    public long getWhen() {
        return when;
    }

    public NotificationModel setWhen(long when) {
        this.when = when;
        if (mBuilder != null)
            mBuilder.setWhen(when);
        return this;
    }

    public int getSmallIcon() {
        return smallIcon;
    }

    public NotificationModel setSmallIcon(int smallIcon) {
        this.smallIcon = smallIcon;
        if (mBuilder != null)
            mBuilder.setSmallIcon(smallIcon);

        return this;
    }

    public NotificationModel setIcon(int id) {

        return this;
    }

    public NotificationModel setProgress(int max, int progress, boolean indeterminate) {
        if (mBuilder != null)
            mBuilder.setProgress(max, progress, indeterminate);
        return this;
    }

    public NotificationModel setPendingIntent(PendingIntent pendingIntent) {
        if (mBuilder != null) {
            mBuilder.setContentIntent(pendingIntent);
        }
        return this;
    }

    /**
     * mBuilder.setVibrate(new long[] {0,300,500,700});
     * mBuilder.build().vibrate = new long[] {0,300,500,700};
     * 实现效果：延迟0ms，然后振动300ms，在延迟500ms，接着在振动700ms。
     *
     * mBuilder.setLights(0xff0000ff, 300, 0)
     * Notification notify = mBuilder.build();
     * notify.flags = Notification.FLAG_SHOW_LIGHTS;
     * notify.ledARGB = 0xff0000ff;
     * notify.ledOnMS = 300;
     * notify.ledOffMS = 300;
     *
     * //获取默认铃声
     * mBuilder.setDefaults(Notification.DEFAULT_SOUND)
     * //获取自定义铃声
     * mBuilder.setSound(Uri.parse("file:///sdcard/xx/xx.mp3"))
     * //获取Android多媒体库内的铃声
     * mBuilder.setSound(Uri.withAppendedPath(Audio.Media.INTERNAL_CONTENT_URI, "5"))
     *
     *
     *  方法：setProgress(int max, int progress,boolean indeterminate)
     *  属性：max:进度条最大数值  、progress:当前进度、indeterminate:表示进度是否不确定，true为不确定，如下第3幅图所示  ，false为确定下第1幅图所示
     *  功能：设置带进度条的通知，可以在下载中使用
     *  效果图如下：
     *   注意：此方法在4.0及以后版本才有用，如果为早期版本：需要自定义通知布局，其中包含ProgressBar视图
     *   使用：如果为确定的进度条：调用setProgress(max, progress, false)来设置通知，在更新进度的时候在此发起通知更新progress，并且在下载完成后要移除进度条，通过调用setProgress(0, 0, false)既可。
     *   如果为不确定（持续活动）的进度条，这是在处理进度无法准确获知时显示活动正在持续，所以调用setProgress(0, 0, true) ，操作结束时，调用setProgress(0, 0, false)并更新通知以移除指示条
     */
}
