package com.lg.meng.observable.message;

/**
 * Author: Ligang
 * Date: 2019/4/7
 * Description: 用于 提醒用户有更新等
 */

public class AppMessage {
    private int id; //app 内部消息id

    private boolean remind; //是否提醒用户
    private Object data; //消息数据

    public AppMessage() {
    }

    public AppMessage(int id) {
        this.id = id;
    }

    public AppMessage(int id, boolean remind) {
        this.id = id;
        this.remind = remind;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isRemind() {
        return remind;
    }

    public void setRemind(boolean remind) {
        this.remind = remind;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AppMessage)) return false;

        AppMessage message = (AppMessage) o;

        return id == message.id;

    }

    @Override
    public int hashCode() {
        return id;
    }

    public interface AppMessageId {
        /**
         * app 更新
         */
        int ID_APP_UPDATE = 0x02;
        /**
         * 用户新消息
         */
        int ID_USER_MESSAGE = 0x03;
    }
}
