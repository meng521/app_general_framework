package com.lg.meng.utils;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;

/**
 * Author: Li&Meng
 * Date: 2019/6/15
 * Description:
 * String taskShopStoreSource = String.format(getString(R.string.task_shop_store), order.getSeller().getShopName() + "【点击复制店铺名】");
 * tvShopStore.setText(SpannableStringBuild.build(taskShopStoreSource, 0xff008BD6, "【点击复制店铺名】"));
 */

public class SpannableStringBuild {
    public static SpannableString build(CharSequence source, int color, Object text){
        SpannableString spannableString = new SpannableString(source);
        ForegroundColorSpan colorSpan = new ForegroundColorSpan(color);
        int start = source.toString().indexOf(text.toString());
        spannableString.setSpan(colorSpan, start, start+text.toString().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableString;
    }
    public static SpannableString buildStyle(CharSequence source,int style, Object text){
        SpannableString spannableString = new SpannableString(source);
        StyleSpan styleSpan = new StyleSpan(style);
        int start = source.toString().indexOf(text.toString());
        spannableString.setSpan(styleSpan, start, start+text.toString().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableString;
    }
}
