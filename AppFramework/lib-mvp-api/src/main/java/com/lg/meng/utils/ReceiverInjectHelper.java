package com.lg.meng.utils;

import android.content.Context;

import com.lg.meng.ReceiverRegistor;

import java.lang.reflect.Constructor;

/**
 * Author: Ligang
 * Date: 2019/5/6
 * Description:
 */

public class ReceiverInjectHelper {
    public static ReceiverRegistor inject(Context host) {
        String classFullName = host.getClass().getName() + "_ReceiverInjector";
        try {
            Class proxy = Class.forName(classFullName);
            Constructor constructor = proxy.getConstructor(host.getClass());
            Object newInstance = constructor.newInstance(host);
            return (ReceiverRegistor) newInstance;
        } catch (Exception e) {

        }
        return null;
    }
}
