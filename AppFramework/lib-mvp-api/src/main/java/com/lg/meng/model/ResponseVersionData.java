package com.lg.meng.model;

import java.io.Serializable;

/**
 * Author: Ligang
 * Date: 2019/9/4
 * Description:
 */

public class ResponseVersionData implements Serializable {


    /**
     * fileLength : 0
     * updateUrl : http://39.100.76.8/app/ppx/app-debug_v1.0-1_20191212_233131.apk
     * md5Code : 84701e6cf199f38704302b195772a92f
     * versionCode : 1
     * versionName : 1.0
     * content : 测试
     * isForceUpdate : true
     */

    private long fileLength;
    private String updateUrl;
    private String md5Code;
    private int versionCode;
    private String versionName;
    private String content;
    private boolean isForceUpdate;

    public long getFileLength() {
        return fileLength;
    }

    public void setFileLength(long fileLength) {
        this.fileLength = fileLength;
    }

    public String getUpdateUrl() {
        return updateUrl;
    }

    public void setUpdateUrl(String updateUrl) {
        this.updateUrl = updateUrl;
    }

    public String getMd5Code() {
        return md5Code;
    }

    public void setMd5Code(String md5Code) {
        this.md5Code = md5Code;
    }

    public int getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(int versionCode) {
        this.versionCode = versionCode;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isIsForceUpdate() {
        return isForceUpdate;
    }

    public void setIsForceUpdate(boolean isForceUpdate) {
        this.isForceUpdate = isForceUpdate;
    }
}


