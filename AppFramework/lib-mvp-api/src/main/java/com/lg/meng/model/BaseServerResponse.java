package com.lg.meng.model;

/**
 * Author: Ligang
 * Date: 2019/11/25
 * Description:
 */
public abstract class BaseServerResponse {
    public abstract boolean isShowErrorMsg();

    public abstract String getErrorMsg();
}
