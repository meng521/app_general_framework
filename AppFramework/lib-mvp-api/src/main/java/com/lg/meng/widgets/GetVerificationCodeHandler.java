package com.lg.meng.widgets;

import android.os.CountDownTimer;
import android.widget.TextView;

/**
 * Author: Ligang
 * Date: 2019/11/25
 * Description:
 */
public class GetVerificationCodeHandler {
    private CountDownTimer countDownTimer;
    private int resetTime = 60;
    private TextView tv;

    public GetVerificationCodeHandler(TextView textView, int resetTime) {
        this.tv = textView;
        this.resetTime = resetTime;
    }

    public void startCountDownTimer() {
        tv.setEnabled(false);
        if (countDownTimer == null) {
            countDownTimer = new CountDownTimer(resetTime * 1000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    tv.setText(millisUntilFinished / 1000 + "秒后重新发送");
                }

                @Override
                public void onFinish() {
                    tv.setEnabled(true);
                    tv.setText("重新发送");
                }
            };
        }
        countDownTimer.start();
    }

    public static GetVerificationCodeHandler newInstance(TextView textView, int resetTime) {
        return new GetVerificationCodeHandler(textView, resetTime);
    }
}
