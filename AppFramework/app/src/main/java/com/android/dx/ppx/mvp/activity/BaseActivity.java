package com.android.dx.ppx.mvp.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.WindowManager;

import com.android.dx.ppx.R;
import com.android.dx.ppx.mvp.fragment.BaseFragment;
import com.android.dx.ppx.mvp.fragment.FragmentBackHandler;
import com.lg.meng.base.BaseMvpActivity;
import com.lg.meng.utils.StatusBarUtils;

/**
 * Author: Ligang
 * Date: 2019/11/08
 * Description:
 */
public abstract class BaseActivity extends BaseMvpActivity {
    protected BaseActivity activity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        activity = this;
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        //保持竖屏(禁止横屏切换）
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setStatusBar();
        super.onCreate(savedInstanceState);
    }

    // 在setContentView之前执行
    public void setStatusBar() {
    /*
     为统一标题栏与状态栏的颜色，我们需要更改状态栏的颜色，而状态栏文字颜色是在android 6.0之后才可以进行更改
     所以统一在6.0之后进行文字状态栏的更改
    */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (isUseFullScreenMode()) {
                StatusBarUtils.transparencyBar(this);
            } else {
                StatusBarUtils.setStatusBarColor(this, setStatusBarColor());
            }

            if (isUserLightMode()) {
                StatusBarUtils.setLightStatusBar(this, true);
            }
        }

    }

    // 是否设置成透明状态栏，即就是全屏模式
    protected boolean isUseFullScreenMode() {
        return true;
    }

    protected int setStatusBarColor() {
        return R.color.white;
    }

    // 是否改变状态栏文字颜色为黑色，默认为黑色
    protected boolean isUserLightMode() {
        return false;
    }

    @Override
    public void onBackPressed() {
        FragmentBackHandler fragmentBackHandler = FragmentBackHandler.getInstance(this);
        if (fragmentBackHandler != null) {
            BaseFragment currentFragment = fragmentBackHandler.getCurrentFragment();
            if (currentFragment != null) {
                if (currentFragment.onBackPressed()) {
                    return;
                }
            }
        }
        super.onBackPressed();
    }
}

