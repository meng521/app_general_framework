package com.android.dx.ppx.mvp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.android.dx.ppx.mvp.activity.BaseActivity;
import com.lg.meng.base.BaseMvpFragment;

/**
 * Author: Ligang
 * Date: 2019/11/08
 * Description:
 */
public abstract class BaseFragment extends BaseMvpFragment {
    protected BaseActivity activity;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        activity = (BaseActivity) getActivity();
        super.onActivityCreated(savedInstanceState);

    }

    protected void jumpTo(Class clz) {
        activity.jumpTo(clz);
    }

    /**
     * 所有继承BackHandledFragment的子类都将在这个方法中实现物理Back键按下后的逻辑
     * FragmentActivity捕捉到物理返回键点击事件后会首先询问Fragment是否消费该事件
     * 如果没有Fragment消息时FragmentActivity自己才会消费该事件
     */
    public boolean onBackPressed() {
        return false;
    }
}
