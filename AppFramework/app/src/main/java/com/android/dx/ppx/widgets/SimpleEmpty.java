package com.android.dx.ppx.widgets;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.android.dx.ppx.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Author: Ligang
 * Date: 2019/11/15
 * Description:
 */

public class SimpleEmpty {
    public static final int STATE_LOADING = 1 << 1;
    public static final int STATE_FAIL = 1 << 2;
    public static final int STATE_NO_CONTENT = 1 << 3;
    public static final int STATE_GONE = 1 << 4;
    String[] mMessages = null;
    public View emptyView;
    @BindView(R.id.img_recycler_empty)
    ImageView imgRecyclerEmpty;
    @BindView(R.id.tv_recycler_empty)
    TextView tvRecyclerEmpty;

    public SimpleEmpty(Context context, View.OnClickListener onClickListener) {
        this(context, LayoutInflater.from(context).inflate(R.layout.layout_recycler_empty, null), onClickListener);
    }

    public SimpleEmpty(Context context, View emptyView, View.OnClickListener onClickListener) {
        mMessages = context.getResources().getStringArray(R.array.recycler_empty);
        this.emptyView = emptyView;
        ButterKnife.bind(this, this.emptyView);
        setState(STATE_LOADING);
        emptyView.setOnClickListener(v -> {
            setState(STATE_LOADING);
            if (onClickListener != null) {
                onClickListener.onClick(v);
            }
        });
    }

    public void setState(int state) {
        emptyView.setVisibility(View.VISIBLE);
        switch (state) {
            case STATE_LOADING:
                imgRecyclerEmpty.setVisibility(View.GONE);
                tvRecyclerEmpty.setText(mMessages[0]);
                emptyView.setEnabled(false);
                emptyView.setVisibility(View.VISIBLE);
                break;
            case STATE_NO_CONTENT:
                imgRecyclerEmpty.setVisibility(View.VISIBLE);
                tvRecyclerEmpty.setText(mMessages[1]);
                emptyView.setEnabled(false);
                emptyView.setVisibility(View.VISIBLE);
                break;
            case STATE_FAIL:
                imgRecyclerEmpty.setVisibility(View.VISIBLE);
                tvRecyclerEmpty.setText(mMessages[2]);
                emptyView.setEnabled(true);
                emptyView.setVisibility(View.VISIBLE);
                break;
            case STATE_GONE:
                emptyView.setVisibility(View.GONE);
                break;
        }
    }
}
