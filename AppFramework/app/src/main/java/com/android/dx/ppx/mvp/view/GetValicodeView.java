package com.android.dx.ppx.mvp.view;

import com.lg.meng.base.BaseMvpView;

/**
 * Author: Ligang
 * Date: 2019/11/15
 * Description: 获取验证码
 */
public interface GetValicodeView extends BaseMvpView {
    void getValicodeSuccess();

    void getValicodeFailed(Throwable e);
}
