package com.android.dx.ppx.mvp.activity;

import android.content.Intent;

import com.android.dx.ppx.R;
import com.lg.meng.utils.ThreadUtils;

/**
 * Author: Ligang
 * Date: 2019/11/08
 * Description:
 */
public class SplashActivity extends BaseActivity {
    @Override
    protected int provideContentViewId() {
        return R.layout.activity_splash;
    }

    @Override
    protected void beforeSetContentView() {

    }

    @Override
    protected void afterSetContentView() {
        ThreadUtils.post(2500, () -> {
            startActivity(new Intent(activity, HomeActivity.class));
        });
    }
}
