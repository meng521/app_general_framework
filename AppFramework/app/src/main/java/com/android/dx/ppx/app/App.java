package com.android.dx.ppx.app;

import android.app.Application;

import com.google.gson.Gson;
import com.lg.meng.application.StubApplication;

/**
 * Author: Ligang
 * Date: 2019/11/07
 * Description:
 */
public class App extends Application {
    public static Gson gson;

    @Override
    public void onCreate() {
        super.onCreate();

        gson = new Gson();
        StubApplication.init(this);
    }
}
