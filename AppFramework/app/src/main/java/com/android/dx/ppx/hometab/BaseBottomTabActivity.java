package com.android.dx.ppx.hometab;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.View;


import com.android.dx.ppx.R;
import com.android.dx.ppx.mvp.activity.BaseActivity;
import com.android.dx.ppx.mvp.fragment.BaseFragment;

import java.util.List;

/**
 * Created by Li&Meng on 2018/8/30.
 * 底部tabs架构基类
 * --- 子类有两种：容器+Fragment+底部Tabs  和  ViewPager+Fragment+底部Tabs
 */
public abstract class BaseBottomTabActivity extends BaseActivity {
    protected View centerView;
    protected List<BaseFragment> fragments;
    protected BottomTabView bottomTabView;
    protected boolean isCenterViewSkipNewAty; //点击CenterView后是否是跳转到新的Activity
    protected FragmentManager fragmentManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        fragmentManager = getSupportFragmentManager();
        fragments = getFragments();
        super.onCreate(savedInstanceState);
        bottomTabView = findViewById(R.id.bottomTabView);
        bottomTabView.setOnSecondSelectListener(new BottomTabView.OnSecondSelectListener() {
            @Override
            public void onSecondSelect(int position) {
                onTabContinuousClick(position);
            }
        });
        setCenterView();
        init();
    }

    public void switchTab(int position) {
        bottomTabView.switchTab(position);
    }

    /**
     * 需要设置CenterView的话，就重写该方法
     * 并且在方法中调用带参数的setCenterView方法
     */
    protected void setCenterView() {

    }

    protected final View setCenterView(int centerViewLayout, boolean isCenterViewSkipNewAty, View.OnClickListener clickListener) {
        this.isCenterViewSkipNewAty = isCenterViewSkipNewAty;
        View centerView = getLayoutInflater().inflate(centerViewLayout, bottomTabView, false);
        if (isCenterViewSkipNewAty) {
            if (clickListener != null) {
                centerView.setOnClickListener(clickListener);
            }
        } else {
            centerView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onCenterViewClick(v);
                }
            });
        }
        return this.centerView = centerView;
    }

    protected View getCenterView() {
        return this.centerView;
    }

    /**
     * 显示tab标签上tip消息的数量
     *
     * @param tabIndex
     * @param tipNumber
     */
    protected void showTipNumber(int tabIndex, int tipNumber) {
        bottomTabView.showTipNumber(tabIndex, tipNumber);
    }

    protected void showTipPoint(int tabIndex) {
        bottomTabView.showTipPoint(tabIndex);
    }

    /**
     * 隐藏tab标签上的tip消息
     *
     * @param tabIndex
     */
    protected void hideTipNumber(int tabIndex) {
        bottomTabView.hideTipNumber(tabIndex);
    }

    protected void hideTipPoint(int tabIndex) {
        bottomTabView.hideTipPoint(tabIndex);
    }

    protected abstract void init();

    protected abstract List<TabItemView> getTabViews();

    protected abstract List<BaseFragment> getFragments();

    protected abstract void onCenterViewClick(View centerView);

    //连续选中
    protected abstract void onTabContinuousClick(int position);

    protected BaseFragment getFragment(int tabIndex) {
        if (fragments != null && fragments.size() > tabIndex) {
            return fragments.get(tabIndex);
        }
        throw new RuntimeException("Fragments is null or tabIndex out of size");
    }

    protected abstract BaseFragment getCurrentFragment();
}
