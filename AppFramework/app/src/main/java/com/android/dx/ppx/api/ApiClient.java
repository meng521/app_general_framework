package com.android.dx.ppx.api;

/**
 * Author: Ligang
 * Date: 2019/11/15
 * Description:
 */
public class ApiClient extends ApiManager<ApiService> {

    private static ApiClient instance;

    private ApiClient() {
    }

    public static ApiClient getInstance() {
        if (instance == null) {
            synchronized (ApiClient.class) {
                if (instance == null) {
                    instance = new ApiClient();
                }
            }
        }
        return instance;
    }

}
