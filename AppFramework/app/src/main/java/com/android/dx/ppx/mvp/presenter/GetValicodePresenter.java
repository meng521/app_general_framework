package com.android.dx.ppx.mvp.presenter;

import android.text.TextUtils;

import com.android.dx.ppx.mvp.view.GetValicodeView;
import com.blankj.utilcode.util.RegexUtils;
import com.blankj.utilcode.util.ToastUtils;

/**
 * Author: Ligang
 * Date: 2019/11/15
 * Description:
 */
public class GetValicodePresenter extends BasePresenter<GetValicodeView> {
    public void getValicode(String phone) {
        if (!checkInput(phone)) {
            return;
        }
//        ApiClient.getInstance().sendLoginSms(phone)
//                .subscribe(simpleObserver(new Callback<HttpResponse>() {
//                    @Override
//                    public void resultSuccess(HttpResponse response) {
//                        if (response.getData() != null) {
//                            getView().getValicodeSuccess(response.getData().toString());
//                        }
//                    }
//
//                    @Override
//                    public void resultError(Throwable e) {
//                        getView().getValicodeFailed(e);
//                    }
//                }));
    }

    private boolean checkInput(String phone) {
        if (TextUtils.isEmpty(phone)) {
            ToastUtils.showShort("请填写手机号");
            return false;
        }
        if (!RegexUtils.isMobileSimple(phone)) {
            ToastUtils.showShort("请填写合法的手机号");
            return false;
        }

        return true;
    }
}
