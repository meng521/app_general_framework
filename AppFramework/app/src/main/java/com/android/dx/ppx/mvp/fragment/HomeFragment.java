package com.android.dx.ppx.mvp.fragment;

import android.app.PendingIntent;
import android.content.Intent;
import android.view.View;
import android.widget.Toast;

import com.android.dx.ppx.R;
import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.lg.meng.model.NotificationModel;
import com.lg.meng.utils.NotificationHelper;

/**
 * Author: Ligang
 * Date: 2019/11/08
 * Description:
 */
public class HomeFragment extends BaseFragment{
    @Override
    protected int provideContentViewId() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initViews() {
        findViewById(R.id.btn).setOnClickListener(v -> {
            showNoti();
        });
    }

    @Override
    protected void initData() {

    }

    public void showNoti(){
        ToastUtils.showLong("" + NotificationHelper.isPermissionOpen(activity));
        Intent intent = new Intent("notification_clicked");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(activity, 100, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationHelper.getHelper().newNotifier(NotificationHelper.ID_DOWN_APP).
                setmContext(activity).
                setBuilder().
                setPendingIntent(pendingIntent).
                setTicker(getString(com.lg.meng.R.string.app_name) + "APK下载").
                setWhen(System.currentTimeMillis()).setNotifyId(827).
                setDefaults(NotificationModel.DEFAULT_SOUND).setOngoing(true).setPriority(NotificationModel.PRIORITY_HIGH).
                setNotification().
                setNotificationView(com.lg.meng.R.layout.layout_notify_down).
                setTextViewText(com.lg.meng.R.id.down_name, AppUtils.getAppName() + "APK下载中").
                setManager().setIcon().showNotification();
    }
}
