package com.android.dx.ppx.api;


import java.util.List;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Author: Ligang
 * Date:  2019/8/30
 * Description:
 */
public interface ApiService {
    /**
     * 设备激活
     */
    @POST("api-v2-device/incar/bindDeviceToUser_v2")
    Observable bindDeviceToUser(@Body RequestBody body);

    /**
     * 从后台搜索歌曲
     */
    @GET("api-v2-content/incar/searchMusicAudios/{singerName}/{musicName}/{page}")
    Observable searchMusicFromServer(@Path("singerName") String singerName, @Path("musicName") String musicName, @Path("page") String page);

    /**
     * 从我的后台收藏歌曲
     */
    @FormUrlEncoded
    @POST("http://39.100.76.8/SmaradioAppManager/api-v2-user/incar/collectMusic")
    Observable collectMusic(@Field("deviceId") int deviceId, @Field("collectedMusicJson") String collectedMusicJson);


    /**
     * 提交log文件
     */
    @Multipart
//    @POST("http://39.100.76.8/SmaradioAppManager/file/uploadLog")
    @POST("api-v2-behaviour/incar/uploadLog")
    Observable uploadLog(@Part List<MultipartBody.Part> partList);

}
