package com.android.dx.ppx.mvp.fragment;

import com.android.dx.ppx.R;

/**
 * Author: Ligang
 * Date: 2019/11/08
 * Description:
 */
public class TeamFragment extends BaseFragment{
    @Override
    protected int provideContentViewId() {
        return R.layout.fragment_team;
    }

    @Override
    protected void initViews() {

    }

    @Override
    protected void initData() {

    }
}
