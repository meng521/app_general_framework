package com.android.dx.ppx.hometab;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.dx.ppx.R;
import com.blankj.utilcode.util.ConvertUtils;


/**
 * Created by Li&Meng on 2018/8/30.
 * 代表底部tab的Item
 * 通过setStatus更改tab的图片和文字颜色
 */
public class TabItemView extends LinearLayout {


    /**
     * 两个状态 未选中、选中
     */
    public final static int DEFAULT = 0;
    public final static int PRESS = 1;

    /**
     * Item 的标题
     */
    public String title;

    /**
     * 标题的两个状态的颜色 选中、未选中
     */
    public int colorDef;
    public int colorPress;

    /**
     * 两个图标的 资源 id ，选中、未选中
     */
    public int iconResDef;
    public int iconResPress;

    public TextView tvTitle;
    public ImageView ivIcon;
    private Animation animation;
    /**
     * 解决刚进入界面的时候 自动执行tab点击动画
     **/
    private boolean isFirstShow;

    public void setFirstShow(boolean firstShow) {
        isFirstShow = firstShow;
    }

    @Override
    public void setAnimation(Animation animation) {
        this.animation = animation;
    }

    public TabItemView(Context context, int iconResDef, int iconResPress) {
        this(context, "", 0, 0, iconResDef, iconResPress);
    }

    public TabItemView(Context context, String title, int colorDef, int colorPress,
                       int iconResDef, int iconResPress) {
        super(context);
        this.title = title;
        this.colorDef = colorDef;
        this.colorPress = colorPress;
        this.iconResDef = iconResDef;
        this.iconResPress = iconResPress;
        init();
    }

    /**
     * 初始化
     */
    public void init() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.view_tab_item, this);
        if (!TextUtils.isEmpty(title)) {
            ViewStub viewStub = (ViewStub) view.findViewById(R.id.tvViewStub);
            viewStub.inflate();
            tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            tvTitle.setText(title);
        }
        ivIcon = (ImageView) view.findViewById(R.id.ivIcon);
        LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParams.weight = 1;
        view.setLayoutParams(layoutParams);
    }

    /**
     * 设置状态
     */
    public void setStatus(int status) {
        if (tvTitle != null) {
            tvTitle.setTextColor(ContextCompat.getColor(super.getContext(), status == PRESS ? colorPress : colorDef));
        }
        ivIcon.setImageResource(status == PRESS ? iconResPress : iconResDef);
        if (animation != null && status == PRESS) {
            if (isFirstShow) {
                isFirstShow = false;
                return;
            }
            animation.reset();
            ivIcon.startAnimation(animation);
        }
    }

    private TextView tipCountTv;

    public void showTipNumber(int tipCount) {
        inflateTipTvIfNull();
        tipCountTv.setVisibility(View.VISIBLE);
        tipCountTv.setText(tipCount + "");
    }

    public void hideTipNumber() {
        if (tipCountTv != null && tipCountTv.getVisibility() != View.GONE) {
            tipCountTv.setVisibility(View.GONE);
        }
    }

    public void showTipPoint() {
        inflateTipTvIfNull();
        tipCountTv.setVisibility(View.VISIBLE);
        ViewGroup.LayoutParams params = tipCountTv.getLayoutParams();
        params.width = ConvertUtils.dp2px(8);
        params.height = ConvertUtils.dp2px(8);
        tipCountTv.setLayoutParams(params);
    }

    public void hideTipPoint() {
        if (tipCountTv != null && tipCountTv.getVisibility() != View.GONE) {
            tipCountTv.setVisibility(View.GONE);
        }
    }

    /**
     * 如果tipCountTv(提示消息TextView)为null,就加载
     */
    private void inflateTipTvIfNull() {
        if (tipCountTv == null) {
            ViewStub viewStub = (ViewStub) findViewById(R.id.tipCountStub);
            viewStub.inflate();
            tipCountTv = (TextView) findViewById(R.id.tvTipCount);
        }
    }

    private ImageView tipImageView;

    public void showTipImageView(int tipImgResId) {
        inflateTipImageViewIfNull();
        tipImageView.setVisibility(View.VISIBLE);
        tipImageView.setImageResource(tipImgResId);
    }

    public void hideTipImageView() {
        if (tipImageView != null) {
            tipImageView.setVisibility(View.GONE);
        }
    }

    public void showTipImageView(int tipImgResId, int topMargindip, int rightMargindip) {
        showTipImageView(tipImgResId);
        ViewGroup.LayoutParams params = tipImageView.getLayoutParams();
        if (params != null && params instanceof MarginLayoutParams) {
            MarginLayoutParams layoutParams = (MarginLayoutParams) params;
            layoutParams.topMargin = dip2px(getContext(), topMargindip);
            layoutParams.rightMargin = dip2px(getContext(), rightMargindip);
        }
    }

    /**
     * tipImageView(提示消息图片)为null,就加载
     */
    private void inflateTipImageViewIfNull() {
        if (tipImageView == null) {
            ViewStub viewStup = (ViewStub) findViewById(R.id.tipViewStub);
            viewStup.inflate();
            tipImageView = (ImageView) findViewById(R.id.tipImageView);
        }
    }

    public void showTipImageView(int tipImgResId, int topMargindip, int rightMargindip, int widthdip, int heighdip) {
        showTipImageView(tipImgResId, topMargindip, rightMargindip);
        ViewGroup.LayoutParams params = tipImageView.getLayoutParams();
        params.width = dip2px(getContext(), widthdip);
        params.height = dip2px(getContext(), heighdip);
        tipImageView.setLayoutParams(params);
    }

    /**
     * 将dip或dp值转换为px值，保证尺寸大小不变
     */
    private int dip2px(Context context, float dipValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }
}
