package com.android.dx.ppx.api;

import com.android.dx.ppx.model.HttpResponse;
import com.lg.meng.api.BaseServerResponseError;

/**
 * Author: Ligang
 * Date: 2019/11/07
 * Description:
 */
public class ServerResponseError extends BaseServerResponseError {
    private HttpResponse httpResponse;

    public ServerResponseError(HttpResponse httpResponse) {
        super(httpResponse);
        this.httpResponse = httpResponse;
    }


    public HttpResponse getHttpResponse() {
        return httpResponse;
    }

    public void setHttpResponse(HttpResponse httpResponse) {
        this.httpResponse = httpResponse;
    }
}
