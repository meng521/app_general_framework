package com.android.dx.ppx.mvp.activity;

import com.android.dx.ppx.R;
import com.android.dx.ppx.hometab.BaseBottomFrameLayoutTabActivity;
import com.android.dx.ppx.hometab.TabItemView;
import com.android.dx.ppx.mvp.fragment.BaseFragment;
import com.android.dx.ppx.mvp.fragment.HomeFragment;
import com.android.dx.ppx.mvp.fragment.MineFragment;
import com.android.dx.ppx.mvp.fragment.ShopStoreFragment;
import com.android.dx.ppx.mvp.fragment.TaskFragment;
import com.android.dx.ppx.mvp.fragment.TeamFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: Ligang
 * Date: 2019/11/08
 * Description:
 */
public class HomeActivity extends BaseBottomFrameLayoutTabActivity {
    @Override
    protected List<TabItemView> getTabViews() {
        List<TabItemView> tabItemViews = new ArrayList<>();
        tabItemViews.add(new TabItemView(activity, getString(R.string.tab_home),
                R.color.bottom_tab_text_def, R.color.bottom_tab_text_selected,
                R.drawable.tab_home_d, R.drawable.tab_home_s));
        tabItemViews.add(new TabItemView(activity, getString(R.string.tab_task),
                R.color.bottom_tab_text_def, R.color.bottom_tab_text_selected,
                R.drawable.tab_task_d, R.drawable.tab_task_s));
        tabItemViews.add(new TabItemView(activity, getString(R.string.tab_team),
                R.color.bottom_tab_text_def, R.color.bottom_tab_text_selected,
                R.drawable.tab_team_d, R.drawable.tab_team_s));
        tabItemViews.add(new TabItemView(activity, getString(R.string.tab_shop),
                R.color.bottom_tab_text_def, R.color.bottom_tab_text_selected,
                R.drawable.tab_shop_d, R.drawable.tab_shop_s));
        tabItemViews.add(new TabItemView(activity, getString(R.string.tab_mine),
                R.color.bottom_tab_text_def, R.color.bottom_tab_text_selected,
                R.drawable.tab_mine_d, R.drawable.tab_mine_s));
        return tabItemViews;
    }

    @Override
    protected List<BaseFragment> getFragments() {
        List<BaseFragment> fragments = new ArrayList<>();
        fragments.add(new HomeFragment());
        fragments.add(new TaskFragment());
        fragments.add(new TeamFragment());
        fragments.add(new ShopStoreFragment());
        fragments.add(new MineFragment());
        return fragments;
    }
}
