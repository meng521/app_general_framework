package com.android.dx.ppx.model;

import com.lg.meng.model.BaseServerResponse;

/**
 * Author: Ligang
 * Date: 2019/11/07
 * Description:
 */
public class HttpResponse extends BaseServerResponse {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return true;
    }

    @Override
    public boolean isShowErrorMsg() {
        return false;
    }

    @Override
    public String getErrorMsg() {
        return "";
    }
}
