package com.android.dx.ppx.adapter;

import android.support.annotation.Nullable;

import com.android.dx.ppx.widgets.SimpleEmpty;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * Author: Ligang
 * Date: 2019/4/15
 * Description:
 */

public abstract class BaseEmptyQuickAdapter<T, K extends BaseViewHolder> extends BaseQuickAdapter<T, K> {
    private SimpleEmpty simpleEmpty;

    public BaseEmptyQuickAdapter(int layoutResId, @Nullable List<T> data) {
        super(layoutResId, data);
    }

    public BaseEmptyQuickAdapter(@Nullable List<T> data) {
        super(data);
    }

    public BaseEmptyQuickAdapter(int layoutResId) {
        super(layoutResId);
    }

    public void setEmptyView(SimpleEmpty simpleEmpty) {
        super.setEmptyView(simpleEmpty.emptyView);
        this.simpleEmpty = simpleEmpty;
    }

    public void setEmptyState(int state) {
        if (getData().size() != 0) {
            return;
        }
        if (simpleEmpty == null) {
            return;
        }
        simpleEmpty.setState(state);
    }
}

