package com.android.dx.ppx.hometab;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import java.util.List;

/**
 * Created by Li&Meng on 2018/8/30.
 * 底部所有TabItemView的容器，管理所有TabItemView
 */

public class BottomTabView extends LinearLayout {
    /**
     * 记录最新的选择位置
     */
    private int lastPosition = -1;
    /**
     * 所有 TabItem 的集合
     */
    private List<TabItemView> tabItemViews;

    public BottomTabView(Context context) {
        super(context);
    }

    public BottomTabView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * 设置 Tab Item View
     */
    public void setTabItemViews(List<TabItemView> tabItemViews) {
        setTabItemViews(tabItemViews, null);
    }

    public List<TabItemView> getTabItemViews() {
        return tabItemViews;
    }

    /**
     * 设置 Tab Item View
     */
    public void setTabItemViews(List<TabItemView> tabItemViews, View centerView) {

        if (this.tabItemViews != null) {
            throw new RuntimeException("不能重复设置！");
        }

        if (tabItemViews == null || tabItemViews.size() < 2) {
            throw new RuntimeException("TabItemView 的数量必须大于2！");
        }

        this.tabItemViews = tabItemViews;
        //将所有TabItemView添加到容器中，如果有centerView就放在中间
        for (int i = 0; i < tabItemViews.size(); i++) {
            final int finalI = i;
            if (centerView != null && i == tabItemViews.size() >> 1) {
                this.addView(centerView); //添加CenterView
            }
            final TabItemView tabItemView = tabItemViews.get(i);
            this.addView(tabItemView);
            tabItemView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    switchTab(finalI);
                }
            });
        }

        /**
         * 将所有的 TabItem 设置为 初始化状态
         */
        for (TabItemView tab : tabItemViews) {
            tab.setStatus(TabItemView.DEFAULT);
        }

        /**
         * 默认状态选择第一个
         */
        tabItemViews.get(0).setFirstShow(true);
        updatePosition(0);
    }

    private long lastClickTime;

    /**
     * 切换Tab
     *
     * @param index
     */
    public void switchTab(int index) {
        if (index == lastPosition && System.currentTimeMillis() - lastClickTime > 1000) {
            // 第二次点击
            if (onSecondSelectListener != null) {
                onSecondSelectListener.onSecondSelect(index);
                lastClickTime = System.currentTimeMillis();
            }
            return;
        }
        if (onTabItemSelectListener != null) {
            if (onTabItemSelectListener.onInterceptTabItemSelect(index)) {//外部是否拦截了
                return;
            }
            onTabItemSelectListener.onTabItemSelect(index);
        }
        if (index != lastPosition) {
            lastClickTime = 0;
        } else {
            lastClickTime = System.currentTimeMillis();
        }
        updatePosition(index);

    }

    /**
     * 更新被选中 Tab Item 的状态
     * 恢复上一个 Tab Item 的状态
     */
    public void updatePosition(int position) {
        if (lastPosition != position) {
            if (tabItemViews != null && tabItemViews.size() != 0) {
                if (position != -1) {
                    tabItemViews.get(position).setStatus(TabItemView.PRESS);
                }
                if (lastPosition != -1) {
                    tabItemViews.get(lastPosition).setStatus(TabItemView.DEFAULT);
                }
                lastPosition = position;
            } else {
                throw new RuntimeException("please setTabItemViews !");
            }
        }
    }

    private OnTabItemSelectListener onTabItemSelectListener;
    private OnSecondSelectListener onSecondSelectListener;

    public void setOnTabItemSelectListener(OnTabItemSelectListener onTabItemSelectListener) {
        this.onTabItemSelectListener = onTabItemSelectListener;
    }

    public void setOnSecondSelectListener(OnSecondSelectListener onSecondSelectListener) {
        this.onSecondSelectListener = onSecondSelectListener;
    }

    public void showTipNumber(int tabIndex, int tipNumber) {
        tabItemViews.get(tabIndex).showTipNumber(tipNumber);
    }

    public void showTipPoint(int tabIndex) {
        tabItemViews.get(tabIndex).showTipPoint();
    }

    public void showTipImageView(int tabIndex, int tipImgResId) {
        tabItemViews.get(tabIndex).showTipImageView(tipImgResId);
    }

    public void showTipImageView(int tabIndex, int tipImgResId, int topMargindip, int rightMargindip) {
        tabItemViews.get(tabIndex).showTipImageView(tipImgResId, topMargindip, rightMargindip);
    }

    public void showTipImageView(int tabIndex, int tipImgResId, int topMargindip, int rightMargindip, int widthdip, int heighdip) {
        tabItemViews.get(tabIndex).showTipImageView(tipImgResId, topMargindip, rightMargindip, widthdip, heighdip);
    }

    public void hideTipImageView(int tabIndex) {
        tabItemViews.get(tabIndex).hideTipImageView();
    }

    public void hideTipNumber(int tabIndex) {
        tabItemViews.get(tabIndex).hideTipNumber();
    }

    public void hideTipPoint(int tabIndex) {
        tabItemViews.get(tabIndex).hideTipPoint();
    }

    public int getTaemViewSize() {
        return tabItemViews.size();
    }


    /**
     * 第二次被选择的监听器
     */
    public interface OnSecondSelectListener {
        void onSecondSelect(int position);
    }

    public int getLastPosition() {
        return lastPosition;
    }

    /**
     * Created by Li&Meng on 2017/6/30.
     * description： 底部tabs第一次被选择的监听器
     */
    public interface OnTabItemSelectListener {
        void onTabItemSelect(int position);

        boolean onInterceptTabItemSelect(int position);
    }
}
