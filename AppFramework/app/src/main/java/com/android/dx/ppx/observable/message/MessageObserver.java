package com.android.dx.ppx.observable.message;

import com.android.dx.ppx.model.AppMessage;
import com.lg.meng.observable.Observer;

/**
 * Author: Ligang
 * Date: 2019/4/7
 * Description:
 */

public interface MessageObserver extends Observer {
    /*
        消息更新
     */
    void onMessageUpdate(AppMessage message);
}
