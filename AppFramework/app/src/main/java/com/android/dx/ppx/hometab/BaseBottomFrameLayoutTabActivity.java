package com.android.dx.ppx.hometab;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.android.dx.ppx.R;
import com.android.dx.ppx.mvp.fragment.BaseFragment;


/**
 * Created by Li&Meng on 2018/8/30.
 * 容器+Fragment+底部Tabs  基类
 */
public abstract class BaseBottomFrameLayoutTabActivity extends BaseBottomTabActivity implements BottomTabView.OnTabItemSelectListener {
    @Override
    protected void init() {
        bottomTabView = findViewById(R.id.bottomTabView);
        if (getCenterView() == null) {
            bottomTabView.setTabItemViews(getTabViews());
        } else {
            bottomTabView.setTabItemViews(getTabViews(), getCenterView());
        }
        bottomTabView.setOnTabItemSelectListener(this);
        onTabItemSelect(0);
    }

    @Override
    protected int provideContentViewId() {
        return R.layout.activity_base_bottom_tab;
    }

    @Override
    protected void beforeSetContentView() {

    }

    @Override
    protected void afterSetContentView() {

    }

    private void changeTab(int index) {
        try {
            if (lastTabIndex == index) return;
            if (getCenterView() != null && index != getTabViews().size() >> 1) {
                centerView.setSelected(false);
            }
            Fragment fragment = fragmentManager.findFragmentByTag(fragments.get(index).getClass().getName());
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            if (fragment == null) {
                fragment = fragments.get(index);
                transaction.add(R.id.fragmentContainer, fragment, fragment.getClass().getName());
            } else {
                transaction.show(fragment);
            }
            if (lastTabIndex != -1) {
                transaction.hide(fragments.get(lastTabIndex));
            }
            transaction.commit();
            lastTabIndex = index;
        } catch (Exception e) {

        }
    }

    private int lastTabIndex = -1;

    @Override
    protected void onCenterViewClick(View centerView) {
        int index = getTabViews().size() >> 1;
        changeTab(index);
        //取消其它的选中状态
        bottomTabView.updatePosition(-1);
        centerView.setSelected(true);
    }

    @Override
    public void onTabItemSelect(int position) {
        if (centerView != null && !isCenterViewSkipNewAty) {
            position = position < getTabViews().size() >> 1 ? position : position + 1;
        }
        changeTab(position);
    }

    /**
     * 是否拦截切换Tab操作
     * 适用场景：有的tab需要登录之后才能切换
     * 子类若要实现该功能，需重写该方法
     *
     * @return true代表拦截切换tab操作
     */
    @Override
    public boolean onInterceptTabItemSelect(int position) {
        return false;
    }

    /**
     * 已选中某个位置的Tab，再次点击这个位置的tab，会触发该方法
     *
     * @param position
     */
    @Override
    protected void onTabContinuousClick(int position) {

    }

    @Override
    protected BaseFragment getCurrentFragment() {
        if (lastTabIndex >= 0) {
            return fragments.get(lastTabIndex);
        }
        return null;
    }
}
