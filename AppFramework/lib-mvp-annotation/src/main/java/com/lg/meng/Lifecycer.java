package com.lg.meng;

/**
 * Author: Ligang
 * Date: 2019/5/6
 * Description:
 */

public interface Lifecycer {
    void onDestory();
}
