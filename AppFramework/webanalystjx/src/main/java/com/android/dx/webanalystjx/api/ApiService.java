package com.android.dx.webanalystjx.api;

import com.android.dx.webanalystjx.model.HttpResponse;
import com.android.dx.webanalystjx.model.MerchResponse;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Author: Ligang
 * Date: 2020/01/04
 * Description:
 */
public interface ApiService {

    @GET("merchs")
    Observable<HttpResponse<List<MerchResponse>>> getMechers();

    /**
     * parentId : 店铺id
     * accessToken: pt_key
     * receiver : 收货人
     */
    @FormUrlEncoded
    @POST("saveBuyer")
    Observable<HttpResponse> saveBuyer(@Field("loginName") String loginName,
                                       @Field("accessToken") String pt_key);
}
