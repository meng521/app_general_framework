package com.android.dx.webanalystjx;


import android.text.TextUtils;
import android.view.View;

import com.android.dx.webanalystjx.app.App;
import com.android.dx.webanalystjx.mvp.GoodsListActivity;
import com.android.dx.webanalystjx.mvp.activity.AddBuyerActivity;
import com.android.dx.webanalystjx.mvp.activity.WebviewActivity;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.lg.meng.utils.ClipboardUtils;

import butterknife.OnClick;

/**
 * 京喜买手工具
 */
public class MainActivity extends BaseTitleActivity {

    @Override
    protected int provideContentViewId() {
        return R.layout.activity_main;
    }

    @Override
    protected void beforeSetContentView() {

    }

    @Override
    protected void afterSetContentView() {
        findViewById(R.id.btn_back).setVisibility(View.GONE);
        findViewById(R.id.tv_back).setVisibility(View.GONE);
    }

    @OnClick({R.id.btn_jx_login, R.id.btn_get_ptkey, R.id.btn_goods_list})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_jx_login:
                jumpTo(JxLoginActivity.class);
                break;
            case R.id.btn_get_ptkey:
                if (!TextUtils.isEmpty(App.getPtkey())) {
                    ClipboardUtils.copyText(App.getPtkey());
                    ToastUtils.showLong("复制成功\n" + App.getPtkey());
                    LogUtils.e(App.getPtkey());
                } else {
                    ToastUtils.showLong("请先登录京喜");
                }
                jumpTo(AddBuyerActivity.class);
                break;
            case R.id.btn_goods_list:
                jumpTo(GoodsListActivity.class);
                break;
        }
    }

    @Override
    protected String provideTitle() {
        return "买手添加工具";
    }

}
