package com.android.dx.webanalystjx.mvp.view;

import com.lg.meng.base.BaseMvpView;

/**
 * Author: Ligang
 * Date: 2020/01/05
 * Description:
 */
public interface AddBuyerView extends BaseMvpView {
    void addBuyerSuccess();

    void addBuyerFailed();
}
