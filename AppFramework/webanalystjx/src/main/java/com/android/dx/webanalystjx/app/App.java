package com.android.dx.webanalystjx.app;

import android.app.Application;
import android.text.TextUtils;

import com.blankj.utilcode.util.LogUtils;
import com.google.gson.Gson;
import com.just.agentweb.AgentWebConfig;
import com.lg.meng.application.StubApplication;
import com.lg.meng.utils.AppToast;
import com.lg.meng.utils.FileUtils;

/**
 * Author: Ligang
 * Date: 2019/11/07
 * Description:
 */
public class App extends Application {
    public static Gson gson;
    public static App instance;

    public static String HOST;

    @Override
    public void onCreate() {
        super.onCreate();
        gson = new Gson();
        instance = this;
        StubApplication.init(instance);
        HOST = FileUtils.readAssetFile("host.txt").trim().replace(" ", "").replace("\n", "");
    }

    public static String getPtkey() {
        String cookies = AgentWebConfig.getCookiesByUrl("https://plogin.m.jd.com/cgi-bin/mm/dosmslogin");
        if (!TextUtils.isEmpty(cookies)) {
            LogUtils.e("cookies:\n" + cookies);
            String[] strings = cookies.split(";");
            if (strings != null && strings.length > 0) {
                for (String str : strings) {
                    if (str.contains("pt_key")) {
                        return str.trim().split("=")[1];
                    }
                }
            }
        }
        return "";
    }
}
