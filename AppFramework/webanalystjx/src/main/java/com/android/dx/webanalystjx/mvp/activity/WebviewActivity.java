package com.android.dx.webanalystjx.mvp.activity;

import android.util.Log;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.android.dx.webanalystjx.BaseTitleActivity;
import com.android.dx.webanalystjx.R;

import butterknife.BindView;

/**
 * Author: Ligang
 * Date: 2020/01/05
 * Description:
 */
public class WebviewActivity extends BaseTitleActivity {
    private static final String TAG = WebviewActivity.class.getName();
    @BindView(R.id.webview)
    WebView webView;

    String pddLoginUrl = "http://app.yangkeduo.com/?page_id=10002_1578161520753_nLRZsrwRrF&is_back=1";

    @Override
    protected String provideTitle() {
        return "";
    }

    @Override
    protected int provideContentViewId() {
        return R.layout.activity_webview;
    }

    @Override
    protected void beforeSetContentView() {

    }

    @Override
    protected void afterSetContentView() {

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                log("1shouldOverrideUrlLoading url=" + url);
                return super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                log("2shouldOverrideUrlLoading url=" + request.getUrl());
                return super.shouldOverrideUrlLoading(view, request);
            }

            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
                log("2shouldInterceptRequest url=" + request.getUrl());
                return super.shouldInterceptRequest(view, request);
            }

            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
                log("1shouldInterceptRequest url=" + url);
                return super.shouldInterceptRequest(view, url);
            }
        });
        webView.loadUrl(pddLoginUrl);
    }

    private void log(Object obj) {
        Log.e(TAG, obj + "");
    }
}
