package com.android.dx.webanalystjx.model;

/**
 * Author: Ligang
 * Date: 2020/01/04
 * Description:
 */
public class MerchResponse {

    @Override
    public String toString() {
        return "MerchResponse{" +
                "id='" + id + '\'' +
                ", isNewRecord=" + isNewRecord +
                ", remarks='" + remarks + '\'' +
                ", createDate='" + createDate + '\'' +
                ", updateDate='" + updateDate + '\'' +
                ", parentId=" + parentId +
                ", parentName='" + parentName + '\'' +
                ", merchName='" + merchName + '\'' +
                ", loginName='" + loginName + '\'' +
                ", dayTradeAmount=" + dayTradeAmount +
                ", dayLimitAmount=" + dayLimitAmount +
                ", totalTradeAmount=" + totalTradeAmount +
                ", status='" + status + '\'' +
                ", childNums=" + childNums +
                '}';
    }

    /**
     * id : 21
     * isNewRecord : false
     * remarks : 111
     * createDate : 2020-01-04 14:24:08
     * updateDate : 2020-01-04 14:24:19
     * parentId : 30
     * parentName : 德远
     * merchName : 双立人京东自营旗舰店
     * loginName : 1000001635
     * dayTradeAmount : 0
     * dayLimitAmount : 10000
     * totalTradeAmount : 0
     * status : 1
     * childNums : 20
     */

    private String id;
    private boolean isNewRecord;
    private String remarks;
    private String createDate;
    private String updateDate;
    private int parentId;
    private String parentName;
    private String merchName;
    private String loginName;
    private int dayTradeAmount;
    private int dayLimitAmount;
    private int totalTradeAmount;
    private String status;
    private int childNums;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isIsNewRecord() {
        return isNewRecord;
    }

    public void setIsNewRecord(boolean isNewRecord) {
        this.isNewRecord = isNewRecord;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getMerchName() {
        return merchName;
    }

    public void setMerchName(String merchName) {
        this.merchName = merchName;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public int getDayTradeAmount() {
        return dayTradeAmount;
    }

    public void setDayTradeAmount(int dayTradeAmount) {
        this.dayTradeAmount = dayTradeAmount;
    }

    public int getDayLimitAmount() {
        return dayLimitAmount;
    }

    public void setDayLimitAmount(int dayLimitAmount) {
        this.dayLimitAmount = dayLimitAmount;
    }

    public int getTotalTradeAmount() {
        return totalTradeAmount;
    }

    public void setTotalTradeAmount(int totalTradeAmount) {
        this.totalTradeAmount = totalTradeAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getChildNums() {
        return childNums;
    }

    public void setChildNums(int childNums) {
        this.childNums = childNums;
    }
}
