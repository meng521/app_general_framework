package com.android.dx.webanalystjx.api;

import com.android.dx.webanalystjx.app.App;
import com.lg.meng.api.BaseApiManager;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Author: Ligang
 * Date: 2020/01/03
 * Description:
 */
public class JdApiClient extends BaseApiManager<JdApiService> {
    private static JdApiClient instance;

    private JdApiClient() {
    }

    public static JdApiClient getInstance() {
        if (instance == null) {
            synchronized (JdApiClient.class) {
                if (instance == null) {
                    instance = new JdApiClient();
                }
            }
        }
        return instance;
    }

    @Override
    protected String baseUrl() {
        return "https://plogin.m.jd.com/";
    }

    @Override
    protected Converter.Factory converterFactory() {
        return null;
    }

    /*** 获取某商店的商品列表 ***/
    @GET("https://wqsou.jd.com/search/searchjson")
    public Observable<ResponseBody> getShopGoodsList(String shopId) {
        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put("datatype", "1");
        paramsMap.put("page", "1");
        paramsMap.put("pagesize", "20");
        paramsMap.put("merge_sku", "no");
        paramsMap.put("qp_disable", "yes");
        paramsMap.put("key", "ids,," + shopId);
        return apiService.getShopGoodsList(paramsMap).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * 加入购物车
     **/
    @GET("https://wqdeal.jd.com/deal/confirmorder/main")
    public Observable<ResponseBody> addCart(String wareid) {
        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put("sceneval", 2 + "");
        paramsMap.put("wdref", "https://item.m.jd.com/product/" + wareid + ".html");
        paramsMap.put("scene", "jd");
        paramsMap.put("isCanEdit", 1 + "");
        paramsMap.put("commlist", wareid + ",,1," + wareid + ",1,0,0");
        paramsMap.put("locationid", "22-1930-50947");
        paramsMap.put("type", 0 + "");
        paramsMap.put("lg", 0 + "");
        paramsMap.put("supm", 0 + "");

        Map<String, String> headMap = new HashMap<>();
        headMap.put("Cookie", App.getPtkey());
        headMap.put("Host", "wqdeal.jd.com");
        headMap.put("Referer", "https://item.m.jd.com/ware/view.action?wareId=" + wareid + "&clickUrl=");
        return apiService.addCart(paramsMap, headMap).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * 下单
     **/
    @POST("https://wqdeal.jd.com/deal/msubmit/confirm")
    public Observable<ResponseBody> commitOrder(String wareid) {
        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put("sceneval", 2 + "");
        paramsMap.put("commlist", wareid + ",,1," + wareid + ",1,0,0");

        Map<String, String> headMap = new HashMap<>();
        headMap.put("Cookie", App.getPtkey());
        headMap.put("Host", "wqdeal.jd.com");
        headMap.put("Referer", "https://item.m.jd.com/ware/view.action?wareId=" + wareid + "&clickUrl=");
        return apiService.commitOrder(paramsMap, headMap).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * 获取payid
     **/
    @GET("https://wq.jd.com/jdpaygw/jdappmpay")
    public Observable<ResponseBody> getPayid(String dealId) {
        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put("dealId", dealId);
        paramsMap.put("sceneval", "2");
        paramsMap.put("g_login_type", "1");
        paramsMap.put("g_ty", "ls");
        paramsMap.put("_", System.currentTimeMillis() + "");
        paramsMap.put("r", getRandom());

        Map<String, String> headMap = new HashMap<>();
        headMap.put("Cookie", App.getPtkey());
        headMap.put("Host", "wq.jd.com");
        headMap.put("Referer", "https://wqs.jd.com/order/orderlist_merge.shtml?orderType=waitPay&ptag=7155.1.12&sceneval=2");
        return apiService.getPayid(paramsMap, headMap).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    private String getRandom() {
        double random = 0;
        while (random == 0) {
            random = Math.random();
        }
        return (random + "").substring(0, 18);

    }

    /**
     * 校验payid
     *
     */
    @GET("https://pay.m.jd.com/newpay/index.action")
    public Observable<ResponseBody> checkPay(String payId) {
        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put("lastPage", "https://wqs.jd.com/order/orderlist_merge.shtml?orderType=waitPay&ptag=7155.1.12&sceneval=2");
        paramsMap.put("appId", "jd_m_pay");
        paramsMap.put("payId", payId);
        paramsMap.put("_format_", "JSON");


        Map<String, String> headMap = new HashMap<>();
        headMap.put("Cookie", App.getPtkey());
        headMap.put("Host", "pay.m.jd.com");
        headMap.put("Referer", "https://pay.m.jd.com/cpay/newPay-index.html?payId=" + payId + "&appId=jd_m_pay");
        headMap.put("Content-Type", "application/x-www-form-urlencoded");
        headMap.put("User-Agent", "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Mobile Safari/537.36");
        return apiService.newpay(paramsMap, headMap).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * 拿支付链接
     **/
    @POST("https://pay.m.jd.com/index.action")
    public Observable<ResponseBody> getPayUrl(String wareid, String payId) {
        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put("functionId", "wapWeiXinPay");
        paramsMap.put("body", "{\"appId\":\"jd_m_pay\",\"payId\":\"" + payId + "\"}");
        paramsMap.put("appId", "jd_m_pay");
        paramsMap.put("payId", payId);
        paramsMap.put("_format_", "JSON");

        Map<String, String> headMap = new HashMap<>();
        headMap.put("Cookie", App.getPtkey());
        headMap.put("Host", "pay.m.jd.com");
        headMap.put("Referer", "https://item.m.jd.com/ware/view.action?wareId=" + wareid + "&clickUrl=");
        return apiService.getPayUrl(paramsMap, headMap).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }
}
