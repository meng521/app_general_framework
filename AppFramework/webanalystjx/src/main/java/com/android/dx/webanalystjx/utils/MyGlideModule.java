package com.android.dx.webanalystjx.utils;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Author: Ligang
 * Date: 2019/12/10
 * Description:
 */
@GlideModule
public final class MyGlideModule extends AppGlideModule {

}
