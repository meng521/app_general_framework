package com.android.dx.webanalystjx.api;

import java.util.Map;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

/**
 * Author: Ligang
 * Date: 2020/01/03
 * Description:
 */
public interface JdApiService {
    @GET("https://wqsou.jd.com/search/searchjson")
    Observable<ResponseBody> getShopGoodsList(@QueryMap Map<String, String> map);

    /**
     * 加入购物车
     */
    @GET("https://wqdeal.jd.com/deal/confirmorder/main")
    Observable<ResponseBody> addCart(@QueryMap Map<String, String> map, @HeaderMap Map<String, String> headMap);

    /**
     * 下单
     */
    @FormUrlEncoded
    @POST("https://wqdeal.jd.com/deal/msubmit/confirm")
    Observable<ResponseBody> commitOrder(@FieldMap Map<String, String> map, @HeaderMap Map<String, String> headMap);

    /**
     * 获取payid
     */
    @GET("https://wq.jd.com/jdpaygw/jdappmpay")
    Observable<ResponseBody> getPayid(@QueryMap Map<String, String> map, @HeaderMap Map<String, String> headMap);

    /**
     * newpay
     */
    @GET("https://pay.m.jd.com/newpay/index.action")
    Observable<ResponseBody> newpay(@QueryMap Map<String, String> map, @HeaderMap Map<String, String> headMap);

    /**
     * 拿支付链接
     */
    @FormUrlEncoded
    @POST("https://pay.m.jd.com/index.action")
    Observable<ResponseBody> getPayUrl(@FieldMap Map<String, String> map, @HeaderMap Map<String, String> headMap);


}
