package com.android.dx.webanalystjx.api;

import com.android.dx.webanalystjx.model.HttpResponse;
import com.lg.meng.api.BaseServerResponseError;

/**
 * Author: Ligang
 * Date: 2020/01/04
 * Description:
 */
public class ServerResponseError extends BaseServerResponseError {
    private HttpResponse httpResponse;

    public ServerResponseError(HttpResponse httpResponse) {
        super(httpResponse);
        this.httpResponse = httpResponse;
    }

    public HttpResponse getHttpResponse() {
        return httpResponse;
    }

    public void setHttpResponse(HttpResponse httpResponse) {
        this.httpResponse = httpResponse;
    }
}
