package com.android.dx.webanalystjx.mvp.presenter;

import android.text.TextUtils;

import com.android.dx.webanalystjx.api.ApiClient;
import com.android.dx.webanalystjx.api.ServerResponseError;
import com.android.dx.webanalystjx.mvp.view.AddBuyerView;
import com.blankj.utilcode.util.RegexUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.lg.meng.base.BasePresenter;
import com.lg.meng.model.BaseServerResponse;

/**
 * Author: Ligang
 * Date: 2020/01/05
 * Description:
 */
public class AddBuyerPresenter extends BasePresenter<AddBuyerView> {
    public void addBuyer(String phone, String pt_key) {
        if (!checkInput(phone)) {
            return;
        }
        showLoadingDialog(false, "正在添加请稍后");
        ApiClient.getInstance().saveBuyer(phone, pt_key)
                .subscribe(simpleObserver(new Callback<BaseServerResponse>() {
                    @Override
                    public void resultSuccess(BaseServerResponse data) {
                        dissmissLoadingDialog();
                        view.addBuyerSuccess();
                    }

                    @Override
                    public void resultError(Throwable e) {
                        dissmissLoadingDialog();
                        view.addBuyerFailed();
                    }
                }));
    }

    private boolean checkInput(String phone) {
        if (TextUtils.isEmpty(phone)) {
            ToastUtils.showShort("请填写手机号");
            return false;
        }

        if (!RegexUtils.isMobileSimple(phone)) {
            ToastUtils.showShort("请填写合法的手机号");
            return false;
        }
        return true;
    }
}
