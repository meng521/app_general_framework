package com.android.dx.webanalystjx.model;

import com.lg.meng.model.BaseServerResponse;

/**
 * Author: Ligang
 * Date: 2020/01/04
 * Description:
 */
public class HttpResponse<T> extends BaseServerResponse {
    private String message;
    private int code;
    private T data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public boolean isShowErrorMsg() {
        return !isSuccess();
    }

    @Override
    public String getErrorMsg() {
        return message;
    }

    public boolean isSuccess() {
        return code == 200;
    }

}
