package com.android.dx.webanalystjx.adapter;

import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.android.dx.webanalystjx.R;
import com.android.dx.webanalystjx.app.App;
import com.android.dx.webanalystjx.model.Goods;
import com.android.dx.webanalystjx.utils.GlideApp;
import com.chad.library.adapter.base.BaseViewHolder;
import com.lg.meng.base.BaseEmptyQuickAdapter;

import java.util.List;

/**
 * Author: Ligang
 * Date: 2020/01/03
 * Description:
 */
public class GoodsListAdapter extends BaseEmptyQuickAdapter<Goods, BaseViewHolder> {
    public GoodsListAdapter(List<Goods> data) {
        super(R.layout.item_goods_list, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, Goods item) {
        helper.setText(R.id.tv_goods_title, item.getTitle());
        helper.setText(R.id.tv_goods_price, "￥" + item.getPrice());
        GlideApp.with(App.instance).load("https://img10.360buyimg.com/n7/s525x525_" + item.getImageurl()).into((ImageView) helper.getView(R.id.iv_goods));
    }
}
