package com.android.dx.webanalystjx.mvp.presenter;

import android.content.Intent;
import android.net.Uri;

import com.android.dx.webanalystjx.api.JdApiClient;
import com.android.dx.webanalystjx.app.App;
import com.android.dx.webanalystjx.model.CommitOrderResponse;
import com.android.dx.webanalystjx.model.GetPayDeeplinkResponse;
import com.android.dx.webanalystjx.model.GetPayidResponse;
import com.android.dx.webanalystjx.mvp.view.AddCartView;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.lg.meng.base.BasePresenter;
import com.lg.meng.utils.ClipboardUtils;
import com.lg.meng.utils.ThreadUtils;

import java.io.IOException;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import okhttp3.ResponseBody;

import static com.blankj.utilcode.util.ActivityUtils.startActivity;

/**
 * Author: Ligang
 * Date: 2020/01/03
 * Description:
 */
public class AddCardPresenter extends BasePresenter<AddCartView> {
    public void addCart(String wareid) {
        showLoadingDialog(false);
        JdApiClient.getInstance().addCart(wareid)
                .subscribe(new Observer<ResponseBody>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        try {
                            if (responseBody.string().contains("确认订单")) {
                                ThreadUtils.post(1000, () -> commitOrder(wareid));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            dissmissLoadingDialog();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        ToastUtils.showLong(e.getMessage());
                        dissmissLoadingDialog();
                    }

                    @Override
                    public void onComplete() {
                        dissmissLoadingDialog();
                    }
                });
    }

    private void commitOrder(String wareid) {
        JdApiClient.getInstance().commitOrder(wareid)
                .subscribe(new Observer<ResponseBody>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        try {
                            String string = responseBody.string();
                            LogUtils.e("下单返回：\n" + string);
                            CommitOrderResponse commitOrderResponse = App.gson.fromJson(string, CommitOrderResponse.class);
                            if (commitOrderResponse.getErrId().equals("0")) {
                                ToastUtils.showLong("下单成功");
                                ThreadUtils.post(1000, () -> getPayid(wareid, commitOrderResponse.getDealId()));
                            } else {
                                ToastUtils.showLong(commitOrderResponse.getErrMsg());
                                dissmissLoadingDialog();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                            ToastUtils.showLong("下单异常");
                            dissmissLoadingDialog();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtils.e("下单异常：\n" + e.getMessage());
                        ToastUtils.showLong("下单异常");
                        dissmissLoadingDialog();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void getPayid(String wareid, String dealId) {
        JdApiClient.getInstance().getPayid(dealId)
                .subscribe(new Observer<ResponseBody>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        try {
                            String string = responseBody.string();
                            LogUtils.e("getPayid返回：\n" + string);
                            GetPayidResponse commitOrderResponse = App.gson.fromJson(string, GetPayidResponse.class);
                            if (commitOrderResponse.getErrno() == 0) {
                                String jumpUrl = commitOrderResponse.getData().getJumpurl();
                                String payId = jumpUrl.split("payId=")[1].split("&")[0];
                                checkPay(wareid, payId);
                            } else {
                                ToastUtils.showLong("getPayid失败");
                                dissmissLoadingDialog();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                            ToastUtils.showLong("getPayid失败");
                            dissmissLoadingDialog();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        ToastUtils.showLong("getPayid失败：" + e.getMessage());
                        dissmissLoadingDialog();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void checkPay(String wareid, String payId) {
        JdApiClient.getInstance().checkPay(payId)
                .subscribe(new Observer<ResponseBody>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        getPayUrl(wareid, payId);
                    }

                    @Override
                    public void onError(Throwable e) {
                        dissmissLoadingDialog();
                        ToastUtils.showLong("checkPay失败：" + e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    /**
     * @param wareid 100004466546
     * @param payId  d5466d7ef5e94ff6b637b5de6ff6b6b3
     */
    public void getPayUrl(String wareid, String payId) {
        JdApiClient.getInstance().getPayUrl(wareid, payId)
                .subscribe(new Observer<ResponseBody>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        try {
                            GetPayDeeplinkResponse response = App.gson.fromJson(responseBody.string(), GetPayDeeplinkResponse.class);
                            ToastUtils.showLong(response.getDeepLink());
                            ClipboardUtils.copyText(response.getDeepLink());
//                            ((BaseMvpActivity) context).jumpTo(PayActivity.class, response.getDeepLink());

                            Uri uri = Uri.parse(response.getDeepLink());
                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                            startActivity(intent);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        dissmissLoadingDialog();
                    }

                    @Override
                    public void onError(Throwable e) {
                        ToastUtils.showLong("getPayUrl失败：" + e.getMessage());
                        dissmissLoadingDialog();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
