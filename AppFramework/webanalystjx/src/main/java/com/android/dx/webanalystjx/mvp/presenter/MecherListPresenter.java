package com.android.dx.webanalystjx.mvp.presenter;

import com.android.dx.webanalystjx.api.ApiClient;
import com.android.dx.webanalystjx.model.HttpResponse;
import com.android.dx.webanalystjx.model.MerchResponse;
import com.android.dx.webanalystjx.mvp.view.MecherListView;
import com.lg.meng.base.BasePresenter;

import java.util.List;

/**
 * Author: Ligang
 * Date: 2020/01/04
 * Description:
 */
public class MecherListPresenter extends BasePresenter<MecherListView> {
    public void getMecherList() {
        ApiClient.getInstance().getMechers()
                .subscribe(simpleObserver(new Callback<HttpResponse<List<MerchResponse>>>() {
                    @Override
                    public void resultSuccess(HttpResponse<List<MerchResponse>> data) {
                        if(data.getData() == null || data.getData().size() == 0){
                            view.getMecherListEmpty();
                            return;
                        }
                        view.getMecherListSuccess(data.getData());
                    }

                    @Override
                    public void resultError(Throwable e) {
                        view.getMecherListFailed();
                    }
                }));
    }
}
