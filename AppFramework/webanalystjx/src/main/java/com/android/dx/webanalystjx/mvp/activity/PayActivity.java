package com.android.dx.webanalystjx.mvp.activity;

import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.android.dx.webanalystjx.BaseTitleActivity;
import com.android.dx.webanalystjx.R;
import com.just.agentweb.AgentWeb;
import com.just.agentweb.DefaultWebClient;
import com.just.agentweb.WebChromeClient;
import com.just.agentweb.WebViewClient;

import butterknife.BindView;

/**
 * Author: Ligang
 * Date: 2020/01/04
 * Description:
 */
public class PayActivity extends BaseTitleActivity {
    @BindView(R.id.web_container)
    LinearLayout webContainer;

    AgentWeb mAgentWeb;

    @Override
    protected String provideTitle() {
        return "支付";
    }

    @Override
    protected int provideContentViewId() {
        return R.layout.activity_pay;
    }

    @Override
    protected void beforeSetContentView() {

    }

    String deeplink;

    @Override
    protected void afterSetContentView() {
        deeplink = (String) getParam(0);

        mAgentWeb = AgentWeb.with(this)
                .setAgentWebParent(webContainer, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
                .useDefaultIndicator(-1, 3)
//                .setAgentWebWebSettings(getSettings())
                .setWebViewClient(mWebViewClient)
                .setWebChromeClient(mWebChromeClient)
                .setSecurityType(AgentWeb.SecurityType.STRICT_CHECK)
                .setMainFrameErrorView(R.layout.agentweb_error_page, -1)
//                .useMiddlewareWebChrome(getMiddlewareWebChrome())
//                .useMiddlewareWebClient(getMiddlewareWebClient())
                .setOpenOtherPageWays(DefaultWebClient.OpenOtherPageWays.DISALLOW)
//                .interceptUnkownUrl()
                .createAgentWeb()
                .ready()
                .go(deeplink);
    }

    private WebChromeClient mWebChromeClient = new WebChromeClient() {

    };
    private WebViewClient mWebViewClient = new WebViewClient() {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return true;
        }
    };

    @Override
    protected void onPause() {
        if (mAgentWeb != null) {
            mAgentWeb.getWebLifeCycle().onPause();
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        if (mAgentWeb != null) {
            mAgentWeb.getWebLifeCycle().onResume();
        }
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        if (mAgentWeb != null) {
            mAgentWeb.getWebLifeCycle().onDestroy();
        }
        super.onDestroy();
    }
}