package com.android.dx.webanalystjx.model;

/**
 * Author: Ligang
 * Date: 2020/01/03
 * Description:
 */
public class CommitOrderResponse {

    /**
     * errId : 0
     * errMsg :
     * nextUrl :
     * idc :
     * traceId : 813513445315401579
     * resultCode :
     * pin : ishiheiye
     * appid : wxae3e8056daea8727
     * dealId : 108888730759
     * totalPrice : 319900
     * ordeType : 0
     * riskResult :
     * phoneNumber :
     * uuid :
     */

    private String errId;
    private String errMsg;
    private String nextUrl;
    private String idc;
    private String traceId;
    private String resultCode;
    private String pin;
    private String appid;
    private String dealId;
    private String totalPrice;
    private String ordeType;
    private String riskResult;
    private String phoneNumber;
    private String uuid;

    public String getErrId() {
        return errId;
    }

    public void setErrId(String errId) {
        this.errId = errId;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public String getNextUrl() {
        return nextUrl;
    }

    public void setNextUrl(String nextUrl) {
        this.nextUrl = nextUrl;
    }

    public String getIdc() {
        return idc;
    }

    public void setIdc(String idc) {
        this.idc = idc;
    }

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getOrdeType() {
        return ordeType;
    }

    public void setOrdeType(String ordeType) {
        this.ordeType = ordeType;
    }

    public String getRiskResult() {
        return riskResult;
    }

    public void setRiskResult(String riskResult) {
        this.riskResult = riskResult;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
