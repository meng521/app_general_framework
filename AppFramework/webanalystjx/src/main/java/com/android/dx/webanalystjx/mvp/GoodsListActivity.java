package com.android.dx.webanalystjx.mvp;

import android.support.v7.widget.RecyclerView;

import com.android.dx.webanalystjx.BaseTitleActivity;
import com.android.dx.webanalystjx.R;
import com.android.dx.webanalystjx.adapter.GoodsListAdapter;
import com.android.dx.webanalystjx.api.JdApiClient;
import com.android.dx.webanalystjx.model.Goods;
import com.android.dx.webanalystjx.mvp.presenter.AddCardPresenter;
import com.android.dx.webanalystjx.mvp.view.AddCartView;
import com.blankj.utilcode.util.LogUtils;
import com.lg.meng.BindPresenter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import okhttp3.ResponseBody;

/**
 * Author: Ligang
 * Date: 2020/01/03
 * Description:
 */
public class GoodsListActivity extends BaseTitleActivity implements AddCartView {
    private List<Goods> goodsList = new ArrayList<>();
    private GoodsListAdapter goodsListAdapter;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindPresenter
    public AddCardPresenter addCardPresenter;

    @Override
    protected String provideTitle() {
        return "商品列表";
    }

    @Override
    protected int provideContentViewId() {
        return R.layout.activity_goods_list;
    }

    @Override
    protected void beforeSetContentView() {

    }

    @Override
    protected void afterSetContentView() {
        goodsListAdapter = new GoodsListAdapter(goodsList);
        recyclerView.setAdapter(goodsListAdapter);
        getGoodsList();

        goodsListAdapter.setOnItemClickListener((adapter, view, position) -> {
            addCardPresenter.addCart(goodsList.get(position).getWareid());
//            ToastUtils.showLong(sdf.format(new Date(1578039815397L)));
//            addCardPresenter.getPayUrl("100004466546","d5466d7ef5e94ff6b637b5de6ff6b6b3");
        });
    }

    private void getGoodsList() {
        JdApiClient.getInstance().getShopGoodsList("1000003445")
                .subscribe(new Observer<ResponseBody>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        try {
                            String string = responseBody.string();
                            string = string.replace("searchCB(", "").trim();
                            string = string.substring(0, string.length() - 1);
                            LogUtils.e(string);
                            JSONObject jsonObject = new JSONObject(string);
                            JSONObject dataObj = jsonObject.getJSONObject("data");
                            JSONArray paragraphList = dataObj.getJSONObject("searchm").getJSONArray("Paragraph");
                            goodsList.clear();
                            for (int i = 0; i < paragraphList.length(); i++) {
                                Goods goods = new Goods();
                                JSONObject obj = paragraphList.getJSONObject(i);
                                String imageurl = obj.getJSONObject("Content").getString("imageurl");
                                String warename = obj.getJSONObject("Content").getString("warename");
                                String dredisprice = obj.getString("dredisprice");
                                goods.setImageurl(imageurl);
                                goods.setTitle(warename);
                                goods.setPrice(dredisprice);
                                goods.setWareid(obj.getString("wareid"));
                                goodsList.add(goods);
                            }
                            goodsListAdapter.notifyDataSetChanged();
                            LogUtils.e(goodsList.get(0).toString());
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (Exception e) {

                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void commitOrder(String wareid) {

    }
}
