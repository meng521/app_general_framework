package com.android.dx.webanalystjx.model;

/**
 * Author: Ligang
 * Date: 2020/01/03
 * Description:
 */
public class Goods{
    private String title;
    private String price;
    private String wareid;
    private String imageurl;

    @Override
    public String toString() {
        return "Goods{" +
                "title='" + title + '\'' +
                ", price='" + price + '\'' +
                ", wareid='" + wareid + '\'' +
                ", imageurl='" + imageurl + '\'' +
                '}';
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getWareid() {
        return wareid;
    }

    public void setWareid(String wareid) {
        this.wareid = wareid;
    }
}
