package com.android.dx.webanalystjx.mvp.activity;

import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.android.dx.webanalystjx.BaseTitleActivity;
import com.android.dx.webanalystjx.JxLoginActivity;
import com.android.dx.webanalystjx.R;
import com.android.dx.webanalystjx.app.App;
import com.android.dx.webanalystjx.mvp.presenter.AddBuyerPresenter;
import com.android.dx.webanalystjx.mvp.view.AddBuyerView;
import com.lg.meng.BindPresenter;
import com.lg.meng.dialog.CommonTipDialog;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Author: Ligang
 * Date: 2020/01/04
 * Description:
 */
public class AddBuyerActivity extends BaseTitleActivity implements AddBuyerView {
    @BindPresenter
    AddBuyerPresenter addBuyerPresenter;
    @BindView(R.id.et_phone)
    EditText etPhone;

    @Override
    protected String provideTitle() {
        return "添加买手";
    }

    @Override
    protected int provideContentViewId() {
        return R.layout.activity_add_buyer;
    }

    @Override
    protected void beforeSetContentView() {

    }

    @Override
    protected void afterSetContentView() {
        findViewById(R.id.tv_back).setVisibility(View.GONE);
        findViewById(R.id.btn_back).setVisibility(View.GONE);
        String phoneNum = (String) getParam(0);
        if (!TextUtils.isEmpty(phoneNum)) {
            etPhone.getText().append(phoneNum);
        }

        right("重新登录", v -> {
            jumpTo(JxLoginActivity.class);
            finish();
        });
    }

    @OnClick({R.id.btn_add})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_add:
                addBuyerPresenter.addBuyer(etPhone.getText().toString(), App.getPtkey());
                break;
        }
    }

    @Override
    public void addBuyerSuccess() {
        CommonTipDialog.newInstance(activity)
                .title("添加买手成功")
                .setOkButton("好的", (dialog, which) -> {
                    finish();
                    jumpTo(JxLoginActivity.class);
                })
                .cancelable(false)
                .canceledOnTouchOutside(false).show();
    }

    @Override
    public void addBuyerFailed() {

    }
}
