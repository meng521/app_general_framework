package com.android.dx.webanalystjx.model;

/**
 * Author: Ligang
 * Date: 2020/01/04
 * Description:
 */
public class GetPayDeeplinkResponse {

    /**
     * code : 0
     * deepLink : weixin://wap/pay?appid%3Dwxae3e8056daea8727%26noncestr%3Da3bf6e4db673b6449c2f7d13ee6ec9c0%26package%3DWAP%26prepayid%3Dwx040041232513986598072f7e1805459000%26timestamp%3D1578069683%26sign%3DC4183A99C93FDA3DDA4C39948369ADF8
     * jdPrePayId : 112545020010400412305839
     * message :
     * payEnum : 1125
     */

    private String code;
    private String deepLink;
    private String jdPrePayId;
    private String message;
    private String payEnum;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDeepLink() {
        return deepLink;
    }

    public void setDeepLink(String deepLink) {
        this.deepLink = deepLink;
    }

    public String getJdPrePayId() {
        return jdPrePayId;
    }

    public void setJdPrePayId(String jdPrePayId) {
        this.jdPrePayId = jdPrePayId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPayEnum() {
        return payEnum;
    }

    public void setPayEnum(String payEnum) {
        this.payEnum = payEnum;
    }
}
