package com.android.dx.webanalystjx.mvp.view;

import com.android.dx.webanalystjx.model.MerchResponse;
import com.lg.meng.base.BaseMvpView;

import java.util.List;

/**
 * Author: Ligang
 * Date: 2020/01/04
 * Description:
 */
public interface MecherListView extends BaseMvpView {
    void getMecherListSuccess(List<MerchResponse> data);

    void getMecherListFailed();

    void getMecherListEmpty();
}
