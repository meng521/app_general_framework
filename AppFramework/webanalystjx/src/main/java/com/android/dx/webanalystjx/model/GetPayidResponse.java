package com.android.dx.webanalystjx.model;

/**
 * Author: Ligang
 * Date: 2020/01/03
 * Description:
 */
public class GetPayidResponse {

    /**
     * errno : 0
     * msg :
     * rurl :
     * data : {"jumpurl":"https://pay.m.jd.com/cpay/pay-index.html?payId=71af45df721442aabafaec26dcda1712&appId=jd_m_pay"}
     */

    private int errno;
    private String msg;
    private String rurl;
    private DataBean data;

    public int getErrno() {
        return errno;
    }

    public void setErrno(int errno) {
        this.errno = errno;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getRurl() {
        return rurl;
    }

    public void setRurl(String rurl) {
        this.rurl = rurl;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * jumpurl : https://pay.m.jd.com/cpay/pay-index.html?payId=71af45df721442aabafaec26dcda1712&appId=jd_m_pay
         */

        private String jumpurl;

        public String getJumpurl() {
            return jumpurl;
        }

        public void setJumpurl(String jumpurl) {
            this.jumpurl = jumpurl;
        }
    }
}
