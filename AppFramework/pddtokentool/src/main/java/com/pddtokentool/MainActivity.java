package com.pddtokentool;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.just.agentweb.AgentWeb;
import com.just.agentweb.DefaultWebClient;
import com.just.agentweb.WebChromeClient;
import com.just.agentweb.WebViewClient;
import com.lg.meng.utils.AppToast;
import com.lg.meng.utils.ClipboardUtils;
import com.lg.meng.utils.ThreadUtils;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {
    @BindView(R.id.web_container)
    LinearLayout webContainer;

    AgentWeb mAgentWeb;

    @Override
    protected int provideContentViewId() {
        return R.layout.activity_main;
    }

    @Override
    protected void beforeSetContentView() {

    }

    String pddLoginUrl = "http://app.yangkeduo.com/login.html";

    @Override
    protected void afterSetContentView() {
        AgentWeb.PreAgentWeb preAgentWeb = AgentWeb.with(this)
                .setAgentWebParent(webContainer, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
                .useDefaultIndicator(-1, 3)
                .setWebViewClient(mWebViewClient)
                .setWebChromeClient(mWebChromeClient)
                .setSecurityType(AgentWeb.SecurityType.STRICT_CHECK)
                .setMainFrameErrorView(R.layout.agentweb_error_page, -1)
                .setOpenOtherPageWays(DefaultWebClient.OpenOtherPageWays.DISALLOW)
                .interceptUnkownUrl()
                .createAgentWeb()
                .ready();
        mAgentWeb = preAgentWeb.go(pddLoginUrl);
//        if (!TextUtils.isEmpty(App.getPtkey())) {
//            mAgentWeb.clearWebCache();
//            mAgentWeb.getWebCreator().getWebView().loadUrl(pddLoginUrl);
//        }
        startTask();
        jumpTo(SuningpingouActivity.class);
    }

    private WebChromeClient mWebChromeClient = new WebChromeClient() {

    };
    private WebViewClient mWebViewClient = new WebViewClient() {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
//            if (request.getUrl().toString().contains("pinduoduo://com")) {
//                return true;
//            }
            return super.shouldOverrideUrlLoading(view, request);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
//            if (url.contains("pinduoduo://com")) {
//                return true;
//            }
            return super.shouldOverrideUrlLoading(view, url);
        }
    };

    @Override
    protected void onPause() {
        if (mAgentWeb != null) {
            mAgentWeb.getWebLifeCycle().onPause();
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        if (mAgentWeb != null) {
            mAgentWeb.getWebLifeCycle().onResume();
        }
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        if (mAgentWeb != null) {
            mAgentWeb.getWebLifeCycle().onDestroy();
        }
        super.onDestroy();
    }

    @Override
    protected void exeDestory() {
        super.exeDestory();
        stopTask();
    }

    private Timer timer = new Timer();
    private TimerTask timerTask;

    private void startTask() {
        if (timerTask == null) {
            timerTask = new TimerTask() {
                @Override
                public void run() {
                    if (!TextUtils.isEmpty(App.getPddCookie())) {
                        ThreadUtils.runOnUiThread(() -> {
                            AppToast.showLong("token复制成功\n" + App.getPddCookie());
                        });
                        stopTask();
                    }
                }
            };
            timer.schedule(timerTask, 1000, 1000);
        }
    }

    private void stopTask() {
        if (timerTask != null) {
            timerTask.cancel();
            timerTask = null;
        }
    }

    private void log(Object obj) {
        Log.e(TAG, obj + "");
    }

    private static final String TAG = MainActivity.class.getName();

    @OnClick({R.id.tv_right, R.id.tv_left})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_right:
                mAgentWeb.clearWebCache();
                mAgentWeb.getWebCreator().getWebView().loadUrl(pddLoginUrl);
                startTask();
                break;
            case R.id.tv_left:
                String pddCookie = App.getPddCookie();
                if (TextUtils.isEmpty(pddCookie)) {
                    AppToast.showLong("请先登录拼多多");
                } else {
                    ClipboardUtils.copyText(pddCookie);
                    AppToast.showLong("token复制成功\n" + pddCookie);
                }
                break;
        }
    }
}
