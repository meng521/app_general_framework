package com.pddtokentool;

import android.app.Application;
import android.text.TextUtils;

import com.blankj.utilcode.util.LogUtils;
import com.google.gson.Gson;
import com.just.agentweb.AgentWebConfig;
import com.lg.meng.application.StubApplication;

/**
 * Author: Ligang
 * Date: 2019/11/07
 * Description:
 */
public class App extends Application {
    public static Gson gson;
    public static App instance;

    @Override
    public void onCreate() {
        super.onCreate();

        gson = new Gson();
        instance = this;
        StubApplication.init(instance);
    }

    public static String getPddCookie() {
        String cookies = AgentWebConfig.getCookiesByUrl("http://app.yangkeduo.com");
        if (cookies != null && cookies.contains("PDDAccessToken")) {
            String[] strings = cookies.split(";");
            if (strings != null && strings.length > 0) {
                for (String str : strings) {
                    if (str.contains("PDDAccessToken")) {
                        return str.trim().split("=")[1];
                    }
                }
            }
        }
        return "";
    }
}
