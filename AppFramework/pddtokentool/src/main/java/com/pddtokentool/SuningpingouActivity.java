package com.pddtokentool;

import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.just.agentweb.AgentWeb;
import com.just.agentweb.DefaultWebClient;
import com.just.agentweb.WebChromeClient;
import com.just.agentweb.WebViewClient;
import com.lg.meng.utils.AppToast;
import com.lg.meng.utils.ClipboardUtils;
import com.lg.meng.utils.FileUtils;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Author: Ligang
 * Date: 2020/01/05
 * Description:
 */
public class SuningpingouActivity extends BaseActivity {
    @BindView(R.id.web_container)
    LinearLayout webContainer;

    AgentWeb mAgentWeb;

    String pddLoginUrl = "https://cuxiao.suning.com/group_index.html";

    String commitOrderUrl = "https://pintrade.suning.com/pgs/order/private/indSubmitOrder.do";

    @Override
    protected int provideContentViewId() {
        return R.layout.activity_suningpingou;
    }

    @Override
    protected void beforeSetContentView() {

    }

    @Override
    protected void afterSetContentView() {
        AgentWeb.PreAgentWeb preAgentWeb = AgentWeb.with(this)
                .setAgentWebParent(webContainer, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
                .useDefaultIndicator(-1, 3)
                .setWebViewClient(mWebViewClient)
                .setWebChromeClient(mWebChromeClient)
                .setSecurityType(AgentWeb.SecurityType.STRICT_CHECK)
                .setMainFrameErrorView(R.layout.agentweb_error_page, -1)
                .setOpenOtherPageWays(DefaultWebClient.OpenOtherPageWays.DISALLOW)
                .interceptUnkownUrl()
                .createAgentWeb()
                .ready();
        mAgentWeb = preAgentWeb.go(pddLoginUrl);
        mAgentWeb.getJsInterfaceHolder().addJavaObject("interception", new AndroidInterface());
    }

    private class AndroidInterface {
        @JavascriptInterface
        public void open(String requestID, String url, String method) {
            Log.e("===", "open:" + requestID + ",url:" + url + ",method:" + method);
        }

        @JavascriptInterface
        public void send(String requestID, String body) {
            Log.e("===", "send:" + requestID + ",body:" + body);
        }
    }

    private WebChromeClient mWebChromeClient = new WebChromeClient() {

    };
    private WebViewClient mWebViewClient = new WebViewClient() {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
//            if (request.getUrl().toString().contains("pinduoduo://com")) {
//                return true;
//            }
            log("1shouldOverrideUrlLoading:" + request.getUrl().toString());

            return super.shouldOverrideUrlLoading(view, request);
        }


        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
//            String js = FileUtils.readAssetFile("interceptheader.html");
//            view.loadUrl(js);
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            String js = FileUtils.readAssetFile("interceptheader.html");
            view.loadUrl(js);
            super.onPageFinished(view, url);
        }

        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
            log("1shouldInterceptRequest:" + request.getUrl().toString());
            String url = request.getUrl().toString();
            if (url.startsWith(commitOrderUrl)) {
            }
            return super.shouldInterceptRequest(view, request);
        }

    };


    @Override
    protected void onPause() {
        if (mAgentWeb != null) {
            mAgentWeb.getWebLifeCycle().onPause();
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        if (mAgentWeb != null) {
            mAgentWeb.getWebLifeCycle().onResume();
        }
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        if (mAgentWeb != null) {
            mAgentWeb.getWebLifeCycle().onDestroy();
        }
        super.onDestroy();
    }

    @Override
    protected void exeDestory() {
        super.exeDestory();
    }

    private void log(Object obj) {
        Log.e(TAG, obj + "");
    }

    private static final String TAG = SuningpingouActivity.class.getName();

    @OnClick({R.id.tv_right, R.id.tv_left})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_right:
                mAgentWeb.clearWebCache();
                mAgentWeb.getWebCreator().getWebView().loadUrl(pddLoginUrl);
                break;
            case R.id.tv_left:
                String pddCookie = App.getPddCookie();
                if (TextUtils.isEmpty(pddCookie)) {
                    AppToast.showLong("请先登录拼多多");
                } else {
                    ClipboardUtils.copyText(pddCookie);
                    AppToast.showLong("token复制成功\n" + pddCookie);
                }
                break;
        }
    }
}
