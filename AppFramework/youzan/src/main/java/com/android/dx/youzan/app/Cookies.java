package com.android.dx.youzan.app;

/**
 * Author: Ligang
 * Date: 2020/01/10
 * Description:
 */
public class Cookies {
    @Override
    public String toString() {
        return "Cookies{" +
                "cookie='" + cookie + '\'' +
                ", _csrf='" + _csrf + '\'' +
                '}';
    }

    private String cookie;
    private String _csrf;

    public String getCookie() {
        return cookie;
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
    }

    public String get_csrf() {
        return _csrf;
    }

    public void set_csrf(String _csrf) {
        this._csrf = _csrf;
    }
}
