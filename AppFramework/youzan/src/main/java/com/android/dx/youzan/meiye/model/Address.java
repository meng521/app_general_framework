package com.android.dx.youzan.meiye.model;

/**
 * Author: Ligang
 * Date: 2020/01/10
 * Description:
 */
public class Address {

    /**
     * id : 124321480
     * userId : 488346362
     * userNickName : 李刚
     * phone : 17711322848
     * areaCode : 510107
     * postCode : 610041
     * country : 中国
     * province : 四川省
     * city : 成都市
     * county : 武侯区
     * address : 晋阳巷1号11栋
     * community :
     * isDefault : 1
     * lat : 30.656010041179
     * lon : 104.00771015288
     */

    private int id;
    private int userId;
    private String userNickName;
    private String phone;
    private String areaCode;
    private String postCode;
    private String country;
    private String province;
    private String city;
    private String county;
    private String address;
    private String community;
    private int isDefault;
    private String lat;
    private String lon;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserNickName() {
        return userNickName;
    }

    public void setUserNickName(String userNickName) {
        this.userNickName = userNickName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCommunity() {
        return community;
    }

    public void setCommunity(String community) {
        this.community = community;
    }

    public int getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(int isDefault) {
        this.isDefault = isDefault;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }
}
