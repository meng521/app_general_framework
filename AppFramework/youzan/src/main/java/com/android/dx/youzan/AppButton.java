package com.android.dx.youzan;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Author: Ligang
 * Date: 2020/01/10
 * Description:
 */
public class AppButton extends android.support.v7.widget.AppCompatTextView {
    public AppButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTextAppearance(context, R.style.CommonFilTextButtonStyle);
    }
}
