package com.android.dx.youzan;


import android.Manifest;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.dx.youzan.base.BaseActivity;
import com.android.dx.youzan.meiye.MeiyeMainActivity;
import com.android.dx.youzan.utils.FileDownloadUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.lg.meng.utils.PermissionsUtils;
import com.lg.meng.utils.ThreadUtils;

import java.io.File;

import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {
    @BindView(R.id.myview)
    MyView myView;
    @BindView(R.id.et_x)
    EditText etX;
    @BindView(R.id.et_y)
    EditText etY;
    @BindView(R.id.tv)
    TextView tv;
    Bitmap bitmap;
    Bitmap bitmap2;

    @Override
    protected int provideContentViewId() {
        return R.layout.activity_main;
    }

    @Override
    protected void beforeSetContentView() {

    }

    @Override
    protected void afterSetContentView() {
        PermissionsUtils.permissionRun(activity, () -> {

        }, "读写手机存储", Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    protected boolean isUserLightMode() {
        return true;
    }

    @OnClick({R.id.btn1, R.id.btn2})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn1:
                jumpTo(MeiyeMainActivity.class);
                break;
            case R.id.btn2:
                ThreadUtils.runOnThread(() -> {
                    String bigUrl = "https://img.yzcdn.cn/public_files/2019/04/01/067c1344e556590b31ec9a70aeaa3690.png";
                    String originUrl = "https://img.yzcdn.cn/public_files/2019/04/01/298083c8cbb3ebfc0dbfb5c80d2ca5fc.png";
                    File[] files = FileDownloadUtils.download(bigUrl, originUrl);
                    if (files == null) {
                        ThreadUtils.runOnUiThread(() -> {
                            ToastUtils.showLong("图片下载失败");
                        });
                    } else {
                        bitmap = BitmapFactory.decodeFile(files[0].getAbsolutePath());
                        bitmap2 = BitmapFactory.decodeFile(files[1].getAbsolutePath());
                        ThreadUtils.runOnUiThread(() -> myView.setBitmap(bitmap));
                        find();
                    }

                });
                break;
        }
    }

    private void find() {
        ThreadUtils.runOnThread(() -> {
            int y = 43 + 27;
            int touchCount = 0;
            for (int i = 1; i < bitmap.getWidth(); i++) {
                if (Math.abs(bitmap.getPixel(i, y) - bitmap2.getPixel(i, y)) > 100000) {
                    if (findX == 0) {
                        findX = i;
                    }
                    touchCount++;
                    if (touchCount >= 20) {
                        ThreadUtils.runOnUiThread(() -> {
                            ToastUtils.showLong(findX + "");
                        });
                        return;
                    }
                    continue;
                }
                touchCount = 0;
                findX = 0;
                Log.e("=========", "(" + i + "," + y + ") " + (bitmap.getPixel(i, y) - bitmap2.getPixel(i, y)) + "");
            }

        });
    }

    int findX;

}
