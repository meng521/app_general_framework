package com.android.dx.youzan.meiye;

import android.view.View;
import android.widget.TextView;

import com.android.dx.youzan.R;
import com.android.dx.youzan.app.App;
import com.android.dx.youzan.base.BaseTitleActivity;
import com.lg.meng.utils.AppToast;
import com.lg.meng.utils.ClipboardUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Author: Ligang
 * Date: 2020/01/10
 * Description:
 */
public class MeiyeMainActivity extends BaseTitleActivity {
    @BindView(R.id.tv_msg)
    TextView tvMsg;

    @Override
    protected int provideContentViewId() {
        return R.layout.activity_youzan_meiye;
    }

    @Override
    protected void beforeSetContentView() {

    }

    @Override
    protected void afterSetContentView() {
        findViewById(R.id.btn_back).setVisibility(View.GONE);
        findViewById(R.id.tv_back).setVisibility(View.GONE);
    }

    @OnClick({R.id.btn0, R.id.btn1, R.id.btn5, R.id.btn_req, R.id.btn_req2, R.id.btn_req3})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn0:
                jumpTo(LoginMeiyeActivity.class);
                break;

            case R.id.btn5:
                jumpTo(LoginMeiyeStoreActivity.class);
                break;
            case R.id.btn1:
                if (App.getCookie() == null) {
                    AppToast.show("未登录");
                } else {
                    AppToast.show(App.getCookie().toString());
                    ClipboardUtils.copyText(App.getCookie().getCookie());
                }

                break;
            case R.id.btn_req:
                if (App.getCookie() == null) {
                    AppToast.show("未登录");
                    return;
                }
                tvMsg.setText("");
                ServiceMain.main(null);
                break;
            case R.id.btn_req2:
                if (App.getCookie() == null) {
                    AppToast.show("未登录");
                    return;
                }
                tvMsg.setText("");
                CardMain.main(null);
                break;
            case R.id.btn_req3:
                if (App.getCookie() == null) {
                    AppToast.show("未登录");
                    return;
                }
                tvMsg.setText("");
                ProductMain.main(null);
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(String string) {
        tvMsg.append(string + "\n");
    }

    @Override
    protected String provideTitle() {
        return "有赞美业";
    }
}
