package com.android.dx.youzan.meiye;

import android.util.Log;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.android.dx.youzan.R;
import com.android.dx.youzan.app.App;
import com.android.dx.youzan.base.BaseTitleActivity;
import com.blankj.utilcode.util.LogUtils;
import com.just.agentweb.AgentWeb;
import com.just.agentweb.DefaultWebClient;
import com.just.agentweb.WebViewClient;
import com.lg.meng.utils.FileUtils;

import butterknife.BindView;

/**
 * Author: Ligang
 * Date: 2020/01/10
 * Description:
 */
public class LoginMeiyeStoreActivity extends BaseTitleActivity {
    @BindView(R.id.web_container)
    LinearLayout webContainer;

    AgentWeb mAgentWeb;

    //    String loginUrl = "https://passport.youzan.com/login/password";
    String loginUrl = "https://account.youzan.com/login?product=beauty";
    String storeUrl = "https://mei.youzan.com/dashboard";

    @Override
    protected String provideTitle() {
        return "有赞美业店铺";
    }

    @Override
    protected int provideContentViewId() {
        return R.layout.activity_login_meiye;
    }

    @Override
    protected void beforeSetContentView() {

    }

    @Override
    protected void afterSetContentView() {
        AgentWeb.PreAgentWeb preAgentWeb = AgentWeb.with(this)
                .setAgentWebParent(webContainer, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
                .useDefaultIndicator(-1, 3)
                .setWebViewClient(mWebViewClient)
                .setSecurityType(AgentWeb.SecurityType.STRICT_CHECK)
                .setMainFrameErrorView(R.layout.agentweb_error_page, -1)
                .setOpenOtherPageWays(DefaultWebClient.OpenOtherPageWays.DISALLOW)
                .interceptUnkownUrl()
                .createAgentWeb()
                .ready();
        if (App.getStoreCookie() != null) {
            
            mAgentWeb = preAgentWeb.go(storeUrl);
        } else {
            mAgentWeb = preAgentWeb.go(loginUrl);
        }

//        mAgentWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidInterface());
//        mAgentWeb.clearWebCache();
//        mAgentWeb.getWebCreator().getWebView().loadUrl(loginUrl);

        right("清除Cookie", view -> {
            mAgentWeb.clearWebCache();
            mAgentWeb.getWebCreator().getWebView().loadUrl(loginUrl);
        });
    }

    private WebViewClient mWebViewClient = new WebViewClient() {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
//            String js = FileUtils.readAssetFile("getphone.js");
//            view.loadUrl(js);
//            LogUtils.e("zhurujS:\n" + js);
            Log.e("===========","onPageFinished:" + url);
            if(url.startsWith("https://www.youzan.com/v2/shop/list")){
                mAgentWeb.getWebCreator().getWebView().loadUrl("https://mei.youzan.com/dashboard");
            }
        }
    };
}
