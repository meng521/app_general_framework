package com.android.dx.youzan.meiye.model;

import java.util.List;

/**
 * Author: Ligang
 * Date: 2020/01/10
 * Description:
 */
public class Buyer {

    /**
     * code : 10000
     * message : 正确
     * data : {"yzUid":488346362,"kdtId":43992027,"name":"买买萌萌萌萌萌萌萌萌好你","gender":0,"growth":0,"lockGrowth":0,"lockLevel":0,"mobile":"17711322848","memberNo":"47500533","member":1,"fullMember":1,"profileInfo":{"gender":0,"birthday":"","weixin":"","areaCode":0,"address":"","wxNickName":"","remark":""},"memberLevelSettingDTO":{"levelId":132816309872,"levelAlias":"abkxe1t5hjnnle","kdtId":43992027,"level":1,"levelName":"普通会员","levelValue":0,"nextLevelValue":5000,"levelRightSettingJsonList":[{"enable":1,"levelRightType":1,"memberRightType":203}]},"avatar":"","consultantIds":[],"isHidden":0,"countryCode":"+86","currentPoints":0,"uicNickName":"买买萌萌萌萌萌萌萌萌好你","uicAvatar":"http://thirdwx.qlogo.cn/mmopen/ajNVdqHZLLByDoZ25lRs7We255ZGQXhL0OhqnboRgeWr7ozCJjcY2IBOlibzCC9q1sDXialXmt8aibgFgU6FqpBzA/132","uicGender":0,"accountBalance":0,"cardCount":0,"couponCount":0,"memberRightsListVOList":[{"rightsType":203,"title":"生日祝福","gift":0}],"waitPayCount":0,"waitShipCount":0,"waitReceiptCount":0,"waitCommentCount":0,"waitArriveCount":0}
     * desc : 正确
     */

    private int code;
    private String message;
    private DataBean data;
    private String desc;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static class DataBean {
        /**
         * yzUid : 488346362
         * kdtId : 43992027
         * name : 买买萌萌萌萌萌萌萌萌好你
         * gender : 0
         * growth : 0
         * lockGrowth : 0
         * lockLevel : 0
         * mobile : 17711322848
         * memberNo : 47500533
         * member : 1
         * fullMember : 1
         * profileInfo : {"gender":0,"birthday":"","weixin":"","areaCode":0,"address":"","wxNickName":"","remark":""}
         * memberLevelSettingDTO : {"levelId":132816309872,"levelAlias":"abkxe1t5hjnnle","kdtId":43992027,"level":1,"levelName":"普通会员","levelValue":0,"nextLevelValue":5000,"levelRightSettingJsonList":[{"enable":1,"levelRightType":1,"memberRightType":203}]}
         * avatar :
         * consultantIds : []
         * isHidden : 0
         * countryCode : +86
         * currentPoints : 0
         * uicNickName : 买买萌萌萌萌萌萌萌萌好你
         * uicAvatar : http://thirdwx.qlogo.cn/mmopen/ajNVdqHZLLByDoZ25lRs7We255ZGQXhL0OhqnboRgeWr7ozCJjcY2IBOlibzCC9q1sDXialXmt8aibgFgU6FqpBzA/132
         * uicGender : 0
         * accountBalance : 0
         * cardCount : 0
         * couponCount : 0
         * memberRightsListVOList : [{"rightsType":203,"title":"生日祝福","gift":0}]
         * waitPayCount : 0
         * waitShipCount : 0
         * waitReceiptCount : 0
         * waitCommentCount : 0
         * waitArriveCount : 0
         */

        private int yzUid;
        private int kdtId;
        private String name;
        private int gender;
        private int growth;
        private int lockGrowth;
        private int lockLevel;
        private String mobile;
        private String memberNo;
        private int member;
        private int fullMember;
        private ProfileInfoBean profileInfo;
        private MemberLevelSettingDTOBean memberLevelSettingDTO;
        private String avatar;
        private int isHidden;
        private String countryCode;
        private int currentPoints;
        private String uicNickName;
        private String uicAvatar;
        private int uicGender;
        private int accountBalance;
        private int cardCount;
        private int couponCount;
        private int waitPayCount;
        private int waitShipCount;
        private int waitReceiptCount;
        private int waitCommentCount;
        private int waitArriveCount;
        private List<?> consultantIds;
        private List<MemberRightsListVOListBean> memberRightsListVOList;

        public int getYzUid() {
            return yzUid;
        }

        public void setYzUid(int yzUid) {
            this.yzUid = yzUid;
        }

        public int getKdtId() {
            return kdtId;
        }

        public void setKdtId(int kdtId) {
            this.kdtId = kdtId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getGender() {
            return gender;
        }

        public void setGender(int gender) {
            this.gender = gender;
        }

        public int getGrowth() {
            return growth;
        }

        public void setGrowth(int growth) {
            this.growth = growth;
        }

        public int getLockGrowth() {
            return lockGrowth;
        }

        public void setLockGrowth(int lockGrowth) {
            this.lockGrowth = lockGrowth;
        }

        public int getLockLevel() {
            return lockLevel;
        }

        public void setLockLevel(int lockLevel) {
            this.lockLevel = lockLevel;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getMemberNo() {
            return memberNo;
        }

        public void setMemberNo(String memberNo) {
            this.memberNo = memberNo;
        }

        public int getMember() {
            return member;
        }

        public void setMember(int member) {
            this.member = member;
        }

        public int getFullMember() {
            return fullMember;
        }

        public void setFullMember(int fullMember) {
            this.fullMember = fullMember;
        }

        public ProfileInfoBean getProfileInfo() {
            return profileInfo;
        }

        public void setProfileInfo(ProfileInfoBean profileInfo) {
            this.profileInfo = profileInfo;
        }

        public MemberLevelSettingDTOBean getMemberLevelSettingDTO() {
            return memberLevelSettingDTO;
        }

        public void setMemberLevelSettingDTO(MemberLevelSettingDTOBean memberLevelSettingDTO) {
            this.memberLevelSettingDTO = memberLevelSettingDTO;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public int getIsHidden() {
            return isHidden;
        }

        public void setIsHidden(int isHidden) {
            this.isHidden = isHidden;
        }

        public String getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        public int getCurrentPoints() {
            return currentPoints;
        }

        public void setCurrentPoints(int currentPoints) {
            this.currentPoints = currentPoints;
        }

        public String getUicNickName() {
            return uicNickName;
        }

        public void setUicNickName(String uicNickName) {
            this.uicNickName = uicNickName;
        }

        public String getUicAvatar() {
            return uicAvatar;
        }

        public void setUicAvatar(String uicAvatar) {
            this.uicAvatar = uicAvatar;
        }

        public int getUicGender() {
            return uicGender;
        }

        public void setUicGender(int uicGender) {
            this.uicGender = uicGender;
        }

        public int getAccountBalance() {
            return accountBalance;
        }

        public void setAccountBalance(int accountBalance) {
            this.accountBalance = accountBalance;
        }

        public int getCardCount() {
            return cardCount;
        }

        public void setCardCount(int cardCount) {
            this.cardCount = cardCount;
        }

        public int getCouponCount() {
            return couponCount;
        }

        public void setCouponCount(int couponCount) {
            this.couponCount = couponCount;
        }

        public int getWaitPayCount() {
            return waitPayCount;
        }

        public void setWaitPayCount(int waitPayCount) {
            this.waitPayCount = waitPayCount;
        }

        public int getWaitShipCount() {
            return waitShipCount;
        }

        public void setWaitShipCount(int waitShipCount) {
            this.waitShipCount = waitShipCount;
        }

        public int getWaitReceiptCount() {
            return waitReceiptCount;
        }

        public void setWaitReceiptCount(int waitReceiptCount) {
            this.waitReceiptCount = waitReceiptCount;
        }

        public int getWaitCommentCount() {
            return waitCommentCount;
        }

        public void setWaitCommentCount(int waitCommentCount) {
            this.waitCommentCount = waitCommentCount;
        }

        public int getWaitArriveCount() {
            return waitArriveCount;
        }

        public void setWaitArriveCount(int waitArriveCount) {
            this.waitArriveCount = waitArriveCount;
        }

        public List<?> getConsultantIds() {
            return consultantIds;
        }

        public void setConsultantIds(List<?> consultantIds) {
            this.consultantIds = consultantIds;
        }

        public List<MemberRightsListVOListBean> getMemberRightsListVOList() {
            return memberRightsListVOList;
        }

        public void setMemberRightsListVOList(List<MemberRightsListVOListBean> memberRightsListVOList) {
            this.memberRightsListVOList = memberRightsListVOList;
        }

        public static class ProfileInfoBean {
            /**
             * gender : 0
             * birthday :
             * weixin :
             * areaCode : 0
             * address :
             * wxNickName :
             * remark :
             */

            private int gender;
            private String birthday;
            private String weixin;
            private int areaCode;
            private String address;
            private String wxNickName;
            private String remark;

            public int getGender() {
                return gender;
            }

            public void setGender(int gender) {
                this.gender = gender;
            }

            public String getBirthday() {
                return birthday;
            }

            public void setBirthday(String birthday) {
                this.birthday = birthday;
            }

            public String getWeixin() {
                return weixin;
            }

            public void setWeixin(String weixin) {
                this.weixin = weixin;
            }

            public int getAreaCode() {
                return areaCode;
            }

            public void setAreaCode(int areaCode) {
                this.areaCode = areaCode;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getWxNickName() {
                return wxNickName;
            }

            public void setWxNickName(String wxNickName) {
                this.wxNickName = wxNickName;
            }

            public String getRemark() {
                return remark;
            }

            public void setRemark(String remark) {
                this.remark = remark;
            }
        }

        public static class MemberLevelSettingDTOBean {
            /**
             * levelId : 132816309872
             * levelAlias : abkxe1t5hjnnle
             * kdtId : 43992027
             * level : 1
             * levelName : 普通会员
             * levelValue : 0
             * nextLevelValue : 5000
             * levelRightSettingJsonList : [{"enable":1,"levelRightType":1,"memberRightType":203}]
             */

            private long levelId;
            private String levelAlias;
            private int kdtId;
            private int level;
            private String levelName;
            private int levelValue;
            private int nextLevelValue;
            private List<LevelRightSettingJsonListBean> levelRightSettingJsonList;

            public long getLevelId() {
                return levelId;
            }

            public void setLevelId(long levelId) {
                this.levelId = levelId;
            }

            public String getLevelAlias() {
                return levelAlias;
            }

            public void setLevelAlias(String levelAlias) {
                this.levelAlias = levelAlias;
            }

            public int getKdtId() {
                return kdtId;
            }

            public void setKdtId(int kdtId) {
                this.kdtId = kdtId;
            }

            public int getLevel() {
                return level;
            }

            public void setLevel(int level) {
                this.level = level;
            }

            public String getLevelName() {
                return levelName;
            }

            public void setLevelName(String levelName) {
                this.levelName = levelName;
            }

            public int getLevelValue() {
                return levelValue;
            }

            public void setLevelValue(int levelValue) {
                this.levelValue = levelValue;
            }

            public int getNextLevelValue() {
                return nextLevelValue;
            }

            public void setNextLevelValue(int nextLevelValue) {
                this.nextLevelValue = nextLevelValue;
            }

            public List<LevelRightSettingJsonListBean> getLevelRightSettingJsonList() {
                return levelRightSettingJsonList;
            }

            public void setLevelRightSettingJsonList(List<LevelRightSettingJsonListBean> levelRightSettingJsonList) {
                this.levelRightSettingJsonList = levelRightSettingJsonList;
            }

            public static class LevelRightSettingJsonListBean {
                /**
                 * enable : 1
                 * levelRightType : 1
                 * memberRightType : 203
                 */

                private int enable;
                private int levelRightType;
                private int memberRightType;

                public int getEnable() {
                    return enable;
                }

                public void setEnable(int enable) {
                    this.enable = enable;
                }

                public int getLevelRightType() {
                    return levelRightType;
                }

                public void setLevelRightType(int levelRightType) {
                    this.levelRightType = levelRightType;
                }

                public int getMemberRightType() {
                    return memberRightType;
                }

                public void setMemberRightType(int memberRightType) {
                    this.memberRightType = memberRightType;
                }
            }
        }

        public static class MemberRightsListVOListBean {
            /**
             * rightsType : 203
             * title : 生日祝福
             * gift : 0
             */

            private int rightsType;
            private String title;
            private int gift;

            public int getRightsType() {
                return rightsType;
            }

            public void setRightsType(int rightsType) {
                this.rightsType = rightsType;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public int getGift() {
                return gift;
            }

            public void setGift(int gift) {
                this.gift = gift;
            }
        }
    }
}
