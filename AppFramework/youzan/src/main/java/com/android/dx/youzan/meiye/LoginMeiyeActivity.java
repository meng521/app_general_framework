package com.android.dx.youzan.meiye;

import android.text.TextUtils;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.android.dx.youzan.R;
import com.android.dx.youzan.app.App;
import com.android.dx.youzan.base.BaseTitleActivity;
import com.blankj.utilcode.util.LogUtils;
import com.just.agentweb.AgentWeb;
import com.just.agentweb.DefaultWebClient;
import com.just.agentweb.WebViewClient;
import com.lg.meng.utils.FileUtils;

import butterknife.BindView;

/**
 * Author: Ligang
 * Date: 2020/01/10
 * Description:
 */
public class LoginMeiyeActivity extends BaseTitleActivity {
    @BindView(R.id.web_container)
    LinearLayout webContainer;

    AgentWeb mAgentWeb;

    //    String loginUrl = "https://passport.youzan.com/login/password";
    String loginUrl = "https://passport.youzan.com/login/password?redirectUrl=https%3A%2F%2Fmei1cxndhebhhhelo.youzan.com%2Fapi%2Fnode%2Fauth%2Fsso%2Fcallback%2Fphone%3Fredirect%3Dhttps%253A%252F%252Fmei1cxndhebhhhelo.youzan.com%252Fh5%252Fmember%253FdeptId%253D43991843%2526kdtId%253D43992027";
    String storeUrl = "https://mei1cxndhebhhhelo.youzan.com/h5/home?deptId=43991843&kdtId=43992027";

    @Override
    protected String provideTitle() {
        return "有赞美业买手";
    }

    @Override
    protected int provideContentViewId() {
        return R.layout.activity_login_meiye;
    }

    @Override
    protected void beforeSetContentView() {

    }

    @Override
    protected void afterSetContentView() {
        AgentWeb.PreAgentWeb preAgentWeb = AgentWeb.with(this)
                .setAgentWebParent(webContainer, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
                .useDefaultIndicator(-1, 3)
//                .setWebViewClient(mWebViewClient)
//                .setWebChromeClient(mWebChromeClient)
                .setSecurityType(AgentWeb.SecurityType.STRICT_CHECK)
                .setMainFrameErrorView(R.layout.agentweb_error_page, -1)
                .setOpenOtherPageWays(DefaultWebClient.OpenOtherPageWays.DISALLOW)
                .interceptUnkownUrl()
                .createAgentWeb()
                .ready();
        if (App.getCookie() != null) {
            mAgentWeb = preAgentWeb.go(storeUrl);
        } else {
            mAgentWeb = preAgentWeb.go(loginUrl);
        }

//        mAgentWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidInterface());
//        mAgentWeb.clearWebCache();
//        mAgentWeb.getWebCreator().getWebView().loadUrl(loginUrl);

        right("清除Cookie", view -> {
            mAgentWeb.clearWebCache();
            mAgentWeb.getWebCreator().getWebView().loadUrl(loginUrl);
        });
    }

    private WebViewClient mWebViewClient = new WebViewClient() {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            String js = FileUtils.readAssetFile("getphone.js");
            view.loadUrl(js);
            LogUtils.e("zhurujS:\n" + js);
        }
    };
}
