package com.android.dx.youzan.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.android.dx.youzan.R;

/**
 * Author: Ligang
 * Date: 2019/11/16
 * Description:
 */
public abstract class BaseTitleActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        back().title(provideTitle());
    }

    protected abstract String provideTitle();

    private BaseTitleActivity back() {
        View btnBack = findViewById(R.id.btn_back);
        View tvBack = findViewById(R.id.tv_back);
        if (btnBack != null) {
            btnBack.setOnClickListener(v -> onBackPressed());
        }
        if (tvBack != null) {
            tvBack.setOnClickListener(v -> onBackPressed());
        }
        return this;
    }

    private BaseTitleActivity title(CharSequence title) {
        TextView titleTv = findViewById(R.id.tv_title);
        if (titleTv != null) {
            titleTv.setText(title);
        }
        return this;
    }

    public BaseTitleActivity right(int rightTextId, View.OnClickListener clickListener) {
        return right(getString(rightTextId), clickListener);
    }

    public BaseTitleActivity right(CharSequence rightText, View.OnClickListener clickListener) {
        TextView rightTv = findViewById(R.id.tv_right);
        if (rightTv != null) {
            rightTv.setText(rightText);
            rightTv.setVisibility(View.VISIBLE);
            rightTv.setOnClickListener(clickListener);
        }
        return this;
    }
}
