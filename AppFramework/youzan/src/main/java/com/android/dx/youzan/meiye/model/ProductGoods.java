package com.android.dx.youzan.meiye.model;

import java.util.List;

/**
 * Author: Ligang
 * Date: 2020/01/10
 * Description:
 */
public class ProductGoods {

    /**
     * goodsType : 2
     * id : 1683910
     * kdtId : 43990976
     * deptId : 43993058
     * goodsId : 541081531
     * goodsAlias : 35vjt4h7eh7y8
     * title : ReFa CARAT RAY限定版三色渐变滚轮美容按摩仪
     * snapshotId : QnRjYqfKU96dMNPhPj4vl4mB
     * goodsNo : A0000000790701
     * tags : [105660267]
     * goodsPictureList : [{"id":1531006,"imageUrl":"http://img.yzcdn.cn/upload_files/2020/01/10/Fjp14fdXTzVjYb0n6RotUAftzXYO.jpg","width":790,"height":981,"isDeptSelf":1}]
     * skuTreeList : []
     * priceRange : [279900]
     * originalText :
     * wxShow : 1
     * onShelve : 1
     * seq : 0
     * fromType : 1
     * num : 0
     * hasSku : 0
     * createTime : 1578647106000
     * updateTime : 1578647106000
     * price : 279900
     * postage : 0
     * totalStockNum : 99
     * totalSoldNum : 0
     * soldStatus : 1
     * shelveNum : 0
     * activityTogether : 0
     * status : 1
     * tagList : [{"goodsType":1,"tagId":105660267,"name":"美容"}]
     * tagIdList : [105660267]
     * priceRangeMin : 279900
     * priceRangeMax : 279900
     * itemType : 2
     * defaultGoodsStock : {"nameList":[],"nameVOList":[],"skuId":36496701,"code":"A0000000790701","price":279900,"costPrice":0,"stockNum":99,"totalSoldNum":0,"goodsId":541081531}
     */

    private int goodsType;
    private int id;
    private int kdtId;
    private int deptId;
    private int goodsId;
    private String goodsAlias;
    private String title;
    private String snapshotId;
    private String goodsNo;
    private String originalText;
    private int wxShow;
    private int onShelve;
    private int seq;
    private int fromType;
    private String num;
    private int hasSku;
    private long createTime;
    private long updateTime;
    private int price;
    private int postage;
    private int totalStockNum;
    private int totalSoldNum;
    private int soldStatus;
    private int shelveNum;
    private int activityTogether;
    private int status;
    private int priceRangeMin;
    private int priceRangeMax;
    private int itemType;
    private DefaultGoodsStockBean defaultGoodsStock;
    private List<Integer> tags;
    private List<GoodsPictureListBean> goodsPictureList;
    private List<?> skuTreeList;
    private List<Integer> priceRange;
    private List<TagListBean> tagList;
    private List<Integer> tagIdList;

    public int getGoodsType() {
        return goodsType;
    }

    public void setGoodsType(int goodsType) {
        this.goodsType = goodsType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getKdtId() {
        return kdtId;
    }

    public void setKdtId(int kdtId) {
        this.kdtId = kdtId;
    }

    public int getDeptId() {
        return deptId;
    }

    public void setDeptId(int deptId) {
        this.deptId = deptId;
    }

    public int getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(int goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsAlias() {
        return goodsAlias;
    }

    public void setGoodsAlias(String goodsAlias) {
        this.goodsAlias = goodsAlias;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSnapshotId() {
        return snapshotId;
    }

    public void setSnapshotId(String snapshotId) {
        this.snapshotId = snapshotId;
    }

    public String getGoodsNo() {
        return goodsNo;
    }

    public void setGoodsNo(String goodsNo) {
        this.goodsNo = goodsNo;
    }

    public String getOriginalText() {
        return originalText;
    }

    public void setOriginalText(String originalText) {
        this.originalText = originalText;
    }

    public int getWxShow() {
        return wxShow;
    }

    public void setWxShow(int wxShow) {
        this.wxShow = wxShow;
    }

    public int getOnShelve() {
        return onShelve;
    }

    public void setOnShelve(int onShelve) {
        this.onShelve = onShelve;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public int getFromType() {
        return fromType;
    }

    public void setFromType(int fromType) {
        this.fromType = fromType;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public int getHasSku() {
        return hasSku;
    }

    public void setHasSku(int hasSku) {
        this.hasSku = hasSku;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getPostage() {
        return postage;
    }

    public void setPostage(int postage) {
        this.postage = postage;
    }

    public int getTotalStockNum() {
        return totalStockNum;
    }

    public void setTotalStockNum(int totalStockNum) {
        this.totalStockNum = totalStockNum;
    }

    public int getTotalSoldNum() {
        return totalSoldNum;
    }

    public void setTotalSoldNum(int totalSoldNum) {
        this.totalSoldNum = totalSoldNum;
    }

    public int getSoldStatus() {
        return soldStatus;
    }

    public void setSoldStatus(int soldStatus) {
        this.soldStatus = soldStatus;
    }

    public int getShelveNum() {
        return shelveNum;
    }

    public void setShelveNum(int shelveNum) {
        this.shelveNum = shelveNum;
    }

    public int getActivityTogether() {
        return activityTogether;
    }

    public void setActivityTogether(int activityTogether) {
        this.activityTogether = activityTogether;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getPriceRangeMin() {
        return priceRangeMin;
    }

    public void setPriceRangeMin(int priceRangeMin) {
        this.priceRangeMin = priceRangeMin;
    }

    public int getPriceRangeMax() {
        return priceRangeMax;
    }

    public void setPriceRangeMax(int priceRangeMax) {
        this.priceRangeMax = priceRangeMax;
    }

    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    public DefaultGoodsStockBean getDefaultGoodsStock() {
        return defaultGoodsStock;
    }

    public void setDefaultGoodsStock(DefaultGoodsStockBean defaultGoodsStock) {
        this.defaultGoodsStock = defaultGoodsStock;
    }

    public List<Integer> getTags() {
        return tags;
    }

    public void setTags(List<Integer> tags) {
        this.tags = tags;
    }

    public List<GoodsPictureListBean> getGoodsPictureList() {
        return goodsPictureList;
    }

    public void setGoodsPictureList(List<GoodsPictureListBean> goodsPictureList) {
        this.goodsPictureList = goodsPictureList;
    }

    public List<?> getSkuTreeList() {
        return skuTreeList;
    }

    public void setSkuTreeList(List<?> skuTreeList) {
        this.skuTreeList = skuTreeList;
    }

    public List<Integer> getPriceRange() {
        return priceRange;
    }

    public void setPriceRange(List<Integer> priceRange) {
        this.priceRange = priceRange;
    }

    public List<TagListBean> getTagList() {
        return tagList;
    }

    public void setTagList(List<TagListBean> tagList) {
        this.tagList = tagList;
    }

    public List<Integer> getTagIdList() {
        return tagIdList;
    }

    public void setTagIdList(List<Integer> tagIdList) {
        this.tagIdList = tagIdList;
    }

    public static class DefaultGoodsStockBean {
        /**
         * nameList : []
         * nameVOList : []
         * skuId : 36496701
         * code : A0000000790701
         * price : 279900
         * costPrice : 0
         * stockNum : 99
         * totalSoldNum : 0
         * goodsId : 541081531
         */

        private int skuId;
        private String code;
        private int price;
        private int costPrice;
        private int stockNum;
        private int totalSoldNum;
        private int goodsId;
        private List<?> nameList;
        private List<?> nameVOList;

        public int getSkuId() {
            return skuId;
        }

        public void setSkuId(int skuId) {
            this.skuId = skuId;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public int getCostPrice() {
            return costPrice;
        }

        public void setCostPrice(int costPrice) {
            this.costPrice = costPrice;
        }

        public int getStockNum() {
            return stockNum;
        }

        public void setStockNum(int stockNum) {
            this.stockNum = stockNum;
        }

        public int getTotalSoldNum() {
            return totalSoldNum;
        }

        public void setTotalSoldNum(int totalSoldNum) {
            this.totalSoldNum = totalSoldNum;
        }

        public int getGoodsId() {
            return goodsId;
        }

        public void setGoodsId(int goodsId) {
            this.goodsId = goodsId;
        }

        public List<?> getNameList() {
            return nameList;
        }

        public void setNameList(List<?> nameList) {
            this.nameList = nameList;
        }

        public List<?> getNameVOList() {
            return nameVOList;
        }

        public void setNameVOList(List<?> nameVOList) {
            this.nameVOList = nameVOList;
        }
    }

    public static class GoodsPictureListBean {
        /**
         * id : 1531006
         * imageUrl : http://img.yzcdn.cn/upload_files/2020/01/10/Fjp14fdXTzVjYb0n6RotUAftzXYO.jpg
         * width : 790
         * height : 981
         * isDeptSelf : 1
         */

        private int id;
        private String imageUrl;
        private int width;
        private int height;
        private int isDeptSelf;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public int getWidth() {
            return width;
        }

        public void setWidth(int width) {
            this.width = width;
        }

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }

        public int getIsDeptSelf() {
            return isDeptSelf;
        }

        public void setIsDeptSelf(int isDeptSelf) {
            this.isDeptSelf = isDeptSelf;
        }
    }

    public static class TagListBean {
        /**
         * goodsType : 1
         * tagId : 105660267
         * name : 美容
         */

        private int goodsType;
        private int tagId;
        private String name;

        public int getGoodsType() {
            return goodsType;
        }

        public void setGoodsType(int goodsType) {
            this.goodsType = goodsType;
        }

        public int getTagId() {
            return tagId;
        }

        public void setTagId(int tagId) {
            this.tagId = tagId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
