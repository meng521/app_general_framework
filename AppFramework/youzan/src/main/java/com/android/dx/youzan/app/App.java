package com.android.dx.youzan.app;

import android.app.Application;
import android.text.TextUtils;

import com.blankj.utilcode.util.LogUtils;
import com.google.gson.Gson;
import com.just.agentweb.AgentWebConfig;
import com.lg.meng.application.StubApplication;
import com.lg.meng.utils.FileUtils;

/**
 * Author: Ligang
 * Date: 2019/11/07
 * Description:
 */
public class App extends Application {
    public static Gson gson;
    public static App instance;

    public static String HOST;

    @Override
    public void onCreate() {
        super.onCreate();
        gson = new Gson();
        instance = this;
        StubApplication.init(instance);
//        HOST = FileUtils.readAssetFile("host.txt").trim().replace(" ", "").replace("\n", "");
    }

    public static Cookies getCookie() {
        String cookies = AgentWebConfig.getCookiesByUrl("https://mei1cxndhebhhhelo.youzan.com");
        App.getStoreCookie();
        if (!TextUtils.isEmpty(cookies)) {
            LogUtils.e("cookies:\n" + cookies);
            String[] strings = cookies.split(";");
            StringBuffer sb = new StringBuffer();
            String _csrf = "";
            if (strings != null && strings.length > 0) {
                for (String str : strings) {
                    if (str.contains("mei_h5_csrf_token")) {
                        sb.append(str + ";");
                        _csrf = str.split("=")[1];
                    } else if (str.contains("m_token")) {
                        sb.append(str + ";");
                    }
                }
            }
            if (!TextUtils.isEmpty(sb.toString())) {
                Cookies c = new Cookies();
                c.set_csrf(_csrf);
                c.setCookie(sb.toString());
                return c;
            }
        }
        return null;
    }

    public static String getStoreCookie() {
        String cookies = AgentWebConfig.getCookiesByUrl("https://mei.youzan.com");
        if (!TextUtils.isEmpty(cookies)) {
            LogUtils.e("StoreCookie:" + cookies);
            return cookies;
        }
        return "";
    }
}
