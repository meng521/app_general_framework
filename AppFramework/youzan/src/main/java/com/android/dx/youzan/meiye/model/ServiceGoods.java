package com.android.dx.youzan.meiye.model;

import java.util.List;

/**
 * Author: Ligang
 * Date: 2020/01/10
 * Description:
 */
public class ServiceGoods {

    /**
     * goodsType : 1
     * id : 1910210
     * kdtId : 43990976
     * deptId : 43993058
     * goodsId : 573248177
     * goodsAlias : 2onyhf0dxyd9s
     * title : 泰式SPA
     * snapshotId : g0azKyFdUoNEwOWsze9y5Poj
     * goodsNo : A0000001043042
     * tags : [105634769,105634775]
     * goodsPictureList : [{"id":1526639,"imageUrl":"http://img.yzcdn.cn/upload_files/2020/01/09/FpeXSSYukMIPoIRINfD9bFO1QX7A.jpg","width":3517,"height":2500,"isDeptSelf":1}]
     * skuTreeList : []
     * priceRange : [39900]
     * originalText :
     * serviceDuration : 60
     * wxShow : 1
     * needPay : 1
     * onShelve : 1
     * seq : 0
     * fromType : 1
     * num : 0
     * hasSku : 0
     * createTime : 1578558678000
     * updateTime : 1578558678000
     * price : 39900
     * totalStockNum : 0
     * totalSoldNum : 0
     * soldStatus : 3
     * shelveNum : 0
     * activityTogether : 0
     * status : 1
     * preparationDuration : 0
     * tagList : [{"goodsType":1,"tagId":105634769,"name":"泰式"},{"goodsType":1,"tagId":105634775,"name":"spa"}]
     * tagIdList : [105634769,105634775]
     * priceRangeMin : 39900
     * priceRangeMax : 39900
     * itemType : 1
     * defaultGoodsStock : {"nameList":[],"nameVOList":[],"skuId":36487832,"code":"A0000001043042","price":39900,"costPrice":0,"serviceDuration":60,"stockNum":0,"totalSoldNum":0,"goodsId":573248177}
     */

    private int goodsType;
    private int id;
    private int kdtId;
    private int deptId;
    private int goodsId;
    private String goodsAlias;
    private String title;
    private String snapshotId;
    private String goodsNo;
    private String originalText;
    private int serviceDuration;
    private int wxShow;
    private int needPay;
    private int onShelve;
    private int seq;
    private int fromType;
    private String num;
    private int hasSku;
    private long createTime;
    private long updateTime;
    private int price;
    private int totalStockNum;
    private int totalSoldNum;
    private int soldStatus;
    private int shelveNum;
    private int activityTogether;
    private int status;
    private int preparationDuration;
    private int priceRangeMin;
    private int priceRangeMax;
    private int itemType;
    private DefaultGoodsStockBean defaultGoodsStock;
    private List<Integer> tags;
    private List<GoodsPictureListBean> goodsPictureList;
    private List<?> skuTreeList;
    private List<Integer> priceRange;
    private List<TagListBean> tagList;
    private List<Integer> tagIdList;

    public int getGoodsType() {
        return goodsType;
    }

    public void setGoodsType(int goodsType) {
        this.goodsType = goodsType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getKdtId() {
        return kdtId;
    }

    public void setKdtId(int kdtId) {
        this.kdtId = kdtId;
    }

    public int getDeptId() {
        return deptId;
    }

    public void setDeptId(int deptId) {
        this.deptId = deptId;
    }

    public int getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(int goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsAlias() {
        return goodsAlias;
    }

    public void setGoodsAlias(String goodsAlias) {
        this.goodsAlias = goodsAlias;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSnapshotId() {
        return snapshotId;
    }

    public void setSnapshotId(String snapshotId) {
        this.snapshotId = snapshotId;
    }

    public String getGoodsNo() {
        return goodsNo;
    }

    public void setGoodsNo(String goodsNo) {
        this.goodsNo = goodsNo;
    }

    public String getOriginalText() {
        return originalText;
    }

    public void setOriginalText(String originalText) {
        this.originalText = originalText;
    }

    public int getServiceDuration() {
        return serviceDuration;
    }

    public void setServiceDuration(int serviceDuration) {
        this.serviceDuration = serviceDuration;
    }

    public int getWxShow() {
        return wxShow;
    }

    public void setWxShow(int wxShow) {
        this.wxShow = wxShow;
    }

    public int getNeedPay() {
        return needPay;
    }

    public void setNeedPay(int needPay) {
        this.needPay = needPay;
    }

    public int getOnShelve() {
        return onShelve;
    }

    public void setOnShelve(int onShelve) {
        this.onShelve = onShelve;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public int getFromType() {
        return fromType;
    }

    public void setFromType(int fromType) {
        this.fromType = fromType;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public int getHasSku() {
        return hasSku;
    }

    public void setHasSku(int hasSku) {
        this.hasSku = hasSku;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getTotalStockNum() {
        return totalStockNum;
    }

    public void setTotalStockNum(int totalStockNum) {
        this.totalStockNum = totalStockNum;
    }

    public int getTotalSoldNum() {
        return totalSoldNum;
    }

    public void setTotalSoldNum(int totalSoldNum) {
        this.totalSoldNum = totalSoldNum;
    }

    public int getSoldStatus() {
        return soldStatus;
    }

    public void setSoldStatus(int soldStatus) {
        this.soldStatus = soldStatus;
    }

    public int getShelveNum() {
        return shelveNum;
    }

    public void setShelveNum(int shelveNum) {
        this.shelveNum = shelveNum;
    }

    public int getActivityTogether() {
        return activityTogether;
    }

    public void setActivityTogether(int activityTogether) {
        this.activityTogether = activityTogether;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getPreparationDuration() {
        return preparationDuration;
    }

    public void setPreparationDuration(int preparationDuration) {
        this.preparationDuration = preparationDuration;
    }

    public int getPriceRangeMin() {
        return priceRangeMin;
    }

    public void setPriceRangeMin(int priceRangeMin) {
        this.priceRangeMin = priceRangeMin;
    }

    public int getPriceRangeMax() {
        return priceRangeMax;
    }

    public void setPriceRangeMax(int priceRangeMax) {
        this.priceRangeMax = priceRangeMax;
    }

    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    public DefaultGoodsStockBean getDefaultGoodsStock() {
        return defaultGoodsStock;
    }

    public void setDefaultGoodsStock(DefaultGoodsStockBean defaultGoodsStock) {
        this.defaultGoodsStock = defaultGoodsStock;
    }

    public List<Integer> getTags() {
        return tags;
    }

    public void setTags(List<Integer> tags) {
        this.tags = tags;
    }

    public List<GoodsPictureListBean> getGoodsPictureList() {
        return goodsPictureList;
    }

    public void setGoodsPictureList(List<GoodsPictureListBean> goodsPictureList) {
        this.goodsPictureList = goodsPictureList;
    }

    public List<?> getSkuTreeList() {
        return skuTreeList;
    }

    public void setSkuTreeList(List<?> skuTreeList) {
        this.skuTreeList = skuTreeList;
    }

    public List<Integer> getPriceRange() {
        return priceRange;
    }

    public void setPriceRange(List<Integer> priceRange) {
        this.priceRange = priceRange;
    }

    public List<TagListBean> getTagList() {
        return tagList;
    }

    public void setTagList(List<TagListBean> tagList) {
        this.tagList = tagList;
    }

    public List<Integer> getTagIdList() {
        return tagIdList;
    }

    public void setTagIdList(List<Integer> tagIdList) {
        this.tagIdList = tagIdList;
    }

    public static class DefaultGoodsStockBean {
        /**
         * nameList : []
         * nameVOList : []
         * skuId : 36487832
         * code : A0000001043042
         * price : 39900
         * costPrice : 0
         * serviceDuration : 60
         * stockNum : 0
         * totalSoldNum : 0
         * goodsId : 573248177
         */

        private int skuId;
        private String code;
        private int price;
        private int costPrice;
        private int serviceDuration;
        private int stockNum;
        private int totalSoldNum;
        private int goodsId;
        private List<?> nameList;
        private List<?> nameVOList;

        public int getSkuId() {
            return skuId;
        }

        public void setSkuId(int skuId) {
            this.skuId = skuId;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public int getCostPrice() {
            return costPrice;
        }

        public void setCostPrice(int costPrice) {
            this.costPrice = costPrice;
        }

        public int getServiceDuration() {
            return serviceDuration;
        }

        public void setServiceDuration(int serviceDuration) {
            this.serviceDuration = serviceDuration;
        }

        public int getStockNum() {
            return stockNum;
        }

        public void setStockNum(int stockNum) {
            this.stockNum = stockNum;
        }

        public int getTotalSoldNum() {
            return totalSoldNum;
        }

        public void setTotalSoldNum(int totalSoldNum) {
            this.totalSoldNum = totalSoldNum;
        }

        public int getGoodsId() {
            return goodsId;
        }

        public void setGoodsId(int goodsId) {
            this.goodsId = goodsId;
        }

        public List<?> getNameList() {
            return nameList;
        }

        public void setNameList(List<?> nameList) {
            this.nameList = nameList;
        }

        public List<?> getNameVOList() {
            return nameVOList;
        }

        public void setNameVOList(List<?> nameVOList) {
            this.nameVOList = nameVOList;
        }
    }

    public static class GoodsPictureListBean {
        /**
         * id : 1526639
         * imageUrl : http://img.yzcdn.cn/upload_files/2020/01/09/FpeXSSYukMIPoIRINfD9bFO1QX7A.jpg
         * width : 3517
         * height : 2500
         * isDeptSelf : 1
         */

        private int id;
        private String imageUrl;
        private int width;
        private int height;
        private int isDeptSelf;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public int getWidth() {
            return width;
        }

        public void setWidth(int width) {
            this.width = width;
        }

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }

        public int getIsDeptSelf() {
            return isDeptSelf;
        }

        public void setIsDeptSelf(int isDeptSelf) {
            this.isDeptSelf = isDeptSelf;
        }
    }

    public static class TagListBean {
        /**
         * goodsType : 1
         * tagId : 105634769
         * name : 泰式
         */

        private int goodsType;
        private int tagId;
        private String name;

        public int getGoodsType() {
            return goodsType;
        }

        public void setGoodsType(int goodsType) {
            this.goodsType = goodsType;
        }

        public int getTagId() {
            return tagId;
        }

        public void setTagId(int tagId) {
            this.tagId = tagId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
