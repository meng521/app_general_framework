package com.android.dx.youzan.meiye.model;

import java.util.List;

/**
 * Author: Ligang
 * Date: 2020/01/10
 * Description:
 */
public class CardGoods {

    /**
     * id : 121009
     * kdtId : 43990976
     * deptId : 1
     * cardId : 111376250
     * cardAlias : Y3erthriiiists
     * cardSnapshotId : 582141
     * cardName : spa消费卡
     * cardType : 3
     * cardSubType : 0
     * itemId : 540988623
     * itemSnapshotId : b06WYyFNUb3r08WiJwqYgqg9
     * prepaidId : 2001101428074612
     * cardPrice : 9900
     * wapShow : 1
     * free : 0
     * onShelve : 1
     * available : 1
     * deleted : 0
     * onDept : 0
     * syncWx : 0
     * termDays : 0
     * description :
     * syncWxStatus : 0
     * supportGroup : 0
     * hasActiveGroup : 0
     * bindNumber : 0
     * onShelveDeptNumber : 0
     * selectDeptIds : [1]
     * updatedAtLong : 1578637687
     * createdAtLong : 1578637687
     * prepaidGiftPrice : 0
     * bgColor : #9058cb
     * bgImg : https://b.yzcdn.cn/beauty/card/bgimg/rechargeCard.png
     * bgTextHide : 0
     * bgNameHide : 0
     * displayGrayLayer : 1
     * activityGroupList : []
     * identificationCode :
     * distinguishAccount : 0
     * discountRightsList : [{"rightsId":20371167,"goodsId":105634767,"rightsType":1,"subRightsType":200,"unlimitedTimes":0,"gift":0,"value":100,"scope":2,"accountType":0,"isCardRight":0}]
     * timesRightsList : []
     * pointRightsList : []
     * rightsDesc :
     * giftRightsList : []
     * selectDeptAll : 1
     * selectDeptList : []
     */

    private int id;
    private int kdtId;
    private int deptId;
    private int cardId;
    private String cardAlias;
    private int cardSnapshotId;
    private String cardName;
    private int cardType;
    private int cardSubType;
    private int itemId;
    private String itemSnapshotId;
    private String prepaidId;
    private int cardPrice;
    private int wapShow;
    private int free;
    private int onShelve;
    private int available;
    private int deleted;
    private int onDept;
    private int syncWx;
    private int termDays;
    private String description;
    private int syncWxStatus;
    private int supportGroup;
    private int hasActiveGroup;
    private int bindNumber;
    private int onShelveDeptNumber;
    private int updatedAtLong;
    private int createdAtLong;
    private int prepaidGiftPrice;
    private String bgColor;
    private String bgImg;
    private int bgTextHide;
    private int bgNameHide;
    private int displayGrayLayer;
    private String identificationCode;
    private int distinguishAccount;
    private String rightsDesc;
    private int selectDeptAll;
    private List<Integer> selectDeptIds;
    private List<?> activityGroupList;
    private List<DiscountRightsListBean> discountRightsList;
    private List<?> timesRightsList;
    private List<?> pointRightsList;
    private List<?> giftRightsList;
    private List<?> selectDeptList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getKdtId() {
        return kdtId;
    }

    public void setKdtId(int kdtId) {
        this.kdtId = kdtId;
    }

    public int getDeptId() {
        return deptId;
    }

    public void setDeptId(int deptId) {
        this.deptId = deptId;
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public String getCardAlias() {
        return cardAlias;
    }

    public void setCardAlias(String cardAlias) {
        this.cardAlias = cardAlias;
    }

    public int getCardSnapshotId() {
        return cardSnapshotId;
    }

    public void setCardSnapshotId(int cardSnapshotId) {
        this.cardSnapshotId = cardSnapshotId;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public int getCardType() {
        return cardType;
    }

    public void setCardType(int cardType) {
        this.cardType = cardType;
    }

    public int getCardSubType() {
        return cardSubType;
    }

    public void setCardSubType(int cardSubType) {
        this.cardSubType = cardSubType;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getItemSnapshotId() {
        return itemSnapshotId;
    }

    public void setItemSnapshotId(String itemSnapshotId) {
        this.itemSnapshotId = itemSnapshotId;
    }

    public String getPrepaidId() {
        return prepaidId;
    }

    public void setPrepaidId(String prepaidId) {
        this.prepaidId = prepaidId;
    }

    public int getCardPrice() {
        return cardPrice;
    }

    public void setCardPrice(int cardPrice) {
        this.cardPrice = cardPrice;
    }

    public int getWapShow() {
        return wapShow;
    }

    public void setWapShow(int wapShow) {
        this.wapShow = wapShow;
    }

    public int getFree() {
        return free;
    }

    public void setFree(int free) {
        this.free = free;
    }

    public int getOnShelve() {
        return onShelve;
    }

    public void setOnShelve(int onShelve) {
        this.onShelve = onShelve;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public int getOnDept() {
        return onDept;
    }

    public void setOnDept(int onDept) {
        this.onDept = onDept;
    }

    public int getSyncWx() {
        return syncWx;
    }

    public void setSyncWx(int syncWx) {
        this.syncWx = syncWx;
    }

    public int getTermDays() {
        return termDays;
    }

    public void setTermDays(int termDays) {
        this.termDays = termDays;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSyncWxStatus() {
        return syncWxStatus;
    }

    public void setSyncWxStatus(int syncWxStatus) {
        this.syncWxStatus = syncWxStatus;
    }

    public int getSupportGroup() {
        return supportGroup;
    }

    public void setSupportGroup(int supportGroup) {
        this.supportGroup = supportGroup;
    }

    public int getHasActiveGroup() {
        return hasActiveGroup;
    }

    public void setHasActiveGroup(int hasActiveGroup) {
        this.hasActiveGroup = hasActiveGroup;
    }

    public int getBindNumber() {
        return bindNumber;
    }

    public void setBindNumber(int bindNumber) {
        this.bindNumber = bindNumber;
    }

    public int getOnShelveDeptNumber() {
        return onShelveDeptNumber;
    }

    public void setOnShelveDeptNumber(int onShelveDeptNumber) {
        this.onShelveDeptNumber = onShelveDeptNumber;
    }

    public int getUpdatedAtLong() {
        return updatedAtLong;
    }

    public void setUpdatedAtLong(int updatedAtLong) {
        this.updatedAtLong = updatedAtLong;
    }

    public int getCreatedAtLong() {
        return createdAtLong;
    }

    public void setCreatedAtLong(int createdAtLong) {
        this.createdAtLong = createdAtLong;
    }

    public int getPrepaidGiftPrice() {
        return prepaidGiftPrice;
    }

    public void setPrepaidGiftPrice(int prepaidGiftPrice) {
        this.prepaidGiftPrice = prepaidGiftPrice;
    }

    public String getBgColor() {
        return bgColor;
    }

    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    public String getBgImg() {
        return bgImg;
    }

    public void setBgImg(String bgImg) {
        this.bgImg = bgImg;
    }

    public int getBgTextHide() {
        return bgTextHide;
    }

    public void setBgTextHide(int bgTextHide) {
        this.bgTextHide = bgTextHide;
    }

    public int getBgNameHide() {
        return bgNameHide;
    }

    public void setBgNameHide(int bgNameHide) {
        this.bgNameHide = bgNameHide;
    }

    public int getDisplayGrayLayer() {
        return displayGrayLayer;
    }

    public void setDisplayGrayLayer(int displayGrayLayer) {
        this.displayGrayLayer = displayGrayLayer;
    }

    public String getIdentificationCode() {
        return identificationCode;
    }

    public void setIdentificationCode(String identificationCode) {
        this.identificationCode = identificationCode;
    }

    public int getDistinguishAccount() {
        return distinguishAccount;
    }

    public void setDistinguishAccount(int distinguishAccount) {
        this.distinguishAccount = distinguishAccount;
    }

    public String getRightsDesc() {
        return rightsDesc;
    }

    public void setRightsDesc(String rightsDesc) {
        this.rightsDesc = rightsDesc;
    }

    public int getSelectDeptAll() {
        return selectDeptAll;
    }

    public void setSelectDeptAll(int selectDeptAll) {
        this.selectDeptAll = selectDeptAll;
    }

    public List<Integer> getSelectDeptIds() {
        return selectDeptIds;
    }

    public void setSelectDeptIds(List<Integer> selectDeptIds) {
        this.selectDeptIds = selectDeptIds;
    }

    public List<?> getActivityGroupList() {
        return activityGroupList;
    }

    public void setActivityGroupList(List<?> activityGroupList) {
        this.activityGroupList = activityGroupList;
    }

    public List<DiscountRightsListBean> getDiscountRightsList() {
        return discountRightsList;
    }

    public void setDiscountRightsList(List<DiscountRightsListBean> discountRightsList) {
        this.discountRightsList = discountRightsList;
    }

    public List<?> getTimesRightsList() {
        return timesRightsList;
    }

    public void setTimesRightsList(List<?> timesRightsList) {
        this.timesRightsList = timesRightsList;
    }

    public List<?> getPointRightsList() {
        return pointRightsList;
    }

    public void setPointRightsList(List<?> pointRightsList) {
        this.pointRightsList = pointRightsList;
    }

    public List<?> getGiftRightsList() {
        return giftRightsList;
    }

    public void setGiftRightsList(List<?> giftRightsList) {
        this.giftRightsList = giftRightsList;
    }

    public List<?> getSelectDeptList() {
        return selectDeptList;
    }

    public void setSelectDeptList(List<?> selectDeptList) {
        this.selectDeptList = selectDeptList;
    }

    public static class DiscountRightsListBean {
        /**
         * rightsId : 20371167
         * goodsId : 105634767
         * rightsType : 1
         * subRightsType : 200
         * unlimitedTimes : 0
         * gift : 0
         * value : 100
         * scope : 2
         * accountType : 0
         * isCardRight : 0
         */

        private int rightsId;
        private int goodsId;
        private int rightsType;
        private int subRightsType;
        private int unlimitedTimes;
        private int gift;
        private int value;
        private int scope;
        private int accountType;
        private int isCardRight;

        public int getRightsId() {
            return rightsId;
        }

        public void setRightsId(int rightsId) {
            this.rightsId = rightsId;
        }

        public int getGoodsId() {
            return goodsId;
        }

        public void setGoodsId(int goodsId) {
            this.goodsId = goodsId;
        }

        public int getRightsType() {
            return rightsType;
        }

        public void setRightsType(int rightsType) {
            this.rightsType = rightsType;
        }

        public int getSubRightsType() {
            return subRightsType;
        }

        public void setSubRightsType(int subRightsType) {
            this.subRightsType = subRightsType;
        }

        public int getUnlimitedTimes() {
            return unlimitedTimes;
        }

        public void setUnlimitedTimes(int unlimitedTimes) {
            this.unlimitedTimes = unlimitedTimes;
        }

        public int getGift() {
            return gift;
        }

        public void setGift(int gift) {
            this.gift = gift;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public int getScope() {
            return scope;
        }

        public void setScope(int scope) {
            this.scope = scope;
        }

        public int getAccountType() {
            return accountType;
        }

        public void setAccountType(int accountType) {
            this.accountType = accountType;
        }

        public int getIsCardRight() {
            return isCardRight;
        }

        public void setIsCardRight(int isCardRight) {
            this.isCardRight = isCardRight;
        }
    }
}
